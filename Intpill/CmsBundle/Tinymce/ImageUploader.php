<?php

namespace Intpill\CmsBundle\Tinymce;

use Doctrine\ORM\EntityManager;
use Imagine\Filter\Transformation;
use Imagine\Image\Box;
use Imagine\Gd\Imagine;
use Intpill\CmsBundle\Entity\TinymceImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intpill\CmsBundle\Entity\TinymceImageTemp;
use Symfony\Component\DomCrawler\Crawler;
use Doctrine\Common\Collections\Criteria;

class ImageUploader
{
    private $path;
    private $em;

    public function __construct($path, EntityManager $em)
    {
        $this->path = $path;
        $this->em = $em;
    }

    public function getZoomImageName($imageName)
    {
        $parts = explode('.', $imageName);
        return $parts[0] . '-o.' . $parts[1];
    }

    public function size($image)
    {
        $imagine = (new Imagine())->open($this->path . '/' . $image);
        return $imagine->getSize();
    }

    public function upload(UploadedFile $image, $maxWidth, $maxZoomWidth)
    {
        $maxZoomHeight = round($maxZoomWidth / 1.6);
        $maxHeight = $maxWidth;
        $imageName = uniqid() . '.' . strtolower($image->guessExtension());
        $zoomImageName = $this->getZoomImageName($imageName);

        $this->createPath();

        $imagine = (new Imagine())->open($image->getPathname());

        $transformation = new Transformation();
        $transformation->thumbnail(new Box($maxZoomWidth, $maxZoomHeight))
            ->save($this->path . '/' . $zoomImageName);
        $transformation->apply($imagine);

        $transformation = new Transformation();
        $transformation->thumbnail(new Box($maxWidth, $maxHeight))
            ->save($this->path . '/' . $imageName);
        $transformation->apply($imagine);


        $tinymceImageTemp = new TinymceImageTemp();
        $tinymceImageTemp->setImage($imageName);

        $this->em->persist($tinymceImageTemp);
        $this->em->flush();


        return $imageName;
    }

    public function move($html, $item)
    {
        if (strlen($item) === 0) return false;

        $currentImages = $this->parseImages($html);
        $tinymceImage = $this->em->getRepository('CmsBundle:TinymceImage')->findOneBy(['item' => $item]);
        if ($tinymceImage) {
            $this->removeUnused($currentImages, $tinymceImage->getImages());
        } else {
            $tinymceImage = new TinymceImage();
            $tinymceImage->setItem($item);
        }
        $tinymceImage->setImages($currentImages);

        $this->em->persist($tinymceImage);
        $this->em->flush();

        if (count($currentImages)) {
            $repoTemp = $this->em->getRepository('CmsBundle:TinymceImageTemp');
            $repoTemp->createQueryBuilder('t')->delete()->where('t.image IN(:images)')->setParameter('images', $currentImages)->getQuery()->getResult();
        }

        $this->removeTempOverdue();
    }

    public function remove($item)
    {
        $tinymceImage = $this->em->getRepository('CmsBundle:TinymceImage')->findOneBy(['item' => $item]);
        if ($tinymceImage) {
            foreach ($tinymceImage->getImages() as $image) {
                $this->unlinkImage($image);
            }
            $this->em->remove($tinymceImage);
            $this->em->flush();
        }
    }

    public function parseImages($html)
    {
        $images = [];

        $crawler = new Crawler($html);

        $crawler->filter('img')->each(function (Crawler $node, $i) use (&$images) {
            if ($image = basename($node->attr('src')) ) {
                $images[] = $image;
            }
        });

        return $images;
    }

    public function removeUnused($currentImages, $tinymceImages)
    {
        $images = [];
        foreach ($tinymceImages as $image) {
            if (!in_array($image, $currentImages)) {
                $this->unlinkImage($image);
            }
        }
    }

    public function unlinkImage($image)
    {
        @unlink($this->path . '/' . $image);
        @unlink($this->path . '/' . $this->getZoomImageName($image));
    }

    public function removeTempOverdue()
    {
        $lastDate = new \DateTime("now");
        $lastDate->modify('-1 day');

        $criteria = Criteria::create()
            ->where(Criteria::expr()->lt('createdAt', $lastDate));
        $tinymceImageTemps = $this->em->getRepository('CmsBundle:TinymceImageTemp')->matching($criteria)->toArray();

        foreach ($tinymceImageTemps as $image) {
            $this->unlinkImage($image->getImage());
            $this->em->remove($image);
        }

        $this->em->flush();
    }

    protected function createPath()
    {
        if (!is_dir($this->path)) {
            mkdir($this->path);
        }
    }

    public function getPath()
    {
        return $this->path;
    }
}