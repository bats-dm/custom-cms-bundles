<?php
namespace Intpill\CmsBundle\Content;

use Doctrine\ORM\EntityManager;
use Intpill\CmsBundle\Entity\Setting;

class Settings
{
    protected $em;
    protected $settings;
    protected $configSettings;

    public function __construct(EntityManager $em, array $configSettings)
    {
        $this->em = $em;
        $this->configSettings = $configSettings;
        $this->load();
    }

    public function load()
    {
        $this->settings = $this->configSettings;
        $entitySettings = $this->getEntitySettings();

        foreach ($this->configSettings as $groupSlug => $group) {
            foreach ($group['items'] as $groupItemSlug => $groupItem) {
                if (isset($entitySettings[$groupItemSlug])) {
                    $this->settings[$groupSlug]['items'][$groupItemSlug]['value'] = $entitySettings[$groupItemSlug];
                    $this->settings[$groupSlug]['items'][$groupItemSlug]['type'] = 'text';
                }
            }
        }
    }

    public function getEntitySettings()
    {
        $result = [];
        $entitySettings = $this->em->getRepository('CmsBundle:Setting')->findAll();
        foreach ($entitySettings as $entitySetting) {
            $result[$entitySetting->getSlug()] = $entitySetting->getValue();
        }
        return $result;
    }

    public function findByGroup($group)
    {
        return $this->settings[$group]['items'];
    }

    public function find($slug)
    {
        foreach ($this->settings as $group) {
            foreach ($group['items'] as $groupItemSlug => $groupItem) {
                if ($groupItemSlug == $slug) {
                    return $groupItem;
                }
            }
        }
    }

    public function value($slug)
    {
        return $this->find($slug)['value'];
    }

    public function getSettings()
    {
        return $this->settings;
    }

    public function save($data)
    {
        foreach ($data as $slug => $value) {
            $this->saveOne($slug, $value);
        }
        $this->flush();
    }

    public function saveOne($slug, $value)
    {
        $setting = $this->em->getRepository('CmsBundle:Setting')->findOneBy(['slug' => $slug]);
        if (!$setting) {
            $setting = new Setting();
            $setting->setSlug($slug);
            $this->em->persist($setting);
        }
        $setting->setValue($value);
    }

    public function flush()
    {
        $this->em->flush();
        $this->load();
    }
}