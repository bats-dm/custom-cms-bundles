<?php

namespace Intpill\CmsBundle\Text;


use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class Declension
{
    protected $cache;

    public function __construct($cacheDir)
    {
        $cacheLifetime = 60*60*24*5;

        $this->cache = new FilesystemAdapter(
            '', $cacheLifetime, $cacheDir . '/' . 'text_declension'
        );
    }

    public function getAll($text)
    {
        $declensions = $this->getFromCache($text);

        if (!$declensions) {
            $declensions = $this->requestFromMorpher($text);
            $this->saveToCache($text, $declensions);
        }

        return $declensions;
    }

    public function getOne($text, $case)
    {
        $items = $this->getAll($text);
        return ($items && isset($items[$case])) ? $items[$case] : false;
    }

    protected function requestFromMorpher($text)
    {
        $ch = curl_init(
            'https://ws3.morpher.ru/russian/declension' .
            '?s=' . urlencode($text) .
            '&format=json'
        );

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response, true);

        return $response;
    }

    protected function getFromCache($key)
    {
        $cacheItem = $this->cache->getItem($key);
        return $cacheItem->isHit() ? unserialize($cacheItem->get()) : false;
    }

    protected function saveToCache($key, $value)
    {
        $cacheItem = $this->cache->getItem($key);
        $cacheItem->set(serialize($value));
        $this->cache->save($cacheItem);
    }

}