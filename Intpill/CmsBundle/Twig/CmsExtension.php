<?php

namespace Intpill\CmsBundle\Twig;

use Intpill\CmsBundle\Content\Settings;
use Intpill\CmsBundle\Entity\Setting;
use Intpill\CmsBundle\Service\Notification;
use Intpill\CmsBundle\Service\Notifications;
use Intpill\CmsBundle\Site\Breadcrumbs;
use Intpill\CmsBundle\Site\ActivePage;

class CmsExtension extends \Twig_Extension
{
    protected $page;
    protected $menu;
    protected $breadcrumbs;
    protected $setting;
    protected $notifications;

    public function __construct(ActivePage $page, Breadcrumbs $breadcrumbs, Settings $setting, Notifications $notifications)
    {
        $this->page = $page;
        $this->breadcrumbs = $breadcrumbs;
        $this->setting = $setting;
        $this->notifications = $notifications;
    }

    public function getGlobals()
    {
        return [
            'cms_page' => $this->page,
            'cms_menu' => $this->menu,
            'cms_breadcrumbs' => $this->breadcrumbs,
            'cms_setting' => $this->setting,
            'cms_admin_notifications' => $this->notifications
        ];
    }

    public function getName()
    {
        return 'cms_extension';
    }
}