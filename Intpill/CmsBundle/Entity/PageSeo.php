<?php

namespace Intpill\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PageSeo
 *
 * @ORM\Table(name="cms_page_seo")
 * @ORM\Entity(repositoryClass="Intpill\CmsBundle\Repository\PageSeoRepository")
 */
class PageSeo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    protected $metaKeywords;

    /**
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @ORM\Column(name="sitemap_priority", type="float", nullable=true)
     */
    protected $sitemapPriority;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="seo")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $page;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PageSeo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PageSeo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return PageSeo
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return PageSeo
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set page
     *
     * @param \Intpill\CmsBundle\Entity\Page $page
     *
     * @return PageSeo
     */
    public function setPage(\Intpill\CmsBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \Intpill\CmsBundle\Entity\Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set sitemapPriority
     *
     * @param float $sitemapPriority
     *
     * @return PageSeo
     */
    public function setSitemapPriority($sitemapPriority)
    {
        $this->sitemapPriority = $sitemapPriority;

        return $this;
    }

    /**
     * Get sitemapPriority
     *
     * @return float
     */
    public function getSitemapPriority()
    {
        return $this->sitemapPriority;
    }
}
