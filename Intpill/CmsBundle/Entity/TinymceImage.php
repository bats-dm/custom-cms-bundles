<?php

namespace Intpill\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TinymceImage
 *
 * @ORM\Table(name="cms_tinymce_image")
 * @ORM\Entity()
 */
class TinymceImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="item", type="string", length=255, unique=true)
     *
     * @Assert\NotBlank()
     */
    private $item;

    /**
     * @var array
     *
     * @ORM\Column(name="images", type="simple_array", nullable=true)
     */
    private $images = [];

    /**
     * @Assert\NotBlank()
     * @Assert\Image()
     */
    private $image;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item
     *
     * @param string $item
     *
     * @return TinymceImage
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set images
     *
     * @param array $images
     *
     * @return TinymceImage
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    public function appendImages($images)
    {
        if (!is_array($images)) $images = [$images];

        $this->images = array_merge($this->images, $images);

        return $this;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }
}
