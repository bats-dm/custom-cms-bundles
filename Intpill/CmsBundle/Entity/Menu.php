<?php
namespace Intpill\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Intpill\CmsBundle\Entity\Menu
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @ORM\Table(name="cms_menu")
 */
class Menu
{
    const TYPE_PAGE = 'page';
    const TYPE_LINK = 'link';
    const TYPE_ROOT = 'root';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $parent_id;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $page_id;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="menu")
     * @ORM\JoinColumn(name="page_id", onDelete="SET NULL")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    protected $page;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type = 'page';

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="boolean")
     */
    private $target_blank = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Intpill\CmsBundle\Entity\Menu
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of pid.
     *
     * @param integer $pid
     * @return \Intpill\CmsBundle\Entity\Menu
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;

        return $this;
    }

    /**
     * Get the value of pid.
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \Intpill\CmsBundle\Entity\Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of page_id.
     *
     * @param integer $page_id
     * @return \Intpill\CmsBundle\Entity\Menu
     */
    public function setPageId($page_id)
    {
        $this->page_id = $page_id;

        return $this;
    }

    /**
     * Get the value of page_id.
     *
     * @return integer
     */
    public function getPageId()
    {
        return $this->page_id;
    }

    /**
     * Set Page entity (many to one).
     *
     * @param \Intpill\CmsBundle\Entity\Page $page
     * @return \Intpill\CmsBundle\Entity\Menu
     */
    public function setPage(Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get Page entity (many to one).
     *
     * @return \Intpill\CmsBundle\Entity\Page
     */
    public function getPage()
    {
        return $this->page;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setParent(Menu $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Menu
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    public function setTargetBlank($targetBlank)
    {
        $this->target_blank = $targetBlank;

        return $this;
    }

    public function getTargetBlank()
    {
        return $this->target_blank;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Menu
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Menu
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Menu
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param \Intpill\CmsBundle\Entity\Menu $root
     *
     * @return Menu
     */
    public function setRoot(\Intpill\CmsBundle\Entity\Menu $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Add child
     *
     * @param \Intpill\CmsBundle\Entity\Menu $child
     *
     * @return Menu
     */
    public function addChild(\Intpill\CmsBundle\Entity\Menu $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Intpill\CmsBundle\Entity\Menu $child
     */
    public function removeChild(\Intpill\CmsBundle\Entity\Menu $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Menu[]
     */
    public function getChildren()
    {
        return $this->children;
    }
}
