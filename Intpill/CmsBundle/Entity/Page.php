<?php
namespace Intpill\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @ORM\Table(name="cms_page")
 */
class Page
{
    const TYPE_PAGE = 'page';
    const TYPE_MODULE = 'module';
    const TYPE_PAGE_LINK = 'page_link';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type = Page::TYPE_PAGE;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $css;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $javascript;

    /**
     * @ORM\Column(name="page_head", type="text", nullable=true)
     */
    protected $pageHead;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $module;

    /**
     * @ORM\Column(name="menu_class", length=255, nullable=true)
     */
    protected $menuClass;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    protected $metaKeywords;

    /**
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @ORM\Column(name="sitemap_priority", type="float", nullable=true)
     */
    protected $sitemapPriority;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled = true;

    /**
     * @ORM\Column(name="is_home", type="boolean")
     */
    protected $isHome = false;

    /**
     * @ORM\OneToMany(targetEntity="Intpill\CmsBundle\Entity\Menu", mappedBy="page")
     */
    protected $menu;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="PageSeo", mappedBy="page")
     */
    private $seo;

    /**
     * @param $name
     * @return PageSeo
     */
    public function getSeoByName($name)
    {
        foreach ($this->getSeo() as $seo) {
            if ($seo->getName() == $name) {
                return $seo;
            }
        }
        return false;
    }

    public function __construct()
    {
        $this->menu = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->seo = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getMenuClass()
    {
        return $this->menuClass;
    }

    /**
     * @param mixed $menuClass
     * @return Page
     */
    public function setMenuClass($menuClass)
    {
        $this->menuClass = $menuClass;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param mixed $module
     * @return Page
     */
    public function setModule($module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return Page
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type.
     *
     * @param string $type
     * @return Page
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the value of slug.
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the value of slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of title.
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of metaKeywords.
     *
     * @param string $metaKeywords
     * @return Page
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get the value of metaKeywords.
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set the value of metaDescription.
     *
     * @param string $metaDescription
     * @return Page
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get the value of metaDescription.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set the value of enabled.
     *
     * @param boolean $enabled
     * @return Page
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get the value of enabled.
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get the value of isHome.
     *
     * @return boolean
     */
    public function getIsHome()
    {
        return $this->isHome;
    }

    /**
     * Set the value of enabled.
     *
     * @param boolean $isHome
     * @return Page
     */
    public function setIsHome($isHome)
    {
        $this->isHome = $isHome;

        return $this;
    }

    /**
     * Add Menu entity to collection (one to many).
     *
     * @param \Intpill\CmsBundle\Entity\Menu $menu
     * @return Page
     */
    public function addMenu(Menu $menu)
    {
        $this->menu[] = $menu;

        return $this;
    }

    /**
     * Remove Menu entity from collection (one to many).
     *
     * @param \Intpill\CmsBundle\Entity\Menu $menu
     * @return Page
     */
    public function removeMenu(Menu $menu)
    {
        $this->menu->removeElement($menu);

        return $this;
    }

    /**
     * Get Menu entity collection (one to many).
     *
     * @return Menu[]
     */
    public function getMenu()
    {
        return $this->menu;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Page
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Page
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Page
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set parent
     *
     * @param \Intpill\CmsBundle\Entity\Page $parent
     *
     * @return Page
     */
    public function setParent(\Intpill\CmsBundle\Entity\Page $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Intpill\CmsBundle\Entity\Page
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \Intpill\CmsBundle\Entity\Page $child
     *
     * @return Page
     */
    public function addChild(\Intpill\CmsBundle\Entity\Page $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Intpill\CmsBundle\Entity\Page $child
     */
    public function removeChild(\Intpill\CmsBundle\Entity\Page $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set css
     *
     * @param string $css
     *
     * @return Page
     */
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get css
     *
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set javascript
     *
     * @param string $javascript
     *
     * @return Page
     */
    public function setJavascript($javascript)
    {
        $this->javascript = $javascript;

        return $this;
    }

    /**
     * Get javascript
     *
     * @return string
     */
    public function getJavascript()
    {
        return $this->javascript;
    }

    /**
     * Add seo
     *
     * @param \Intpill\CmsBundle\Entity\PageSeo $seo
     *
     * @return Page
     */
    public function addSeo(\Intpill\CmsBundle\Entity\PageSeo $seo)
    {
        $this->seo[] = $seo;

        return $this;
    }

    /**
     * Remove seo
     *
     * @param \Intpill\CmsBundle\Entity\PageSeo $seo
     */
    public function removeSeo(\Intpill\CmsBundle\Entity\PageSeo $seo)
    {
        $this->seo->removeElement($seo);
    }

    /**
     * Get seo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeo()
    {
        return $this->seo;
    }

    /**
     * Set sitemapPriority
     *
     * @param float $sitemapPriority
     *
     * @return Page
     */
    public function setSitemapPriority($sitemapPriority)
    {
        $this->sitemapPriority = $sitemapPriority;

        return $this;
    }

    /**
     * Get sitemapPriority
     *
     * @return float
     */
    public function getSitemapPriority()
    {
        return $this->sitemapPriority;
    }

    /**
     * Set pageHead
     *
     * @param string $pageHead
     *
     * @return Page
     */
    public function setPageHead($pageHead)
    {
        $this->pageHead = $pageHead;

        return $this;
    }

    /**
     * Get pageHead
     *
     * @return string
     */
    public function getPageHead()
    {
        return $this->pageHead;
    }
}
