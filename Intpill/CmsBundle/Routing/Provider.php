<?php
namespace Intpill\CmsBundle\Routing;

use Intpill\CmsBundle\Entity\Page;
use Intpill\CmsBundle\Site\Module\Modules;
use Intpill\CmsBundle\Site\PageHelper;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Cmf\Component\Routing\RouteProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class Provider implements RouteProviderInterface
{
    protected $em;
    protected $routes = [];
    protected $modules;
    protected $pageController;

    public function __construct(EntityManager $em, Modules $modules, $pageController)
    {
        $this->em = $em;
        $this->modules = $modules;
        $this->pageController = $pageController;
    }

    protected function getSiteRoutes()
    {
        $routes = [];

        $pages = $this->em->getRepository('CmsBundle:Page')
            ->createQueryBuilder('p')
            ->select('p.id, p.slug, p.isHome, p.type, p.module')
            ->getQuery()
            ->getArrayResult()
        ;

        $foundModules = [];
        foreach ($pages as $page) {
            switch ($page['type']) {
                case Page::TYPE_PAGE:
                    $routes[PageHelper::ROUTE_PREFIX . $page['id']] = new Route('/'.$this->getSiteUrl($page['slug'], $page['isHome']), ['_controller' => $this->pageController]);
                    break;
                case Page::TYPE_MODULE:
                    if ($module = $this->modules->find($page['module'])) {
                        $foundModules[] = $module->getName();
                        foreach ($module->getRoutes() as $moduleRoute) {
                            $url = $page['slug'];
                            $defaults = ['_controller' => $moduleRoute->getController()];
                            if ($moduleRoute->getParams()) {
                                $url .= '/'.$moduleRoute->getParams();
                            }
                            if ($moduleRoute->getDefaults()) {
                                $defaults = array_merge($defaults, $moduleRoute->getDefaults());
                            }

                            $routes[PageHelper::ROUTE_PREFIX . $moduleRoute->getName()] = new Route(
                                '/'.$this->getSiteUrl($url, $page['isHome']),
                                $defaults
                            );
                        }
                    }
                    break;
            }
        }
        foreach ($this->modules->getItems() as $module) {
            if (!in_array($module->getName(), $foundModules)) {
                $this->modules->save($module->getName());
            }
        }

        return $routes;
    }

    public function getSiteUrl($slug, $isHome)
    {
        if ($isHome) {
            return '';
        }
        return $slug;
    }

    public function getRouteCollectionForRequest(Request $request)
    {
        $this->routes = $this->getSiteRoutes();

        $routeCollection = new RouteCollection();

        foreach ($this->routes as $routeName => $route) {
            $routeCollection->add($routeName, $route);
        }

        return $routeCollection;
    }

    public function getRouteByName($name)
    {
        if (!isset($this->routes[$name])) throw new RouteNotFoundException('Route '.$name.' not found');

        return $this->routes[$name];
    }

    public function getRoutesByNames($names = null)
    {
        throw new RouteNotFoundException('Routes not found!!!!!');
    }
}