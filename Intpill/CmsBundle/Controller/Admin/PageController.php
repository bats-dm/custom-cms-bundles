<?php

namespace Intpill\CmsBundle\Controller\Admin;


use Intpill\CmsBundle\Entity\Page;
use Sonata\AdminBundle\Controller\CRUDController;

class PageController extends CRUDController
{
    public function listAction()
    {
        $request = $this->getRequest();
        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        } else {
            if (!$this->admin->getRequest()->getSession()->get(sprintf('%s.list_mode', $this->admin->getCode()))) {
                $this->admin->setListMode('tree');
            }
        }
        $listMode = $this->admin->getListMode();
        if ($listMode === 'tree') {
            $request = $this->getRequest();
            $preResponse = $this->preList($request);
            if ($preResponse !== null) {
                return $preResponse;
            }
            return $this->render('CmsBundle:Admin/Sonata:admin_page_tree.html.twig', array(
                'type_page' => Page::TYPE_PAGE,
                'type_module' => Page::TYPE_MODULE,
                'type_page_link' => Page::TYPE_PAGE_LINK,
                'action' => 'list',
                'tree' => $this->getTree(),
                'csrf_token' => $this->getCsrfToken('sonata.batch'),
            ), null);
        }

        return parent::listAction();
    }

    public function getTree($parentNode = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('CmsBundle:Page');

        $result = [];
        /** @var Page $node */
        foreach ($repo->children($parentNode, true, 'name') as $node) {
            $resultItem = [
                'folder' => 0,
                'order' => count($result),
                'key' => $node->getId(),
                'title' => $node->getName(),
                'type' => $node->getType(),
                'slug' => $node->getType() == Page::TYPE_PAGE ? $node->getSlug() : '',
                'link' => [
                    'edit' => $this->admin->generateUrl('edit', ['id' => $node->getId()]),
                    'remove' => $this->admin->generateUrl('delete', ['id' => $node->getId()]),
                    'add' => $this->admin->generateUrl('create', ['parent_id' => $node->getId()])
                ],
                'columns' => [
                    $node->getEnabled() ? '<span class="label label-success"><i class="fa fa-unlock" aria-hidden="true"></i></span>' : '<span class="label label-danger"><i class="fa fa-lock" aria-hidden="true"></i></span>'
                ]
            ];
            if ($children = $this->getTree($node)) {
                $resultItem['folder'] = 1;
                $resultItem['children'] = $children;
            }
            $result[] = $resultItem;
        }

        usort($result, function ($a, $b) {
            if ($a['folder'] == $b['folder']) {
                return $a['order'] < $b['order'] ? -1 : 1;
            }
            return $a['folder'] > $b['folder'] ? -1 : 1;
        });

        return $result;
    }
}