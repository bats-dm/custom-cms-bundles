<?php

namespace Intpill\CmsBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

class SettingController extends CRUDController
{
    public function listAction()
    {
        $request = $this->getRequest();
        $groups = $this->get('cms.settings')->getSettings();
        reset($groups);
        $group = $request->get('group') ?: key($groups);

        if ($slug = $request->request->get('slug')) {
            $this->get('cms.settings')->save($slug);
            $this->addFlash(
                'sonata_flash_success',
                $this->trans('flash_edit_success')
            );
            return $this->redirect($this->admin->generateUrl('list'));
        }

        return $this->render(
            'CmsBundle:Admin/Sonata:admin_setting_list.html.twig',
            [
                'action' => 'list',
                'groups' => $groups,
                'group' => $group,
                'settings' => $this->get('cms.settings')->findByGroup($group)
            ]
        );
    }
}
