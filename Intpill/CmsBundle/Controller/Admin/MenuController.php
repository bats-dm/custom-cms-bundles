<?php

namespace Intpill\CmsBundle\Controller\Admin;

use Intpill\CmsBundle\Entity\Menu;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MenuController extends CRUDController
{
    public function listAction()
    {
        $request = $this->getRequest();
        $preResponse = $this->preList($request);
        if ($preResponse !== null) {
            return $preResponse;
        }
        return $this->render('CmsBundle:Admin/Sonata:admin_menu_tree.html.twig', array(
            'type_page' => Menu::TYPE_PAGE,
            'type_link' => Menu::TYPE_LINK,
            'type_root' => Menu::TYPE_ROOT,
            'action' => 'list',
            'tree' => $this->getTree(),
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ), null);
    }

    public function treeMoveAction(Request $request)
    {
        $id = (int)$request->request->get('id');
        $positon = (int)$request->request->get('position');
        $oldPositon = (int)$request->request->get('old_position');
        $parentId = (int)$request->request->get('parent_id');

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('CmsBundle:Menu');

        $menu = $repo->find($id);

        if ($menu->getType() == Menu::TYPE_ROOT) {
            throw new \Exception(Menu::TYPE_ROOT . ' cant be moved');
        }

        if ($menu->getParentId() == $parentId) {
            if ($positon > $oldPositon) {
                $repo->moveDown($menu, $positon-$oldPositon);
            } else {
                $repo->moveUp($menu, $oldPositon-$positon);
            }
        } else {
            $parentMenu = $repo->find($parentId);
            if (!$parentMenu) {
                throw new \Exception('Parent not found');
            }
            $menu->setParent($parentMenu);
            $em->flush();

            if ($positon > 0) {
                $repo->moveDown($menu, $positon);
            }
        }

        return new JsonResponse([]);
    }

    public function getTree($parentNode = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('CmsBundle:Menu');

        $result = [];
        /** @var Menu $node */
        foreach ($repo->children($parentNode, true) as $node) {
            $resultItem = [
                'key' => $node->getId(),
                'title' => $node->getName(),
                'expanded' => $parentNode ? false : true,
                'type' => $node->getType(),
                'link' => [
                    'edit' => $this->admin->generateUrl('edit', ['id' => $node->getId()]),
                    'remove' => $this->admin->generateUrl('delete', ['id' => $node->getId()]),
                    'add' => $this->admin->generateUrl('create', ['parent_id' => $node->getId()])
                ],
                'columns' => [
                    $node->getEnabled() ? '<span class="label label-success"><i class="fa fa-unlock" aria-hidden="true"></i></span>' : '<span class="label label-danger"><i class="fa fa-lock" aria-hidden="true"></i></span>'
                ]
            ];
            if ($children = $this->getTree($node)) {
                $resultItem['children'] = $children;
            }
            $result[] = $resultItem;
        }

        return $result;
    }
}
