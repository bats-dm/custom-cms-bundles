<?php
/*
 * create in config liip_imagine filter "tinymce_imageuploader"
 */

namespace Intpill\CmsBundle\Controller\Admin;

use Intpill\CmsBundle\Entity\TinymceImageTemp;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;

class TinyMceController extends Controller
{
    public function imageUploadAction(Request $request)
    {
        $maxZoomImageWidth = (int) $request->request->get('max_zoom_width') ?: $this->getParameter('cms.tinymce.imageuploader.zoom_max_width');
        $maxImageWidth = (int) $request->request->get('max_width') ?: $this->getParameter('cms.tinymce.imageuploader.max_width');
        $absolutePath = (bool) $request->request->get('absolute_path');
        $zoom = (bool) $request->request->get('zoom');
        $image = $request->files->get('image');

        $errorList = $this->get('validator')->validate(
            $image,
            [
                new Assert\Image(),
                new Assert\NotBlank()
            ]
        );

        if (count($errorList)) {
            return new JsonResponse([
                'error' => $errorList[0]->getMessage()
            ]);
        }

        $url = '';
        if ($absolutePath) {
            $url = $request->getSchemeAndHttpHost();
        }

        $imageName = $this->get('cms.tinymce.imageuploader')->upload($image, $maxImageWidth, $maxZoomImageWidth);

        $imgSrc = $url . $this->container->get('assets.packages')->getUrl(
            $this->getParameter('cms.tinymce.imageuploader.dir') . '/' . $imageName
        );
        $zoomImgSrc = $url . $this->container->get('assets.packages')->getUrl(
            $this->getParameter('cms.tinymce.imageuploader.dir') . '/' . $this->get('cms.tinymce.imageuploader')->getZoomImageName($imageName)
        );

        $size = $this->get('cms.tinymce.imageuploader')->size($imageName);
        $img = '<img src="' . $imgSrc . '" width="' . $size->getWidth() . '" height="' . $size->getHeight() . '" alt="" >';
        if ($zoom) {
            $img = '<a href="' . $zoomImgSrc . '" data-lightbox="' . uniqid() . '">' . $img . '</a>';
        }

        return new JsonResponse([
            'ok' => 1,
            'img' => $img
        ]);
    }
}
