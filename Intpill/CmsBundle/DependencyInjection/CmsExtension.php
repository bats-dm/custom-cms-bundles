<?php

namespace Intpill\CmsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class CmsExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('cms.page_controller', $config['page_controller']);
        $container->setParameter('cms.settings', $config['settings']);
        $container->setParameter('cms.modules', $config['modules']);
        $container->setParameter('cms.admin_roles', $config['admin_roles']);
        $container->setParameter('cms.page_children', $config['page_children']);
        $container->setParameter('cms.uploader', $config['uploader']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
