<?php

namespace Intpill\CmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cms');

        $rootNode
            ->children()
                ->scalarNode('page_controller')
                    ->defaultValue('AppBundle:Page:index')
                ->end()
                ->arrayNode('settings')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('label')->end()
                            ->arrayNode('items')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('name')->end()
                                        ->scalarNode('value')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('modules')
                    ->prototype('array')
                        ->children()
                            ->arrayNode('routes')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('controller')
                                            ->isRequired()
                                        ->end()
                                        ->scalarNode('title')->end()
                                        ->arrayNode('seo_vars')->prototype('scalar')->end()->end()
                                        ->scalarNode('params')->end()
                                        ->arrayNode('defaults')
                                            ->useAttributeAsKey('name')
                                            ->prototype('scalar')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->scalarNode('name')->end()
                            ->scalarNode('slug')->end()
                            ->scalarNode('parent_id')->end()
                            ->scalarNode('menu_parent_id')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('admin_roles')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('page_children')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('service')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('uploader')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('template')->end()
                            ->arrayNode('fields')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('dir')->end()
                                        ->arrayNode('image_options')
                                            ->children()
                                                ->scalarNode('width')->end()
                                                ->scalarNode('height')->end()
                                                ->booleanNode('outbound')->end()
                                            ->end()
                                        ->end()
                                        ->arrayNode('extra_fields')
                                            ->prototype('array')
                                                ->children()
                                                    ->scalarNode('dir')->end()
                                                    ->arrayNode('image_options')
                                                        ->children()
                                                            ->scalarNode('width')->end()
                                                            ->scalarNode('height')->end()
                                                            ->booleanNode('outbound')->end()
                                                        ->end()
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
