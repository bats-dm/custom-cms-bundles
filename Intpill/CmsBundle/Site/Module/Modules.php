<?php
namespace Intpill\CmsBundle\Site\Module;

use Doctrine\ORM\EntityManager;
use Intpill\CmsBundle\Entity\Menu;
use Intpill\CmsBundle\Entity\Page;

class Modules
{
    /** @var Module[] */
    protected $items;
    protected $em;
    protected $configModules;

    public function __construct($configModules, EntityManager $em)
    {
        $this->em = $em;
        $this->configModules = $configModules;

        $this->load();
    }

    private function load()
    {
        $this->items = [];

        if ($this->configModules) foreach ($this->configModules as $moduleName=>$moduleData) {
            if (isset($moduleData['routes'])) {
                $module = (new Module())->setName($moduleName);
                foreach ($moduleData['routes'] as $routeName=>$routeData) {
                    $module->addRoute(
                        (new Route())
                            ->setName($routeName)
                            ->setController($routeData['controller'])
                            ->setParams($this->getArrayValue($routeData, 'params'))
                            ->setDefaults($this->getArrayValue($routeData, 'defaults'))
                            ->setTitle($this->getArrayValue($routeData, 'title'))
                            ->setSeoVars($this->getArrayValue($routeData, 'seo_vars'))
                    );
                }
                $this->items[] = $module;
            }
        }
    }

    /**
     * @param $moduleName
     * @return bool
     */
    public function save($moduleName)
    {
        if ($this->em->getRepository('CmsBundle:Page')->findBy(['module' => $moduleName])) {
            return false;
        }

        $configModule = null;
        if ($this->configModules) foreach ($this->configModules as $configModuleName=>$moduleData) {
            if ($moduleName == $configModuleName) {
                $configModule = $moduleData;
                break;
            }
        }

        if (!$configModule) {
            return false;
        }

        $pageName = $this->getArrayValue($configModule, 'name');
        $slug = $this->getArrayValue($configModule, 'slug');
        $parentId = $this->getArrayValue($configModule, 'parent_id');
        $menuParentId = $this->getArrayValue($configModule, 'menu_parent_id');

        if ($pageName || $slug) {
            $page = (new Page())
                ->setName($pageName)
                ->setSlug($slug)
                ->setModule($moduleName)
                ->setType(Page::TYPE_MODULE)
                ->setEnabled(true)
            ;
            if ($parentId && ($parentPage = $this->em->getRepository('CmsBundle:Page')->find($parentId))) {
                $page->setParent($parentPage);
            }
            if ($menuParentId && ($menuParent = $this->em->getRepository('CmsBundle:Menu')->find($menuParentId))) {
                $menu = (new Menu())
                    ->setName($pageName)
                    ->setParent($menuParent)
                    ->setType(Menu::TYPE_PAGE)
                    ->setPage($page)
                    ->setEnabled(true)
                ;
                $this->em->persist($menu);
            }
            $this->em->persist($page);
            $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * @return Module[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $moduleName
     * @return Module
     */
    public function find($moduleName)
    {
        foreach ($this->items as $item) {
            if ($item->getName() == $moduleName) {
                return $item;
            }
        }

        return false;
    }

    /**
     * @param $routeName
     * @return Module
     */
    public function findByRoute($routeName)
    {
        foreach ($this->items as $item) {
            foreach ($item->getRoutes() as $route) {
                if ($route->getName() == $routeName) {
                    return $item;
                }
            }
        }

        return false;
    }

    public function getArrayValue($array, $value)
    {
        return isset($array[$value]) ? $array[$value] : '';
    }
}