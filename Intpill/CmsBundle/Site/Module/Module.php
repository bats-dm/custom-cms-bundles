<?php
namespace Intpill\CmsBundle\Site\Module;


use Intpill\CmsBundle\Entity\Page;

class Module
{
    protected $routes = [];
    protected $name;

    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param array $routes
     * @return Module
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
        return $this;
    }

    /**
     * @param Route $route
     * @return Module
     */
    public function addRoute(Route $route)
    {
        $this->routes[] = $route;
        return $this;
    }

    /** @return Route */
    public function getFirstRoute()
    {
        return $this->routes[0];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Module
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}