<?php

namespace Intpill\CmsBundle\Site\Menu;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Intpill\CmsBundle\Entity\Menu;
use Intpill\CmsBundle\Site\Module\Module;
use Intpill\CmsBundle\Site\PageHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Cmf\Component\Routing\ChainRouter as Router;
use Doctrine\ORM\EntityManager;

class MenuBuilder extends MenuBuilderAbstract
{
    /**
     * @param Menu $menuEntity
     * @return MenuItem[]
     * @throws \Exception
     */
    public function create(Menu $menuEntity)
    {
        $menuItems = [];

        /** @var NestedTreeRepository $repo */
        $repo = $this->getEm()->getRepository('CmsBundle:Menu');
        /** @var Menu $child */
        foreach ($repo->children($menuEntity, true) as $child) {
            if ($child->getEnabled()) {
                $menuItem = (new MenuItem())
                    ->setName($child->getName())
                    ->setUrl($this->getUrl($child))
                    ->setBlank($child->getTargetBlank())
                    ->setActive($this->isActive($child))
                ;
                if ($child->getPage() && ($menuService = $child->getPage()->getMenuClass())) {
                    /** @var MenuChildBuilderInterface $menuChildBuilder */
                    $menuChildBuilder = $this->container->get($menuService);
                    if (!($menuChildBuilder instanceof MenuChildBuilderInterface)) {
                        throw new \Exception(get_class($menuChildBuilder).' should implements '.MenuChildBuilderInterface::class);
                    }
                    $menuItem->setChildren($menuChildBuilder->create());
                } elseif ($children = $this->create($child)) {
                    $menuItem->setChildren($children);
                }

                $menuItems[] = $menuItem;
            }
        }

        return $menuItems;
    }

    public function getUrl(Menu $menu)
    {
        if ($menu->getType() == Menu::TYPE_LINK) {
            return $menu->getLink();
        } elseif ($menu->getType() == Menu::TYPE_PAGE && $menu->getPage() && ($route = $this->getPageHelper()->routeName($menu->getPage()))) {
            return $this->getRouter()->generate($route);
        }

        return '#';
    }

    public function isActive(Menu $menu)
    {
        $pageRepo = $this->getEm()->getRepository('CmsBundle:Page');
        $isActive = false;

        if ($menuPage = $menu->getPage()) {
            $currentRoute = $this->getRequestStack()->getCurrentRequest()->get('_route');
            if ($currentRoute == $this->getPageHelper()->routeName($menuPage)) {
                $isActive = true;
            } elseif (count($menu->getChildren()) == 0) {
                foreach ($pageRepo->children($menuPage) as $childPage) {
                    if ($currentRoute == $this->getPageHelper()->routeName($childPage)) {
                        $isActive = true;
                        break;
                    }
                }
            }
        }

        return $isActive;
    }
}