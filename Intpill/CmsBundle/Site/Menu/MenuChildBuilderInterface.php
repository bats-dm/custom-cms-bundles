<?php
namespace Intpill\CmsBundle\Site\Menu;


interface MenuChildBuilderInterface
{
    public function create();
}