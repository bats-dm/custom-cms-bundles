<?php
namespace Intpill\CmsBundle\Site\Menu;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

abstract class MenuBuilderAbstract
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getEm()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    public function getPageHelper()
    {
        return $this->container->get('cms.page_helper');
    }

    public function getRouter()
    {
        return $this->container->get('router');
    }

    public function getRequestStack()
    {
        return $this->container->get('request_stack');
    }
}