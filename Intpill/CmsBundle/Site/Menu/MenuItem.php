<?php
namespace Intpill\CmsBundle\Site\Menu;


class MenuItem
{
    protected $name;
    protected $url;
    protected $blank=false;
    protected $active=false;
    protected $children = [];

    public function isBranchActive()
    {
        $isActive = function ($item) use (&$isActive) {
            if ($item->isActive()) {
                return true;
            }
            foreach ($item->getChildren() as $child) {
                if ($isActive($child)) {
                    return true;
                }
            }
            return false;
        };

        return $isActive($this);
    }

    /**
     * @param MenuItem $child
     * @return $this
     */
    public function addChild(MenuItem $child)
    {
        $this->children[] = $child;
        return $this;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     * @return MenuItem
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return MenuItem
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return MenuItem
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isBlank()
    {
        return $this->blank;
    }

    /**
     * @param boolean $blank
     * @return MenuItem
     */
    public function setBlank($blank)
    {
        $this->blank = $blank;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return MenuItem
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }
}