<?php
namespace Intpill\CmsBundle\Site\Menu;

use Intpill\CmsBundle\Site\Module\Modules;
use Intpill\CmsBundle\Site\PageHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Cmf\Component\Routing\ChainRouter as Router;
use Doctrine\ORM\EntityManager;

abstract class MenuAbstract
{
     /** @return MenuItem[] */
     abstract public function children(MenuItem $menuItem);
     /** @return MenuItem[] */
     abstract public function childrenByObject($object);

     protected $requestStack;
     protected $router;
     protected $em;
     protected $pageHelper;

     public function __construct(RequestStack $requestStack, Router $router, EntityManager $em, PageHelper $pageHelper)
     {
          $this->requestStack = $requestStack;
          $this->router = $router;
          $this->em = $em;
          $this->pageHelper = $pageHelper;
     }
}