<?php
namespace Intpill\CmsBundle\Site;


class Breadcrumbs
{
    protected $items = [];

    public function add($title, $link = null)
    {
        $this->items[] = [
            'title' => $title,
            'link' => $link
        ];
    }

    public function getItems()
    {
        return $this->items;
    }

    public function reverse()
    {
        $this->items = array_reverse($this->items);
    }
}