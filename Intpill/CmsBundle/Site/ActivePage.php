<?php
namespace Intpill\CmsBundle\Site;

use Intpill\CmsBundle\Entity\Page;
use Intpill\CmsBundle\Site\Module\Module;

class ActivePage
{
    protected $routeName;
    protected $route;
    protected $module;
    protected $request;
    protected $type;
    protected $head;
    protected $title;
    protected $keywords;
    protected $description;
    protected $content;
    protected $css;
    protected $javascript;
    protected $pageHead;
    /** @var Page */
    protected $object;
    protected $isHome;

    public function getTitleOrHead()
    {
        return $this->getTitle() ?: $this->getHead();
    }

    public function getObject()
    {
        return $this->object;
    }

    public function setObject(Page $object)
    {
        $this->object = $object;
        return $this;
    }

    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;
        return $this;
    }

    public function getRouteName()
    {
        return $this->routeName;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setHead($head)
    {
        $this->head = $head;
        return $this;
    }

    public function getHead()
    {
        return $this->head;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     * @return ActivePage
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     * @return ActivePage
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param Module $module
     * @return ActivePage
     */
    public function setModule(Module $module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * @param mixed $css
     * @return ActivePage
     */
    public function setCss($css)
    {
        $this->css = $css;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJavascript()
    {
        return $this->javascript;
    }

    /**
     * @param mixed $javascript
     * @return ActivePage
     */
    public function setJavascript($javascript)
    {
        $this->javascript = $javascript;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsHome()
    {
        return $this->isHome;
    }

    /**
     * @param mixed $isHome
     * @return ActivePage
     */
    public function setIsHome($isHome)
    {
        $this->isHome = $isHome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPageHead()
    {
        return $this->pageHead;
    }

    /**
     * @param mixed $pageHead
     * @return ActivePage
     */
    public function setPageHead($pageHead)
    {
        $this->pageHead = $pageHead;
        return $this;
    }
}