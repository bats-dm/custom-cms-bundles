<?php
namespace Intpill\CmsBundle\Site;

use Intpill\CmsBundle\Entity\Page;
use Intpill\CmsBundle\Site\Module\Modules;
use Intpill\CmsBundle\Text\Declension;
use Symfony\Cmf\Component\Routing\ChainRouter as Router;

class PageHelper
{
    const ROUTE_PREFIX = 'page_';

    protected $modules;
    protected $activePage;
    protected $declension;

    public function __construct(Modules $modules, ActivePage $activePage, Declension $declension)
    {
        $this->modules = $modules;
        $this->activePage = $activePage;
        $this->declension = $declension;
    }

    public function routeName(Page $page)
    {
        $route = false;

        if ($page->getType() == Page::TYPE_PAGE) {
            $route = $page->getId();
        } elseif ($page->getType() == Page::TYPE_MODULE) {
            if (($module = $this->modules->find($page->getModule())) && !$module->getFirstRoute()->getParams()) {
                $route = $module->getFirstRoute()->getName();
            }
        }

        return $route ? static::ROUTE_PREFIX . $route : false;
    }

    public function moduleSeoGroups(Page $page)
    {
        $result = [];
        if ($module = $this->modules->find($page->getModule())) {
            foreach ($module->getRoutes() as $route) {
                if ($route->getTitle()) {
                    $result[] = [
                        'name' => $route->getName(),
                        'title' => $route->getTitle(),
                        'vars' => $route->getSeoVars()
                    ];
                }
            }
        }
        return $result;
    }

    public function replaceSeoVars($value, $page, $groupName, $object)
    {
        foreach($this->moduleSeoGroups($page) as $group) {
            if ($group['name'] == $groupName && $group['vars']) {
                foreach ($group['vars'] as $var) {
                    if (preg_match('/%'.$var.'(\|([^%]{1,3}))?%/iu', $value, $matches)) {
                        $getter = 'get'.ucfirst($var);
                        $replace = $object->$getter();
                        if (isset($matches[2])) {
                            if ($declension = $this->declension->getOne($replace, $matches[2])) {
                                $replace = $declension;
                            }
                        }
                        $value = str_replace($matches[0], $replace, $value);
                    }
                }
            }
        }

        return $value;
    }

    public function replaceActivePageSeoVars($object, $seoGroupName)
    {
        $this->activePage->setTitle(
            $object->getTitle() ?:
            $this->replaceSeoVars($this->activePage->getTitle(), $this->activePage->getObject(), $seoGroupName, $object)
        );
        $this->activePage->setKeywords(
            $object->getMetaKeywords() ?:
            $this->replaceSeoVars($this->activePage->getKeywords(), $this->activePage->getObject(), $seoGroupName, $object)
        );
        $this->activePage->setDescription(
            $object->getMetaDescription() ?:
            $this->replaceSeoVars($this->activePage->getDescription(), $this->activePage->getObject(), $seoGroupName, $object)
        );
    }
}