<?php

namespace Intpill\CmsBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Intpill\CmsBundle\Site\ActivePage;
use Intpill\CmsBundle\Entity\Page as EntityPage;
use Intpill\CmsBundle\Site\Module\Modules;
use Intpill\CmsBundle\Site\PageHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Page
{
    protected $em;
    protected $page;
    protected $pageHelper;
    protected $modules;

    public function __construct(EntityManager $em, ActivePage $page, Modules $modules, PageHelper $pageHelper)
    {
        $this->em = $em;
        $this->page = $page;
        $this->modules = $modules;
        $this->pageHelper = $pageHelper;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $routeName = $request->get('_route');

        if (strpos($routeName, PageHelper::ROUTE_PREFIX) === 0) {
            $route = substr($routeName, strlen(PageHelper::ROUTE_PREFIX));
            $module = $this->modules->findByRoute($route);

            if (
                ($object = $this->em->getRepository('CmsBundle:Page')->find($route))
                ||
                ($module && ($object = $this->em->getRepository('CmsBundle:Page')->findOneBy(['module'=>$module->getName()])))
            ) {
                $this->page
                    ->setObject($object)
                    ->setType($object->getType())
                    ->setContent($object->getContent())
                    ->setCss($object->getCss())
                    ->setJavascript($object->getJavascript())
                    ->setPageHead($object->getPageHead())
                    ->setRouteName($routeName)
                    ->setRoute($route)
                    ->setRequest($request)
                    ->setHead($object->getName())
                    ->setTitle($object->getTitle())
                    ->setKeywords($object->getMetaKeywords())
                    ->setDescription($object->getMetaDescription())
                    ->setIsHome($object->getIsHome())
                ;
                if ($module) {
                    if ($seo = $object->getSeoByName($route)) {
                        $this->page
                            ->setTitle($seo->getTitle())
                            ->setKeywords($seo->getMetaKeywords())
                            ->setDescription($seo->getMetaDescription())
                        ;
                    }
                    $this->page->setModule($module);
                }
            }
        }
    }
}