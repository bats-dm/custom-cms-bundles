<?php
namespace Intpill\CmsBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Intpill\CmsBundle\Entity\Page AS EntityPage;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\TranslatorInterface;

class HomePage
{
    protected $session;
    protected $translator;

    public function __construct(Session $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->setHomePage($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->setHomePage($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->checkHomePage($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->checkHomePage($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->checkHomePage($args);
    }

    public function checkHomePage(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!($entity instanceof EntityPage)) {
            return;
        }

        $em = $args->getEntityManager();

        $hasHome = false;
        $pages = $em->getRepository('CmsBundle:Page')->findBy(['enabled' => true]);
        foreach ($pages as $item) {
            if ($item->getIsHome()) {
                $hasHome = true;
                break;
            }
        }

        if (!$hasHome) {
            $this->session->getFlashBag()->add('error', $this->translator->trans('Set home page for module or page', [], 'CmsBundle') . '!!!');
        }
    }

    public function setHomePage(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!($entity instanceof EntityPage)) {
            return;
        }

        $em = $args->getEntityManager();

        if ($entity->getIsHome()) {
            $em->createQuery('UPDATE CmsBundle:Page p SET p.isHome=0 WHERE p.id<>:id')->setParameter('id', $entity->getId())->execute();
        }
    }
}