<?php
namespace Intpill\CmsBundle\EventListener\Uploader;


use Behat\Transliterator\Transliterator;

class EntityHandler
{
    protected $config;
    protected $webRoot;
    protected $entity;
    protected $entityConfig;

    public function __construct($config, $webRoot, $entity)
    {
        $this->config = $config;
        $this->webRoot = $webRoot;
        $this->find($entity);
    }

    public function hasEntity()
    {
        return $this->entity ? true : false;
    }

    protected function find($entity)
    {
        foreach ($this->config as $configEntity => $entityConfig) {
            if ($entity instanceof $configEntity) {
                $this->entity = $entity;
                $this->entityConfig = $entityConfig;
                return true;
            }
        }
        return false;
    }

    protected function buildFileHandler($fieldConfig, $field)
    {
        if (isset($fieldConfig['image_options'])) {
            return (new ImageHandler($fieldConfig, $this->webRoot, $this->entity, $field));
        } else {
            return (new FileHandler($fieldConfig, $this->webRoot, $this->entity, $field));
        }
    }

    public function removeFiles()
    {
        foreach ($this->entityConfig['fields'] as $field => $fieldConfig) {
            $fileHandler = $this->buildFileHandler($fieldConfig, $field);
            $fileHandler->remove();
        }
    }

    public function uploadFiles()
    {
        foreach ($this->entityConfig['fields'] as $field=>$fieldConfig) {
            $fileHandler = $this->buildFileHandler($fieldConfig, $field);
            $fileHandler->upload($this->getFileName());
        }
    }

    protected function getFileName()
    {
        $fileName = '';
        if (isset($this->entityConfig['template'])) {
            $fileName = $this->entityConfig['template'];
            preg_match_all('/{([^{}]+)}/', $this->entityConfig['template'], $matches);
            foreach ($matches[1] as $index => $entityFields) {
                $search = $matches[0][$index];
                $replace = $this->entity;
                foreach (explode('.', $entityFields) as $entityField) {
                    $getter = 'get'.ucfirst($entityField);
                    $replace = $replace->$getter();
                }
                $replace = Transliterator::transliterate($replace);
                $replace = strtolower(substr($replace, 0, 100));
                $fileName = str_replace($search, $replace, $fileName);
            }
        }

        if (!$fileName) {
            $fileName = uniqid();
        }

        return $fileName;
    }
}