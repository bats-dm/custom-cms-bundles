<?php
namespace Intpill\CmsBundle\EventListener\Uploader;

use Imagine\Image\ImageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagine\Gd\Imagine;
use Imagine\Filter\Transformation;
use Imagine\Image\Box;

class FileHandler
{
    protected $fieldConfig;
    protected $webRoot;
    protected $entity;
    protected $field;
    protected $fileName;
    protected $fileExtension;

    public function __construct($fieldConfig, $webRoot, $entity, $field)
    {
        $this->fieldConfig = $fieldConfig;
        $this->webRoot = $webRoot;
        $this->entity = $entity;
        $this->field = $field;
    }

    public function upload($fileName)
    {
        $this->fileName = $fileName;

        if (($file = $this->getEntityUploadedField()) and ($file instanceof UploadedFile)) {
            $this->setEntityFieldSize($file->getSize());
            $this->setEntityFieldExtension($file->guessExtension());

            $this->createDir();

            if ($this->getEntityField()) {
                $this->remove();
            }

            $this->fileExtension = strtolower($file->guessExtension());

            $this->process();

            $this->setEntityField($this->getFileFullName());
            $this->setEntityUploadedField('');
        }
    }

    public function process()
    {
        $this->getEntityUploadedField()->move($this->webRoot . $this->fieldConfig['dir'], $this->getFileFullName());
    }

    public function createDir()
    {
        if (!file_exists($this->webRoot . $this->fieldConfig['dir'])) {
            mkdir($this->webRoot . $this->fieldConfig['dir'], 0777, true);
        }
    }

    protected function getFileFullName()
    {
        return $this->fileName . '.' . $this->fileExtension;
    }

    /** @return UploadedFile  */
    public function getEntityUploadedField()
    {
        return $this->entity->{'getUploaded' . ucfirst($this->field)}();
    }

    public function setEntityUploadedField($value)
    {
        return $this->entity->{'setUploaded' . ucfirst($this->field)}($value);
    }

    public function getEntityField()
    {
        return $this->entity->{'get' . ucfirst($this->field)}();
    }

    public function setEntityField($value)
    {
        return $this->entity->{'set' . ucfirst($this->field)}($value);
    }

    public function setEntityFieldSize($value)
    {
        $method = 'set'.ucfirst($this->field).'Size';
        if (method_exists($this->entity, $method)) {
            $this->entity->$method($value);
        }
    }

    public function setEntityFieldExtension($value)
    {
        $method = 'set'.ucfirst($this->field).'Extension';
        if (method_exists($this->entity, $method)) {
            $this->entity->$method($value);
        }
    }

    public function remove()
    {
        @unlink($this->webRoot . $this->fieldConfig['dir'] . $this->getEntityField());
    }
}