<?php
namespace Intpill\CmsBundle\EventListener\Uploader;

use Imagine\Image\ImageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagine\Gd\Imagine;
use Imagine\Filter\Transformation;
use Imagine\Image\Box;

class ImageHandler extends FileHandler
{
    public function process()
    {
        $this->processImage($this->getFileFullName(), $this->fieldConfig);

        if (isset($this->fieldConfig['extra_fields'])) {
            foreach ($this->fieldConfig['extra_fields'] as $fieldSuffix => $extraConfig) {
                $fileFullName = $this->fileName.'-'.$fieldSuffix.'.'.$this->fileExtension;
                $this->processImage($fileFullName, $extraConfig);
                $this->entity->{'set'.ucfirst($this->field).ucfirst($fieldSuffix)}($fileFullName);
            }
        }
    }

    protected function processImage($fileName, $config)
    {
        $imagine = (new Imagine())->open($this->getEntityUploadedField()->getPathname());

        $transformation = new Transformation();
        $transformation
            ->thumbnail(
                new Box($config['image_options']['width'], $config['image_options']['height']),
                empty($config['image_options']['outbound']) ? ImageInterface::THUMBNAIL_INSET : ImageInterface::THUMBNAIL_OUTBOUND
            )
            ->save($this->webRoot . $this->fieldConfig['dir'] . $fileName);
        $transformation->apply($imagine);
    }
}