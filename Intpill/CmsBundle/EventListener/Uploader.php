<?php

namespace Intpill\CmsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Intpill\CmsBundle\EventListener\Uploader\EntityHandler;

class Uploader implements EventSubscriber
{
    protected $config;
    protected $webRoot;

    public function __construct($config, $webRoot)
    {
        $this->config = $config;
        $this->webRoot = $webRoot;
    }

    public function getSubscribedEvents()
    {
        return [
            'postUpdate',
            'postPersist',
            'preRemove'
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->upload($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->upload($args);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entityHandler = $this->buildEntityHandler($args->getEntity());
        if ($entityHandler->hasEntity()) {
            $entityHandler->removeFiles();
        }
    }

    protected function upload(LifecycleEventArgs $args)
    {
        $entityHandler = $this->buildEntityHandler($args->getEntity());
        if ($entityHandler->hasEntity()) {
            $entityHandler->uploadFiles();
            $args->getEntityManager()->flush();
        }
    }

    protected function buildEntityHandler($entity)
    {
        return (new EntityHandler($this->config, $this->webRoot, $entity));
    }
}