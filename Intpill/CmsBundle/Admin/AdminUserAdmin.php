<?php

namespace Intpill\CmsBundle\Admin;

use Intpill\CmsBundle\Entity\AdminUser;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AdminUserAdmin extends AbstractAdmin
{
    protected $roles;
    protected $passwordEncoder;

    public function getSecurity()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    public function setPasswordEncoder(UserPasswordEncoder $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('roleName', null, [
                'parameters' => [
                    $this->getConfigurationPool()->getContainer()->getParameter('cms.admin_roles')
                ]
            ])
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('new_password', PasswordType::class, [
                'required' => false
            ])
            ->add('role', 'choice', ['choices' => $this->roles])
            ->add('enabled')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('username')
            ->add('roleName', null, [
                    'parameters' => [
                    $this->getConfigurationPool()->getContainer()->getParameter('cms.admin_roles')
                ]
            ])
            ->add('enabled')
        ;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        if (!$this->getSecurity()->isGranted('ROLE_ADMIN_DEV')) {
            $query->andWhere($query->getRootAliases()[0] . ".username!=:admin_dev");
            $query->setParameter('admin_dev', AdminUser::ADMIN_DEV);
        }
        return $query;
    }

    public function preUpdate($object)
    {
        $this->setNewPassword($object);
    }

    public function prePersist($object)
    {
        $this->setNewPassword($object);
    }

    protected function setNewPassword(AdminUser $object)
    {
        if ($object->getNewPassword()) {
            $object->setPassword(
                $this->passwordEncoder->encodePassword($object, $object->getNewPassword())
            );
        }
    }
}
