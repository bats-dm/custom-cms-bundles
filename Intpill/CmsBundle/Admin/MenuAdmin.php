<?php

namespace Intpill\CmsBundle\Admin;

use Intpill\CmsBundle\Entity\Menu;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Knp\Menu\ItemInterface as MenuItemInterface;

class MenuAdmin extends AbstractAdmin
{
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('tree-move');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('Doctrine');
        $subject = $this->getSubject();

        $parentOptions = [
            'query_builder' => function (EntityRepository $er) use($subject) {
                $qb = $er->createQueryBuilder('p');
                $qb->orderBy('p.name', 'ASC');
                if ($subject->getId()) {
                    $qb->where('p.id<>:id')->setParameter('id', $subject->getId());
                }
                return $qb;
            }
        ];

        if (!$subject->getId() && ($pid = $this->getRequest()->get('parent_id'))) {
            $parentOptions['data'] = $doctrine->getRepository('CmsBundle:Menu')->find($pid);
        }

        $formMapper
            ->add('name')
            ->add('parent', null, $parentOptions)
            ->add('type', ChoiceType::class, [
                'choices' => array($this->trans('Page') => Menu::TYPE_PAGE, $this->trans('Link') => Menu::TYPE_LINK),
                'attr' => [
                    'class' => 'menu-type'
                ]
            ])
            ->add('page', null, [
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                'attr' => [
                    'class' => 'menu-types menu-page'
                ]
            ])
            ->add('link', null, [
                'attr' => [
                    'class' => 'menu-types menu-link'
                ]
            ])
            ->add('target_blank', null, [
                'label' => 'In new window'
            ])
            ->add('enabled')
        ;
    }
}
