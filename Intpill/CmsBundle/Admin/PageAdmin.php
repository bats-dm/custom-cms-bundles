<?php

namespace Intpill\CmsBundle\Admin;

use Intpill\CmsBundle\Entity\Page;
use Intpill\CmsBundle\Entity\PageSeo;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class PageAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    public function getSecurity()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
    }

    public function getConfigChildren()
    {
        return $this->getConfigurationPool()->getContainer()->getParameter('cms.page_children');
    }

    public function configure()
    {
        foreach ($this->getConfigChildren() as $child) {
            $this->addChild($this->getConfigurationPool()->getContainer()->get($child['service']));
        }
        $this->listModes['tree'] = [
            'class' => 'fa fa-tree fa-fw',
        ];
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ($children = $this->getConfigChildren()) {
            if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
                return;
            }

            $admin = $this->isChild() ? $this->getParent() : $this;
            $id = $admin->getRequest()->get('id');

            $menu->addChild(
                $this->trans('Page'),
                array('uri' => $admin->generateUrl('edit', array('id' => $id)))
            );
            foreach ($children as $child) {
                $menu->addChild(
                    $child['name'],
                    array('uri' => $admin->generateUrl($child['service'] . '.list', array('id' => $id)))
                );
            }
        }
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('slug')
            ->add('name')
            ->add('parent', null, [
                'label' => 'Родитель'
            ], null, [
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                }
            ])
            ->add('enabled');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('slug')
            ->add('name')
            ->add('parent.name')
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('Doctrine');
        $pageHelper = $this->getConfigurationPool()->getContainer()->get('cms.page_helper');
        /** @var Page $subject */
        $subject = $this->getSubject();

        $parentOptions = [
            'query_builder' => function (EntityRepository $er) use($subject) {
                $qb = $er->createQueryBuilder('p');
                $qb->orderBy('p.name', 'ASC');
                if ($subject->getId()) {
                    $qb->where('p.id<>:id')->setParameter('id', $subject->getId());
                }
                return $qb;
            }
        ];

        if (!$subject->getId() && ($pid = $this->getRequest()->get('parent_id'))) {
            $parentOptions['data'] = $doctrine->getRepository('CmsBundle:Page')->find($pid);
        }

        $formMapper
            ->tab('General')
                ->with('Content')
                    ->add('name')
                    ->add('slug', null, [
                        'required' => false
                    ])
                    ->add('parent', null, $parentOptions)
        ;
        if (!$subject || $subject->getType() == Page::TYPE_PAGE) {
            $formMapper
                ->add('content', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full',
                        'data-item' => 'page-content'
                    ],
                    'required' => false
                ])
                ->add('css')
                ->add('javascript')
                ->add('pageHead')
            ;
        }
        $formMapper
                    ->add('isHome')
                    ->add('enabled')
                ->end()
            ->end()
        ;
        $formMapper
            ->tab('SEO')
        ;
        if ($subject && $subject->getType() == Page::TYPE_MODULE) {
            foreach ($pageHelper->moduleSeoGroups($subject) as $seoGroup) {
                $seoPage = $subject->getSeoByName($seoGroup['name']);
                $varsHint = '';
                if ($seoGroup['vars']) {
                    $varsHint = ' / Переменные: ';
                    foreach ($seoGroup['vars'] as $var) {
                        $varsHint .= '%'.$var.'%,';
                    }
                    $varsHint = substr($varsHint, 0, strlen($varsHint)-1);
                    $varsHint .= ' / Для склонения, перед закрывающимся символом % после | использовать первую большую русскую букву падежа, например: %var|Д%';
                }
                $formMapper
                    ->with($seoGroup['title'].$varsHint, ['class' => 'col-md-6'])
                        ->add('title-'.$seoGroup['name'], 'textarea', [
                            'label' => 'Title',
                            'mapped' => false,
                            'required' => false,
                            'data' => $seoPage ? $seoPage->getTitle() : ''
                        ])
                        ->add('metaKeywords-'.$seoGroup['name'], 'textarea', [
                            'label' => 'Meta Keywords',
                            'mapped' => false,
                            'required' => false,
                            'data' => $seoPage ? $seoPage->getMetaKeywords() : ''
                        ])
                        ->add('metaDescription-'.$seoGroup['name'], 'textarea', [
                            'label' => 'Meta Description',
                            'mapped' => false,
                            'required' => false,
                            'data' => $seoPage ? $seoPage->getMetaDescription() : ''
                        ])
                        ->add('sitemapPriority-'.$seoGroup['name'], 'textarea', [
                            'label' => 'Sitemap Priority',
                            'mapped' => false,
                            'required' => false,
                            'data' => $seoPage ? $seoPage->getSitemapPriority() : ''
                        ])
                    ->end()
                ;
            }
        } else {
            $formMapper
                ->with('')
                    ->add('title')
                    ->add('metaKeywords')
                    ->add('metaDescription')
                    ->add('sitemapPriority')
                ->end()
            ;
        }
        $formMapper
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('slug')
            ->add('name')
            ->add('title')
            ->add('metaKeywords')
            ->add('metaDescription')
            ->add('enabled');
    }

    public function postUpdate($object)
    {
        $this->moveImages($object);
        $this->saveSeo($object);
    }

    public function postPersist($object)
    {
        $this->moveImages($object);
    }

    public function preRemove($object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->remove(
            $this->getItemName($object->getId())
        );
    }

    protected function saveSeo(Page $page)
    {
        if ($page && $page->getType() == Page::TYPE_MODULE) {
            //$pageHelper = $this->getConfigurationPool()->getContainer()->get('cms.page_helper');
            $seoFields = ['title', 'metaKeywords', 'metaDescription', 'sitemapPriority'];
            $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
            $repo = $em->getRepository('CmsBundle:Page');

            foreach ($page->getSeo() as $seo) {
                $em->remove($seo);
            }
            $em->flush();

            $seoData = [];
            foreach ($this->getForm()->all() as $field=>$formData) {
                foreach ($seoFields as $seoField) {
                    $formSeoField = $seoField . '-';
                    if (strpos($field, $formSeoField) === 0) {
                        $group = str_replace($formSeoField, '', $field);
                        if (!isset($seoData[$group])) {
                            $seoData[$group] = [];
                        }
                        $seoData[$group][$seoField] = $formData->getData();
                    }
                }
            }
            foreach ($seoData as $group=>$seoDataItem) {
                $pageSeo = (new PageSeo())->setName($group)->setPage($page);
                foreach ($seoFields as $seoField) {
                    $setter = 'set'.ucfirst($seoField);
                    $pageSeo->$setter($seoDataItem[$seoField]);
                }
                $em->persist($pageSeo);
            }
            $em->flush();
        }
    }

    public function getItemName($objectId)
    {
        return 'page_content_' . $objectId;
    }

    public function moveImages($object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->move(
            $object->getContent(),
            $this->getItemName($object->getId())
        );
    }
}
