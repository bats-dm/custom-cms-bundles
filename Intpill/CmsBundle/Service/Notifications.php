<?php
namespace Intpill\CmsBundle\Service;


class Notifications
{
    /** @var Notification[] */
    protected $items = [];

    public function add(Notification $notification)
    {
        $this->items[] = $notification;
    }

    public function getItems()
    {
        return $this->items;
    }
}