<?php
namespace Intpill\CmsBundle\Service;

/*
 * colors: red, yellow, aqua, blue, black, light-blue, green, gray, navy, teal, olive, lime, orange, fuchsia, purple, maroon
 */

class Notification
{
    const LABEL_ERROR = 'error';
    const LABEL_WARNING = 'warning';
    const LABEL_SUCCESS = 'success';
    const TYPE_BASIC = 'basic';
    const TYPE_DROPDOWN = 'dropdown';

    protected $icon = 'bell-o';
    protected $color = 'black';
    protected $label = self::LABEL_WARNING;
    protected $type = self::TYPE_DROPDOWN;
    protected $items = [];

    /**
     * @param $title
     * @param string $icon
     * @param string $link
     * @param string $color
     */
    public function add($title, $icon='', $link='', $color = 'black', $consider = true)
    {
        $this->items[] = [
            'title' => $title,
            'icon' => $icon,
            'link' => $link,
            'color' => $color,
            'consider' => $consider
        ];
    }

    public function getItems()
    {
        return $this->items;
    }

    public function total()
    {
        $count = 0;
        foreach ($this->items as $item) {
            if ($item['consider']) {
                $count ++;
            }
        }
        return $count;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return Notification
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Notification
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Notification
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }
}