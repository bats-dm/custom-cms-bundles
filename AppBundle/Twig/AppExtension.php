<?php

namespace AppBundle\Twig;

use AppBundle\Menu\LeftMenu;
use AppBundle\Menu\LeftMenuItem;
use AppBundle\Order\Cart;

class AppExtension extends \Twig_Extension
{
    protected $cart;
    protected $leftMenu;

    public function __construct(Cart $cart, LeftMenu $leftMenu)
    {
        $this->cart = $cart;
        $this->leftMenu = $leftMenu;
    }

    public function getGlobals()
    {
        return [
            'app_cart' => $this->cart,
            'app_left_menu' => $this->leftMenu
        ];
    }

    public function getName()
    {
        return 'app_extension';
    }
}