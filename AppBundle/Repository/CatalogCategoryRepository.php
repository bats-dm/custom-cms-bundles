<?php

namespace AppBundle\Repository;
use AppBundle\Entity\CatalogCategory;
use AppBundle\Entity\CatalogCategoryType;
use AppBundle\Entity\CatalogProductParam;
use AppBundle\Entity\CatalogProductParamValue;
use AppBundle\Entity\CatalogProductsParamValues;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * CatalogCategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CatalogCategoryRepository extends NestedTreeRepository
{
    /**
     * @param CatalogCategoryType $categoryType
     * @return CatalogCategory[]
     */
    public function categoriesByType(CatalogCategoryType $categoryType)
    {
        if ($categoryType->getId() == CatalogCategoryType::SEEDS_ID) {
            /** @var CatalogCategory $rootSeedsCategory */
            $rootSeedsCategory = $this->findOneBy(['type'=>$categoryType, 'parent'=>null]);
            return $this->children($rootSeedsCategory, true);
        }

        return $this->findBy(['type'=>$categoryType, 'parent'=>null, 'enabled'=>true], ['lft'=>'ASC']);
    }

    /**
     * @param CatalogCategory $category
     * @return CatalogProductParam[]
     */
    public function productParams($category)
    {
        $em = $this->getEntityManager();

        $params = [];
        if ($category) {
            foreach ($category->getProductParams() as $param) {
                $params[] = $param;
            }
            if ($type = $category->getRootCategory()->getType()) {
                foreach ($type->getProductParams() as $param) {
                    $params[] = $param;
                }
            }
        }

        return $em->getRepository('AppBundle:CatalogProductParam')->findBy(['id' => $params]);
    }

    /**
     * @param CatalogProductParamValue $paramValue
     * @return CatalogCategory[]
     */
    public function findByParamValue(CatalogProductParamValue $paramValue)
    {
        $em = $this->getEntityManager();

        $qb = $this->createQueryBuilder('c');
        $qbPV = $em->getRepository('AppBundle:CatalogProductsParamValues')->createQueryBuilder('pv');

        return $qb
            ->where(
                $qb->expr()->in(
                    'c.id',
                    $qbPV
                        ->select('IDENTITY(p.category)')
                        ->join('pv.product', 'p')
                        ->where('pv.paramValue=:paramValue')
                        ->getDQL()
                )
            )
            ->setParameter('paramValue', $paramValue)
            ->getQuery()
            ->getResult()
        ;
    }
}
