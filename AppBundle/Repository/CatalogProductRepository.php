<?php

namespace AppBundle\Repository;
use AppBundle\Entity\CatalogProduct;
use AppBundle\Entity\CatalogProductParam;
use AppBundle\Entity\CatalogProductParamValue;

/**
 * CatalogProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CatalogProductRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param CatalogProduct $product
     * @return CatalogProductParam[]
     */
    public function params(CatalogProduct $product)
    {
        $em = $this->getEntityManager();

        return $em->getRepository('AppBundle:CatalogCategory')->productParams($product->getCategory());
    }

    /**
     * @param CatalogProduct $product
     * @return CatalogProduct[]
     */
    public function findJointlyPurchasedAll(CatalogProduct $product)
    {
        $em = $this->getEntityManager();
        $result = [];

        if ($product->getCategory() && $product->getBookings()) {
            foreach ($product->getBookings() as $booking) {
                foreach ($booking->getProducts() as $bookingProduct) {
                    if (
                        $bookingProduct->getEnabled() &&
                        $bookingProduct->getCategory() &&
                        $bookingProduct->getCategory() == $product->getCategory() &&
                        $bookingProduct != $product
                    ) {
                        $result[$bookingProduct->getId()] = $bookingProduct;
                    }
                }
            }
        }

        return array_values($result);
    }

    public function findByParamValue(CatalogProductParamValue $paramValue)
    {
        $em = $this->getEntityManager();

        $qb = $this->createQueryBuilder('p');
        $qbPV = $em->getRepository('AppBundle:CatalogProductsParamValues')->createQueryBuilder('pv');

        return $qb
            ->where(
                $qb->expr()->in(
                    'p.id',
                    $qbPV
                        ->select('IDENTITY(pv.product)')
                        ->where('pv.paramValue=:paramValue')
                        ->getDQL()
                )
            )
            ->andWhere('p.enabled=true')
            ->setParameter('paramValue', $paramValue)
            ->getQuery()
            ->getResult()
            ;
    }
}
