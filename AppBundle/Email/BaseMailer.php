<?php
namespace AppBundle\Email;

/**
 * App Base Mailer
 */
class BaseMailer
{
    const FROM = 'bayer-mailer@yandex.ru';
    const NAME = 'Crop Science Россия';

    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send($subject, $email, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(self::FROM, self::NAME)
            ->setTo($email)
            ->setBody($body, 'text/html');
        $res = $this->mailer->send($message);

        return $res;
    }
}