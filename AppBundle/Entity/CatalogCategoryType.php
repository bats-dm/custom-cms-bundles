<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatalogCategoryType
 *
 * @ORM\Table(name="app_catalog_category_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatalogCategoryTypeRepository")
 */
class CatalogCategoryType
{
    const SZR_ID = 1;
    const SEEDS_ID = 2;
    const SZR_BASE_PARAM_ID = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="CatalogProductParam")
     * @ORM\JoinTable(name="app_catalog_category_types_product_params")
     */
    private $productParams;

    public function getParamsRelation()
    {
        if ($this->getId() == self::SZR_ID) {
            return 'category_type';
        } else {
            return 'category';
        }
    }

    public function getBaseParamId()
    {
        if ($this->getId() == self::SZR_ID) {
            return self::SZR_BASE_PARAM_ID;
        }
        return false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CatalogCategoryType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productParams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productParam
     *
     * @param \AppBundle\Entity\CatalogProductParam $productParam
     *
     * @return CatalogCategoryType
     */
    public function addProductParam(\AppBundle\Entity\CatalogProductParam $productParam)
    {
        $this->productParams[] = $productParam;

        return $this;
    }

    /**
     * Remove productParam
     *
     * @param \AppBundle\Entity\CatalogProductParam $productParam
     */
    public function removeProductParam(\AppBundle\Entity\CatalogProductParam $productParam)
    {
        $this->productParams->removeElement($productParam);
    }

    /**
     * Get productParams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductParams()
    {
        return $this->productParams;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
