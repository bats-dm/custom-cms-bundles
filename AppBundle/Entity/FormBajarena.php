<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FormBajarena
 *
 * @ORM\Table(name="app_form_bajarena")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormBajarenaRepository")
 */
class FormBajarena extends Form
{
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="field_activity", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $fieldActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="company_type", type="string", length=255, nullable=true)
     */
    private $companyType;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="post", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $post;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="post_address", type="string", length=255, nullable=true)
     */
    private $postAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_code", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $phoneCode;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_email", type="string", length=255, nullable=true)
     * @Assert\Email()
     */
    private $additionalEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="sown_area", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $sownArea;

    /**
     * @var string
     *
     * @ORM\Column(name="sugar_beet", type="string", length=255, nullable=true)
     */
    private $sugarBeet;

    /**
     * @var string
     *
     * @ORM\Column(name="winter_wheat", type="string", length=255, nullable=true)
     */
    private $winterWheat;

    /**
     * @var string
     *
     * @ORM\Column(name="spring_barley", type="string", length=255, nullable=true)
     */
    private $springBarley;

    /**
     * @var string
     *
     * @ORM\Column(name="potatoes", type="string", length=255, nullable=true)
     */
    private $potatoes;

    /**
     * @var string
     *
     * @ORM\Column(name="corn", type="string", length=255, nullable=true)
     */
    private $corn;

    /**
     * @var string
     *
     * @ORM\Column(name="rape", type="string", length=255, nullable=true)
     */
    private $rape;

    /**
     * @var string
     *
     * @ORM\Column(name="sunflower", type="string", length=255, nullable=true)
     */
    private $sunflower;

    /**
     * @var string
     *
     * @ORM\Column(name="other_cultures", type="string", length=255, nullable=true)
     */
    private $otherCultures;

    /**
     * @var string
     *
     * @ORM\Column(name="treatment_equipment", type="string", length=255, nullable=true)
     */
    private $treatmentEquipment;

    /**
     * @var string
     *
     * @ORM\Column(name="magazines", type="string", length=255, nullable=true)
     */
    private $magazines;


    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return FormBajarena
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return FormBajarena
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set fieldActivity
     *
     * @param string $fieldActivity
     *
     * @return FormBajarena
     */
    public function setFieldActivity($fieldActivity)
    {
        $this->fieldActivity = $fieldActivity;

        return $this;
    }

    /**
     * Get fieldActivity
     *
     * @return string
     */
    public function getFieldActivity()
    {
        return $this->fieldActivity;
    }

    /**
     * Set companyType
     *
     * @param string $companyType
     *
     * @return FormBajarena
     */
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get companyType
     *
     * @return string
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return FormBajarena
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return FormBajarena
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     *
     * @return FormBajarena
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set post
     *
     * @param string $post
     *
     * @return FormBajarena
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return FormBajarena
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set postAddress
     *
     * @param string $postAddress
     *
     * @return FormBajarena
     */
    public function setPostAddress($postAddress)
    {
        $this->postAddress = $postAddress;

        return $this;
    }

    /**
     * Get postAddress
     *
     * @return string
     */
    public function getPostAddress()
    {
        return $this->postAddress;
    }

    /**
     * Set phoneCode
     *
     * @param string $phoneCode
     *
     * @return FormBajarena
     */
    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;

        return $this;
    }

    /**
     * Get phoneCode
     *
     * @return string
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return FormBajarena
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set additionalEmail
     *
     * @param string $additionalEmail
     *
     * @return FormBajarena
     */
    public function setAdditionalEmail($additionalEmail)
    {
        $this->additionalEmail = $additionalEmail;

        return $this;
    }

    /**
     * Get additionalEmail
     *
     * @return string
     */
    public function getAdditionalEmail()
    {
        return $this->additionalEmail;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return FormBajarena
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set sownArea
     *
     * @param string $sownArea
     *
     * @return FormBajarena
     */
    public function setSownArea($sownArea)
    {
        $this->sownArea = $sownArea;

        return $this;
    }

    /**
     * Get sownArea
     *
     * @return string
     */
    public function getSownArea()
    {
        return $this->sownArea;
    }

    /**
     * Set sugarBeet
     *
     * @param string $sugarBeet
     *
     * @return FormBajarena
     */
    public function setSugarBeet($sugarBeet)
    {
        $this->sugarBeet = $sugarBeet;

        return $this;
    }

    /**
     * Get sugarBeet
     *
     * @return string
     */
    public function getSugarBeet()
    {
        return $this->sugarBeet;
    }

    /**
     * Set winterWheat
     *
     * @param string $winterWheat
     *
     * @return FormBajarena
     */
    public function setWinterWheat($winterWheat)
    {
        $this->winterWheat = $winterWheat;

        return $this;
    }

    /**
     * Get winterWheat
     *
     * @return string
     */
    public function getWinterWheat()
    {
        return $this->winterWheat;
    }

    /**
     * Set springBarley
     *
     * @param string $springBarley
     *
     * @return FormBajarena
     */
    public function setSpringBarley($springBarley)
    {
        $this->springBarley = $springBarley;

        return $this;
    }

    /**
     * Get springBarley
     *
     * @return string
     */
    public function getSpringBarley()
    {
        return $this->springBarley;
    }

    /**
     * Set potatoes
     *
     * @param string $potatoes
     *
     * @return FormBajarena
     */
    public function setPotatoes($potatoes)
    {
        $this->potatoes = $potatoes;

        return $this;
    }

    /**
     * Get potatoes
     *
     * @return string
     */
    public function getPotatoes()
    {
        return $this->potatoes;
    }

    /**
     * Set corn
     *
     * @param string $corn
     *
     * @return FormBajarena
     */
    public function setCorn($corn)
    {
        $this->corn = $corn;

        return $this;
    }

    /**
     * Get corn
     *
     * @return string
     */
    public function getCorn()
    {
        return $this->corn;
    }

    /**
     * Set rape
     *
     * @param string $rape
     *
     * @return FormBajarena
     */
    public function setRape($rape)
    {
        $this->rape = $rape;

        return $this;
    }

    /**
     * Get rape
     *
     * @return string
     */
    public function getRape()
    {
        return $this->rape;
    }

    /**
     * Set sunflower
     *
     * @param string $sunflower
     *
     * @return FormBajarena
     */
    public function setSunflower($sunflower)
    {
        $this->sunflower = $sunflower;

        return $this;
    }

    /**
     * Get sunflower
     *
     * @return string
     */
    public function getSunflower()
    {
        return $this->sunflower;
    }

    /**
     * Set otherCultures
     *
     * @param string $otherCultures
     *
     * @return FormBajarena
     */
    public function setOtherCultures($otherCultures)
    {
        $this->otherCultures = $otherCultures;

        return $this;
    }

    /**
     * Get otherCultures
     *
     * @return string
     */
    public function getOtherCultures()
    {
        return $this->otherCultures;
    }

    /**
     * Set treatmentEquipment
     *
     * @param string $treatmentEquipment
     *
     * @return FormBajarena
     */
    public function setTreatmentEquipment($treatmentEquipment)
    {
        $this->treatmentEquipment = $treatmentEquipment;

        return $this;
    }

    /**
     * Get treatmentEquipment
     *
     * @return string
     */
    public function getTreatmentEquipment()
    {
        return $this->treatmentEquipment;
    }

    /**
     * Set magazines
     *
     * @param string $magazines
     *
     * @return FormBajarena
     */
    public function setMagazines($magazines)
    {
        $this->magazines = $magazines;

        return $this;
    }

    /**
     * Get magazines
     *
     * @return string
     */
    public function getMagazines()
    {
        return $this->magazines;
    }
}
