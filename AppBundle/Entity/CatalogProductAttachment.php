<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * CatalogProductAttachment
 *
 * @ORM\Entity()
 * @ORM\Table(name="app_catalog_product_attachment")
 */
class CatalogProductAttachment extends Attachment
{
    /**
     * @ORM\ManyToOne(targetEntity="CatalogProduct")
     */
    protected $object;

    /**
     * Set catalogProduct
     *
     * @param \AppBundle\Entity\CatalogProduct $catalogProduct
     *
     * @return CatalogProductAttachment
     */
    public function setObject(\AppBundle\Entity\CatalogProduct $object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get catalogProduct
     *
     * @return \AppBundle\Entity\CatalogProduct
     */
    public function getObject()
    {
        return $this->object;
    }
}
