<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatalogProductParam
 *
 * @ORM\Table(name="app_catalog_product_param")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatalogProductParamRepository")
 */
class CatalogProductParam
{
    const CROP_ID = 1;
    const TYPE_CHOICE = 'choice';
    const TYPE_MULTICHOICE = 'multichoice';
    const FILTER_TYPE_CHOICE_ICONS = 'choice_icons';
    const FILTER_TYPE_MULTICHOICE_LIST = 'multichoice_list';
    const FILTER_TYPE_CHOICE = 'choice';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogProductParam")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $relation;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type = self::TYPE_MULTICHOICE;

    /**
     * @var string
     *
     * @ORM\Column(name="filter_type", type="string", length=255, nullable=true)
     */
    private $filterType;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogProductParamGroup")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $group;

    /**
     * @ORM\OneToMany(targetEntity="CatalogProductParamValue", mappedBy="param")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $values;

    static function getFilterTypes()
    {
        return [
            self::FILTER_TYPE_CHOICE => 'Раскрывающийся список',
            self::FILTER_TYPE_MULTICHOICE_LIST => 'Список с множественным выбором',
            self::FILTER_TYPE_CHOICE_ICONS => 'Список с иконками',
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CatalogProductParam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CatalogProductParam
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\CatalogProductParamGroup $group
     *
     * @return CatalogProductParam
     */
    public function setGroup(\AppBundle\Entity\CatalogProductParamGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\CatalogProductParamGroup
     */
    public function getGroup()
    {
        return $this->group;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add value
     *
     * @param \AppBundle\Entity\CatalogProductParamValue $value
     *
     * @return CatalogProductParam
     */
    public function addValue(\AppBundle\Entity\CatalogProductParamValue $value)
    {
        $this->values[] = $value;

        return $this;
    }

    /**
     * Remove value
     *
     * @param \AppBundle\Entity\CatalogProductParamValue $value
     */
    public function removeValue(\AppBundle\Entity\CatalogProductParamValue $value)
    {
        $this->values->removeElement($value);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set filterType
     *
     * @param string $filterType
     *
     * @return CatalogProductParam
     */
    public function setFilterType($filterType)
    {
        $this->filterType = $filterType;

        return $this;
    }

    /**
     * Get filterType
     *
     * @return string
     */
    public function getFilterType()
    {
        return $this->filterType;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Set relation
     *
     * @param \AppBundle\Entity\CatalogProductParam $relation
     *
     * @return CatalogProductParam
     */
    public function setRelation(\AppBundle\Entity\CatalogProductParam $relation = null)
    {
        $this->relation = $relation;

        return $this;
    }

    /**
     * Get relation
     *
     * @return \AppBundle\Entity\CatalogProductParam
     */
    public function getRelation()
    {
        return $this->relation;
    }
}
