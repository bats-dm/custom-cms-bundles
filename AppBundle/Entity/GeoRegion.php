<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeoRegion
 *
 * @ORM\Table(name="app_geo_region")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GeoRegionRepository")
 */
class GeoRegion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GeoCountry")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="partner_email", type="string", length=1024, nullable=true)
     */
    private $partnerEmail;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GeoRegion
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\GeoCountry $country
     *
     * @return GeoRegion
     */
    public function setCountry(\AppBundle\Entity\GeoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\GeoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Set partnerEmail
     *
     * @param string $partnerEmail
     *
     * @return GeoRegion
     */
    public function setPartnerEmail($partnerEmail)
    {
        $this->partnerEmail = $partnerEmail;

        return $this;
    }

    /**
     * Get partnerEmail
     *
     * @return string
     */
    public function getPartnerEmail()
    {
        return $this->partnerEmail;
    }
}
