<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FormNewsletterSubscription
 *
 * @ORM\Table(name="app_form_newsletter_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormNewsletterSubscriptionRepository")
 */
class FormNewsletterSubscription extends Form
{
}
