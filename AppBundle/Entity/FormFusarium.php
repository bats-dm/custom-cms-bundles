<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FormFusarium
 *
 * @ORM\Table(name="app_form_fusarium")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormFusariumRepository")
 */
class FormFusarium extends Form
{
}

