<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeoCountry
 *
 * @ORM\Table(name="app_geo_country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GeoCountryRepository")
 */
class GeoCountry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=255, nullable=true)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="alfa2", type="string", length=255, nullable=true)
     */
    private $alfa2;

    /**
     * @var string
     *
     * @ORM\Column(name="alfa3", type="string", length=255, nullable=true)
     */
    private $alfa3;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GeoCountry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return GeoCountry
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set alfa2
     *
     * @param string $alfa2
     *
     * @return GeoCountry
     */
    public function setAlfa2($alfa2)
    {
        $this->alfa2 = $alfa2;

        return $this;
    }

    /**
     * Get alfa2
     *
     * @return string
     */
    public function getAlfa2()
    {
        return $this->alfa2;
    }

    /**
     * Set alfa3
     *
     * @param string $alfa3
     *
     * @return GeoCountry
     */
    public function setAlfa3($alfa3)
    {
        $this->alfa3 = $alfa3;

        return $this;
    }

    /**
     * Get alfa3
     *
     * @return string
     */
    public function getAlfa3()
    {
        return $this->alfa3;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }
}
