<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

/**
 * Order products
 *
 * @ORM\Table(name="app_booking")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="farm_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=40)
     */
    private $farmName;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=40)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(max=40)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=10, max=11)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @Assert\Length(max=500)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="BookingArableArea")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $arableArea;

    /**
     * @ORM\ManyToOne(targetEntity="GeoRegion")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity="BookingOrganization")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $organization;

    /**
     * @ORM\ManyToMany(targetEntity="CatalogProductParamValue")
     * @ORM\JoinTable(name="app_bookings_crops")
     * @Assert\NotBlank()
     */
    private $crops;

    /**
     * @ORM\ManyToMany(targetEntity="CatalogProduct")
     * @ORM\JoinTable(name="app_bookings_products")
     */
    private $products;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @Assert\NotBlank()
     */
    private $rules;

    /**
     * @Recaptcha\IsTrue
     */
    public $recaptcha;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set farmName
     *
     * @param string $farmName
     *
     * @return Booking
     */
    public function setFarmName($farmName)
    {
        $this->farmName = $farmName;

        return $this;
    }

    /**
     * Get farmName
     *
     * @return string
     */
    public function getFarmName()
    {
        return $this->farmName;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Booking
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Booking
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Booking
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Booking
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->crops = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set arableArea
     *
     * @param \AppBundle\Entity\BookingArableArea $arableArea
     *
     * @return Booking
     */
    public function setArableArea(\AppBundle\Entity\BookingArableArea $arableArea = null)
    {
        $this->arableArea = $arableArea;

        return $this;
    }

    /**
     * Get arableArea
     *
     * @return \AppBundle\Entity\BookingArableArea
     */
    public function getArableArea()
    {
        return $this->arableArea;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\BookingOrganization $organization
     *
     * @return Booking
     */
    public function setOrganization(\AppBundle\Entity\BookingOrganization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\BookingOrganization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Add crop
     *
     * @param \AppBundle\Entity\CatalogProductParamValue $crop
     *
     * @return Booking
     */
    public function addCrop(\AppBundle\Entity\CatalogProductParamValue $crop)
    {
        $this->crops[] = $crop;

        return $this;
    }

    /**
     * Remove crop
     *
     * @param \AppBundle\Entity\CatalogProductParamValue $crop
     */
    public function removeCrop(\AppBundle\Entity\CatalogProductParamValue $crop)
    {
        $this->crops->removeElement($crop);
    }

    /**
     * Get crops
     *
     * @return \AppBundle\Entity\CatalogProductParamValue[]
     */
    public function getCrops()
    {
        return $this->crops;
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\CatalogProduct $product
     *
     * @return Booking
     */
    public function addProduct(\AppBundle\Entity\CatalogProduct $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AppBundle\Entity\CatalogProduct $product
     */
    public function removeProduct(\AppBundle\Entity\CatalogProduct $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return CatalogProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Booking
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param mixed $rules
     * @return Booking
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Set region
     *
     * @param \AppBundle\Entity\GeoRegion $region
     *
     * @return Booking
     */
    public function setRegion(\AppBundle\Entity\GeoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \AppBundle\Entity\GeoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }
}
