<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CatalogCategory
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="app_catalog_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatalogCategoryRepository")
 */
class CatalogCategory
{
    const SEEDS_ID = 7;
    const DESICCANT_ID = 5;
    const RETARDANT_ID = 6;
    const EQUIPMENT_FOR_SEEDS_ID = 8;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="page_head", type="text", nullable=true)
     */
    protected $pageHead;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(name="sitemap_priority", type="float", nullable=true)
     */
    protected $sitemapPriority;

    /**
     * @var string
     *
     * @ORM\Column(name="description_advanced", type="text", nullable=true)
     */
    private $descriptionAdvanced;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    private $uploadedImage;

    /**
     * @var string
     *
     * @ORM\Column(name="image_link", type="string", length=255, nullable=true)
     */
    private $imageLink;

    /**
     * @ORM\Column(name="hide_products", type="boolean")
     */
    protected $hideProducts = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="CatalogCategory", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="CatalogCategory", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="CatalogProduct", mappedBy="category")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity="CatalogProductParam")
     * @ORM\JoinTable(name="app_catalog_categories_product_params")
     */
    private $productParams;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogCategoryType")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $type;

    public function productExtraFields()
    {
        $fields = [];
        switch ($this->getRootCategory()->getId()) {
            case self::SEEDS_ID:
                $fields = [
                    'type' => 'Тип'
                ];
                break;
            case self::DESICCANT_ID:
            case self::RETARDANT_ID:
                $fields = [
                    'targetFirst' => 'Культура'
                ];
                break;
            default:
                $fields = [
                    'targetFirst' => 'Культура',
                    'targetSecond' => 'Вредный обьект'
                ];
                break;
        }

        return $fields;
    }

    /**
     * @return mixed
     */
    public function getUploadedImage()
    {
        return $this->uploadedImage;
    }

    /**
     * @param mixed $uploadedImage
     * @return CatalogCategory
     */
    public function setUploadedImage($uploadedImage)
    {
        $this->setUpdatedAt(new \DateTime('now'));
        $this->uploadedImage = $uploadedImage;
        return $this;
    }

    public function getRootCategory()
    {
        $root = $this;
        while ($root->getParent()) {
            $root = $root->getParent();
        }
        return $root;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return CatalogCategory
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getImageLink()
    {
        return $this->imageLink;
    }

    /**
     * @param string $imageLink
     * @return CatalogCategory
     */
    public function setImageLink($imageLink)
    {
        $this->imageLink = $imageLink;
        return $this;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CatalogCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CatalogCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CatalogCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CatalogCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return CatalogCategory
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return CatalogCategory
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set descriptionAdvanced
     *
     * @param string $descriptionAdvanced
     *
     * @return CatalogCategory
     */
    public function setDescriptionAdvanced($descriptionAdvanced)
    {
        $this->descriptionAdvanced = $descriptionAdvanced;

        return $this;
    }

    /**
     * Get descriptionAdvanced
     *
     * @return string
     */
    public function getDescriptionAdvanced()
    {
        return $this->descriptionAdvanced;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CatalogCategory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CatalogCategory
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return CatalogCategory
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return CatalogCategory
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return CatalogCategory
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\CatalogCategory $parent
     *
     * @return CatalogCategory
     */
    public function setParent(\AppBundle\Entity\CatalogCategory $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\CatalogCategory
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\CatalogCategory $child
     *
     * @return CatalogCategory
     */
    public function addChild(\AppBundle\Entity\CatalogCategory $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\CatalogCategory $child
     */
    public function removeChild(\AppBundle\Entity\CatalogCategory $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return mixed
     */
    public function getHideProducts()
    {
        return $this->hideProducts;
    }

    /**
     * @param mixed $hideProducts
     * @return CatalogCategory
     */
    public function setHideProducts($hideProducts)
    {
        $this->hideProducts = $hideProducts;
        return $this;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return CatalogCategory
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\CatalogProduct $product
     *
     * @return CatalogCategory
     */
    public function addProduct(\AppBundle\Entity\CatalogProduct $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AppBundle\Entity\CatalogProduct $product
     */
    public function removeProduct(\AppBundle\Entity\CatalogProduct $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return CatalogProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add productParam
     *
     * @param \AppBundle\Entity\CatalogProductParam $productParam
     *
     * @return CatalogCategory
     */
    public function addProductParam(\AppBundle\Entity\CatalogProductParam $productParam)
    {
        $this->productParams[] = $productParam;

        return $this;
    }

    /**
     * Remove productParam
     *
     * @param \AppBundle\Entity\CatalogProductParam $productParam
     */
    public function removeProductParam(\AppBundle\Entity\CatalogProductParam $productParam)
    {
        $this->productParams->removeElement($productParam);
    }

    /**
     * Get productParams
     *
     * @return CatalogProductParam[]
     */
    public function getProductParams()
    {
        return $this->productParams;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\CatalogCategoryType $type
     *
     * @return CatalogCategory
     */
    public function setType(\AppBundle\Entity\CatalogCategoryType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\CatalogCategoryType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sitemapPriority
     *
     * @param float $sitemapPriority
     *
     * @return CatalogCategory
     */
    public function setSitemapPriority($sitemapPriority)
    {
        $this->sitemapPriority = $sitemapPriority;

        return $this;
    }

    /**
     * Get sitemapPriority
     *
     * @return float
     */
    public function getSitemapPriority()
    {
        return $this->sitemapPriority;
    }

    /**
     * Set pageHead
     *
     * @param string $pageHead
     *
     * @return CatalogCategory
     */
    public function setPageHead($pageHead)
    {
        $this->pageHead = $pageHead;

        return $this;
    }

    /**
     * Get pageHead
     *
     * @return string
     */
    public function getPageHead()
    {
        return $this->pageHead;
    }
}
