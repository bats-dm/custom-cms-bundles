<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatalogProductsParamValues
 *
 * @ORM\Table(name="app_catalog_products_param_values")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatalogProductsParamValuesRepository")
 */
class CatalogProductsParamValues
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogProduct", inversedBy="paramValues")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Column(name="param_value_id", type="integer")
     */
    private $paramValueId;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogProductParamValue")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $paramValue;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogProductsParamValues")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="CatalogProductsParamValues", mappedBy="parent")
     */
    private $children;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     *
     * @return CatalogProductsParamValues
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set paramValueId
     *
     * @param integer $paramValueId
     *
     * @return CatalogProductsParamValues
     */
    public function setParamValueId($paramValueId)
    {
        $this->paramValueId = $paramValueId;

        return $this;
    }

    /**
     * Get paramValueId
     *
     * @return int
     */
    public function getParamValueId()
    {
        return $this->paramValueId;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return CatalogProductsParamValues
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\CatalogProduct $product
     *
     * @return CatalogProductsParamValues
     */
    public function setProduct(\AppBundle\Entity\CatalogProduct $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\CatalogProduct
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set paramValue
     *
     * @param \AppBundle\Entity\CatalogProductParamValue $paramValue
     *
     * @return CatalogProductsParamValues
     */
    public function setParamValue(\AppBundle\Entity\CatalogProductParamValue $paramValue = null)
    {
        $this->paramValue = $paramValue;

        return $this;
    }

    /**
     * Get paramValue
     *
     * @return \AppBundle\Entity\CatalogProductParamValue
     */
    public function getParamValue()
    {
        return $this->paramValue;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\CatalogProductsParamValues $parent
     *
     * @return CatalogProductsParamValues
     */
    public function setParent(\AppBundle\Entity\CatalogProductsParamValues $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\CatalogProductsParamValues
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\CatalogProductsParamValues $child
     *
     * @return CatalogProductsParamValues
     */
    public function addChild(\AppBundle\Entity\CatalogProductsParamValues $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\CatalogProductsParamValues $child
     */
    public function removeChild(\AppBundle\Entity\CatalogProductsParamValues $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
