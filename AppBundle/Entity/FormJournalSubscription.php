<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FormJournalSubscription
 *
 * @ORM\Table(name="app_form_journal_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormJournalSubscriptionRepository")
 */
class FormJournalSubscription extends Form
{
    /**
     * @var string
     *
     * @ORM\Column(name="post_address", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $postAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="post", type="string", length=255, nullable=true)
     */
    private $post;


    /**
     * Set postAddress
     *
     * @param string $postAddress
     *
     * @return FormJournalSubscription
     */
    public function setPostAddress($postAddress)
    {
        $this->postAddress = $postAddress;

        return $this;
    }

    /**
     * Get postAddress
     *
     * @return string
     */
    public function getPostAddress()
    {
        return $this->postAddress;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return FormJournalSubscription
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return FormJournalSubscription
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return FormJournalSubscription
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set post
     *
     * @param string $post
     *
     * @return FormJournalSubscription
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }
}
