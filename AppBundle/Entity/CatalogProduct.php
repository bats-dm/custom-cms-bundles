<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CatalogProduct
 *
 * @ORM\Table(name="app_catalog_product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatalogProductRepository")
 */
class CatalogProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    private $uploadedImage;

    /**
     * @var string
     *
     * @ORM\Column(name="image_m", type="string", length=255, nullable=true)
     */
    private $imageM;

    /**
     * @var string
     *
     * @ORM\Column(name="image_s", type="string", length=255, nullable=true)
     */
    private $imageS;

    /**
     * @var string
     *
     * @ORM\Column(name="purpose", type="text", nullable=true)
     */
    private $purpose;

    /**
     * @var string
     *
     * @ORM\Column(name="advantage", type="text", nullable=true)
     */
    private $advantage;

    /**
     * @var string
     *
     * @ORM\Column(name="form", type="text", nullable=true)
     */
    private $form;

    /**
     * @var string
     *
     * @ORM\Column(name="action_mechanism", type="text", nullable=true)
     */
    private $actionMechanism;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="text", nullable=true)
     */
    private $activity;

    /**
     * @var string
     *
     * @ORM\Column(name="action_duration", type="text", nullable=true)
     */
    private $actionDuration;

    /**
     * @var string
     *
     * @ORM\Column(name="speed", type="text", nullable=true)
     */
    private $speed;

    /**
     * @var string
     *
     * @ORM\Column(name="phytotoxicity", type="text", nullable=true)
     */
    private $phytotoxicity;

    /**
     * @var string
     *
     * @ORM\Column(name="selectivity", type="text", nullable=true)
     */
    private $selectivity;

    /**
     * @var string
     *
     * @ORM\Column(name="resistance", type="text", nullable=true)
     */
    private $resistance;

    /**
     * @var string
     *
     * @ORM\Column(name="compatibility", type="text", nullable=true)
     */
    private $compatibility;

    /**
     * @var string
     *
     * @ORM\Column(name="application_method", type="text", nullable=true)
     */
    private $applicationMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="shelf_life", type="text", nullable=true)
     */
    private $shelfLife;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="text", nullable=true)
     */
    private $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="text", nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="cleaning_equipment", type="text", nullable=true)
     */
    private $cleaningEquipment;

    /**
     * @var string
     *
     * @ORM\Column(name="packaging", type="text", nullable=true)
     */
    private $packaging;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     */
    private $recommendations;

    /**
     * @var string
     *
     * @ORM\Column(name="preparation_method", type="text", nullable=true)
     */
    private $preparationMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="use_in_private_farms", type="text", nullable=true)
     */
    private $useInPrivateFarms;

    /**
     * @var string
     *
     * @ORM\Column(name="target", type="text", nullable=true)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="it_altname", type="text", nullable=true)
     */
    private $itAltname;

    /**
     * @var string
     *
     * @ORM\Column(name="it_overview", type="text", nullable=true)
     */
    private $itOverview;

    /**
     * @var string
     *
     * @ORM\Column(name="it_info12_title", type="text", nullable=true)
     */
    private $itInfo12Title;

    /**
     * @ORM\Column(name="page_head", type="text", nullable=true)
     */
    protected $pageHead;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(name="sitemap_priority", type="float", nullable=true)
     */
    protected $sitemapPriority;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogCategory")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="CatalogCategoryTag", inversedBy="products")
     * @ORM\JoinTable(name="app_catalog_category_tags_products")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $categoryTags;

    /**
     * @ORM\OneToMany(targetEntity="CatalogProductsParamValues", mappedBy="product")
     */
    private $paramValues;

    /**
     * @ORM\ManyToMany(targetEntity="Booking", mappedBy="products")
     */
    private $bookings;

    public function getTargetFirst()
    {
        return $this->convertTarget(0);
    }

    public function getTargetSecond()
    {
        return $this->convertTarget(1);
    }

    protected function convertTarget($index)
    {
        $result = [];
        $buf = @explode('---', strip_tags($this->getTarget()));
        if (isset($buf[$index])) {
            $buf = explode('|', $buf[$index]);
            foreach ($buf as $value) {
                $value = trim($value);
                if ($value) {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUploadedImage()
    {
        return $this->uploadedImage;
    }

    /**
     * @param mixed $uploadedImage
     * @return CatalogProduct
     */
    public function setUploadedImage($uploadedImage)
    {
        $this->setUpdatedAt(new \DateTime('now'));
        $this->uploadedImage = $uploadedImage;
        return $this;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CatalogProduct
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CatalogProduct
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CatalogProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return CatalogProduct
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set imageM
     *
     * @param string $imageM
     *
     * @return CatalogProduct
     */
    public function setImageM($imageM)
    {
        $this->imageM = $imageM;

        return $this;
    }

    /**
     * Get imageM
     *
     * @return string
     */
    public function getImageM()
    {
        return $this->imageM;
    }

    /**
     * Set imageS
     *
     * @param string $imageS
     *
     * @return CatalogProduct
     */
    public function setImageS($imageS)
    {
        $this->imageS = $imageS;

        return $this;
    }

    /**
     * Get imageS
     *
     * @return string
     */
    public function getImageS()
    {
        return $this->imageS;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return CatalogProduct
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set advantage
     *
     * @param string $advantage
     *
     * @return CatalogProduct
     */
    public function setAdvantage($advantage)
    {
        $this->advantage = $advantage;

        return $this;
    }

    /**
     * Get advantage
     *
     * @return string
     */
    public function getAdvantage()
    {
        return $this->advantage;
    }

    /**
     * Set form
     *
     * @param string $form
     *
     * @return CatalogProduct
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set actionMechanism
     *
     * @param string $actionMechanism
     *
     * @return CatalogProduct
     */
    public function setActionMechanism($actionMechanism)
    {
        $this->actionMechanism = $actionMechanism;

        return $this;
    }

    /**
     * Get actionMechanism
     *
     * @return string
     */
    public function getActionMechanism()
    {
        return $this->actionMechanism;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return CatalogProduct
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set actionDuration
     *
     * @param string $actionDuration
     *
     * @return CatalogProduct
     */
    public function setActionDuration($actionDuration)
    {
        $this->actionDuration = $actionDuration;

        return $this;
    }

    /**
     * Get actionDuration
     *
     * @return string
     */
    public function getActionDuration()
    {
        return $this->actionDuration;
    }

    /**
     * Set speed
     *
     * @param string $speed
     *
     * @return CatalogProduct
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return string
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set phytotoxicity
     *
     * @param string $phytotoxicity
     *
     * @return CatalogProduct
     */
    public function setPhytotoxicity($phytotoxicity)
    {
        $this->phytotoxicity = $phytotoxicity;

        return $this;
    }

    /**
     * Get phytotoxicity
     *
     * @return string
     */
    public function getPhytotoxicity()
    {
        return $this->phytotoxicity;
    }

    /**
     * Set selectivity
     *
     * @param string $selectivity
     *
     * @return CatalogProduct
     */
    public function setSelectivity($selectivity)
    {
        $this->selectivity = $selectivity;

        return $this;
    }

    /**
     * Get selectivity
     *
     * @return string
     */
    public function getSelectivity()
    {
        return $this->selectivity;
    }

    /**
     * Set resistance
     *
     * @param string $resistance
     *
     * @return CatalogProduct
     */
    public function setResistance($resistance)
    {
        $this->resistance = $resistance;

        return $this;
    }

    /**
     * Get resistance
     *
     * @return string
     */
    public function getResistance()
    {
        return $this->resistance;
    }

    /**
     * Set compatibility
     *
     * @param string $compatibility
     *
     * @return CatalogProduct
     */
    public function setCompatibility($compatibility)
    {
        $this->compatibility = $compatibility;

        return $this;
    }

    /**
     * Get compatibility
     *
     * @return string
     */
    public function getCompatibility()
    {
        return $this->compatibility;
    }

    /**
     * Set applicationMethod
     *
     * @param string $applicationMethod
     *
     * @return CatalogProduct
     */
    public function setApplicationMethod($applicationMethod)
    {
        $this->applicationMethod = $applicationMethod;

        return $this;
    }

    /**
     * Get applicationMethod
     *
     * @return string
     */
    public function getApplicationMethod()
    {
        return $this->applicationMethod;
    }

    /**
     * Set shelfLife
     *
     * @param string $shelfLife
     *
     * @return CatalogProduct
     */
    public function setShelfLife($shelfLife)
    {
        $this->shelfLife = $shelfLife;

        return $this;
    }

    /**
     * Get shelfLife
     *
     * @return string
     */
    public function getShelfLife()
    {
        return $this->shelfLife;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return CatalogProduct
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return CatalogProduct
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set cleaningEquipment
     *
     * @param string $cleaningEquipment
     *
     * @return CatalogProduct
     */
    public function setCleaningEquipment($cleaningEquipment)
    {
        $this->cleaningEquipment = $cleaningEquipment;

        return $this;
    }

    /**
     * Get cleaningEquipment
     *
     * @return string
     */
    public function getCleaningEquipment()
    {
        return $this->cleaningEquipment;
    }

    /**
     * Set packaging
     *
     * @param string $packaging
     *
     * @return CatalogProduct
     */
    public function setPackaging($packaging)
    {
        $this->packaging = $packaging;

        return $this;
    }

    /**
     * Get packaging
     *
     * @return string
     */
    public function getPackaging()
    {
        return $this->packaging;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     *
     * @return CatalogProduct
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set preparationMethod
     *
     * @param string $preparationMethod
     *
     * @return CatalogProduct
     */
    public function setPreparationMethod($preparationMethod)
    {
        $this->preparationMethod = $preparationMethod;

        return $this;
    }

    /**
     * Get preparationMethod
     *
     * @return string
     */
    public function getPreparationMethod()
    {
        return $this->preparationMethod;
    }

    /**
     * Set useInPrivateFarms
     *
     * @param string $useInPrivateFarms
     *
     * @return CatalogProduct
     */
    public function setUseInPrivateFarms($useInPrivateFarms)
    {
        $this->useInPrivateFarms = $useInPrivateFarms;

        return $this;
    }

    /**
     * Get useInPrivateFarms
     *
     * @return string
     */
    public function getUseInPrivateFarms()
    {
        return $this->useInPrivateFarms;
    }

    /**
     * Set target
     *
     * @param string $target
     *
     * @return CatalogProduct
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CatalogProduct
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return CatalogProduct
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return CatalogProduct
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\CatalogCategory $category
     *
     * @return CatalogProduct
     */
    public function setCategory(\AppBundle\Entity\CatalogCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\CatalogCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CatalogProduct
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CatalogProduct
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return CatalogProduct
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set categoryTag
     *
     * @param \AppBundle\Entity\CatalogCategoryTag $categoryTag
     *
     * @return CatalogProduct
     */
    public function setCategoryTag(\AppBundle\Entity\CatalogCategoryTag $categoryTag = null)
    {
        $this->categoryTag = $categoryTag;

        return $this;
    }

    /**
     * Get categoryTag
     *
     * @return \AppBundle\Entity\CatalogCategoryTag
     */
    public function getCategoryTag()
    {
        return $this->categoryTag;
    }

    /**
     * Add categoryTag
     *
     * @param \AppBundle\Entity\CatalogCategoryTag $categoryTag
     *
     * @return CatalogProduct
     */
    public function addCategoryTag(\AppBundle\Entity\CatalogCategoryTag $categoryTag)
    {
        $this->categoryTags[] = $categoryTag;

        return $this;
    }

    /**
     * Remove categoryTag
     *
     * @param \AppBundle\Entity\CatalogCategoryTag $categoryTag
     */
    public function removeCategoryTag(\AppBundle\Entity\CatalogCategoryTag $categoryTag)
    {
        $this->categoryTags->removeElement($categoryTag);
    }

    /**
     * Get categoryTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategoryTags()
    {
        return $this->categoryTags;
    }

    /**
     * Add paramValue
     *
     * @param \AppBundle\Entity\CatalogProductsParamValues $paramValue
     *
     * @return CatalogProduct
     */
    public function addParamValue(\AppBundle\Entity\CatalogProductsParamValues $paramValue)
    {
        $this->paramValues[] = $paramValue;

        return $this;
    }

    /**
     * Remove paramValue
     *
     * @param \AppBundle\Entity\CatalogProductsParamValues $paramValue
     */
    public function removeParamValue(\AppBundle\Entity\CatalogProductsParamValues $paramValue)
    {
        $this->paramValues->removeElement($paramValue);
    }

    /**
     * Get paramValues
     *
     * @return CatalogProductsParamValues[]
     */
    public function getParamValues()
    {
        return $this->paramValues;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categoryTags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->paramValues = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set itAltname
     *
     * @param string $itAltname
     *
     * @return CatalogProduct
     */
    public function setItAltname($itAltname)
    {
        $this->itAltname = $itAltname;

        return $this;
    }

    /**
     * Get itAltname
     *
     * @return string
     */
    public function getItAltname()
    {
        return $this->itAltname;
    }

    /**
     * Set itOverview
     *
     * @param string $itOverview
     *
     * @return CatalogProduct
     */
    public function setItOverview($itOverview)
    {
        $this->itOverview = $itOverview;

        return $this;
    }

    /**
     * Get itOverview
     *
     * @return string
     */
    public function getItOverview()
    {
        return $this->itOverview;
    }

    /**
     * Set itInfo12Title
     *
     * @param string $itInfo12Title
     *
     * @return CatalogProduct
     */
    public function setItInfo12Title($itInfo12Title)
    {
        $this->itInfo12Title = $itInfo12Title;

        return $this;
    }

    /**
     * Get itInfo12Title
     *
     * @return string
     */
    public function getItInfo12Title()
    {
        return $this->itInfo12Title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return CatalogProduct
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Add booking
     *
     * @param \AppBundle\Entity\Booking $booking
     *
     * @return CatalogProduct
     */
    public function addBooking(\AppBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \AppBundle\Entity\Booking $booking
     */
    public function removeBooking(\AppBundle\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return Booking[]
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Set sitemapPriority
     *
     * @param float $sitemapPriority
     *
     * @return CatalogProduct
     */
    public function setSitemapPriority($sitemapPriority)
    {
        $this->sitemapPriority = $sitemapPriority;

        return $this;
    }

    /**
     * Get sitemapPriority
     *
     * @return float
     */
    public function getSitemapPriority()
    {
        return $this->sitemapPriority;
    }

    /**
     * Set pageHead
     *
     * @param string $pageHead
     *
     * @return CatalogProduct
     */
    public function setPageHead($pageHead)
    {
        $this->pageHead = $pageHead;

        return $this;
    }

    /**
     * Get pageHead
     *
     * @return string
     */
    public function getPageHead()
    {
        return $this->pageHead;
    }
}
