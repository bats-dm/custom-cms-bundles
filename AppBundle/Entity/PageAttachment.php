<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="app_page_attachment")
 */
class PageAttachment extends Attachment
{
    /**
     * @ORM\ManyToOne(targetEntity="Intpill\CmsBundle\Entity\Page")
     */
    protected $object;

    /**
     * Set page
     *
     * @param \Intpill\CmsBundle\Entity\Page $page
     *
     * @return PageAttachment
     */
    public function setObject(\Intpill\CmsBundle\Entity\Page $object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return \Intpill\CmsBundle\Entity\Page
     */
    public function getObject()
    {
        return $this->object;
    }
}
