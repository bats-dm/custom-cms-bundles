<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CatalogProductParamValue
 *
 * @ORM\Table(name="app_catalog_product_param_value")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatalogProductParamValueRepository")
 */
class CatalogProductParamValue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Assert\Image(maxWidth = 60, maxHeight = 60)
     */
    private $uploadedImage;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position=0;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="CatalogProductParam")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $param;

    /**
     * @return mixed
     */
    public function getUploadedImage()
    {
        return $this->uploadedImage;
    }

    /**
     * @param mixed $uploadedImage
     * @return CatalogProductParamValue
     */
    public function setUploadedImage($uploadedImage)
    {
        $this->setUpdatedAt(new \DateTime('now'));
        $this->uploadedImage = $uploadedImage;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return CatalogProductParamValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return CatalogProductParamValue
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set param
     *
     * @param \AppBundle\Entity\CatalogProductParam $param
     *
     * @return CatalogProductParamValue
     */
    public function setParam(\AppBundle\Entity\CatalogProductParam $param = null)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * Get param
     *
     * @return \AppBundle\Entity\CatalogProductParam
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return CatalogProductParamValue
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return CatalogProductParamValue
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function __toString()
    {
        return (string) $this->getValue();
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CatalogProductParamValue
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}
