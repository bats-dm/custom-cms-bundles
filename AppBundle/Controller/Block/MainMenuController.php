<?php

namespace AppBundle\Controller\Block;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainMenuController extends Controller
{
    public function indexAction(Request $request)
    {
        $mainMenu = $this->getDoctrine()->getRepository('CmsBundle:Menu')->find($this->getParameter('main_menu_id'));
        $menuBuilder = $this->get('cms.menu.builder');
        $menuItems = $menuBuilder->create($mainMenu);

        return $this->render('AppBundle:Block:main_menu.html.twig', [
            'menuItems' => $menuItems
        ]);
    }
}