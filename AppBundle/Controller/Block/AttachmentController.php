<?php

namespace AppBundle\Controller\Block;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AttachmentController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('AppBundle:Block:attachment.html.twig', [
            'attachments' => $em->getRepository('AppBundle:PageAttachment')->findBy(['object' => $this->get('cms.page')->getObject()])
        ]);
    }
}