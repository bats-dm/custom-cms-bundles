<?php

namespace AppBundle\Controller\Block;

use AppBundle\Menu\MenuCatalog;
use Intpill\CmsBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LeftMenuController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $menuItems = null;
        $currentMenuItem = null;
        $page = $this->get('cms.page');
        $menuBuilder = $this->get('cms.menu.builder');
        if (($page->getType() == Page::TYPE_MODULE) && (in_array($page->getRoute(), ['catalog', 'product', 'product_tag', 'catalog_tags']))) {
            $menu = $menuBuilder->buildByClass(MenuCatalog::class);
            $menuItems = $menu->childrenByObject(null);
        } else {
            if (($currentMenuItems = $this->get('cms.page')->getObject()->getMenu()) && count($currentMenuItems)) {
                foreach ($currentMenuItems as $currentMenuItem) {
                    if (
                        ($parentMenu = $currentMenuItem->getParent()) &&
                        ($rootMenu = $parentMenu->getParent()) &&
                        ($rootMenu->getId() == $this->getParameter('main_menu_id'))
                    ) {
                        $menu = $menuBuilder->build();
                        $menuItems = $menu->childrenByObject($parentMenu);
                        break;
                    }
                }
            }
        }

        return $this->render('AppBundle:Block:left_menu.html.twig', [
            'menuItems' => $menuItems,
            'menuBuilder' => $menuBuilder
        ]);
    }
}