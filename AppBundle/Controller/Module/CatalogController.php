<?php

namespace AppBundle\Controller\Module;

use AppBundle\Entity\PageAttachment;
use AppBundle\Helper\Arr;
use AppBundle\Helper\Rand;
use Proxies\__CG__\AppBundle\Entity\CatalogCategoryTag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CatalogController extends Controller
{
    /**
     * Category
     *
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(Request $request, $slug)
    {
        $this->get('app.left_menu_catalog')->setUp();

        $pageHelper = $this->get('cms.page_helper');
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:CatalogCategory')->findOneBy(['slug'=>$slug]);

        if (!$category) {
            throw $this->createNotFoundException();
        }

        $pageHelper->replaceActivePageSeoVars($category, 'catalog');
        $this->get('cms.page')->setHead($category->getName());
        $this->get('cms.page')->setPageHead($category->getPageHead());

        $this->get('cms.breadcrumbs')->add('Главная', '/');
        $parent = $category->getParent();
        if ($parent) {
            $this->get('cms.breadcrumbs')->add($parent->getName(), $this->get('router')->generate('page_catalog', ['slug'=>$parent->getSlug()]));
        }
        $this->get('cms.breadcrumbs')->add($category->getName());

        if ($parent = $category->getParent()) {
            $baseCategory = $parent;
        } else {
            $baseCategory = $category;
        }

        $subProducts = [];
        if ($category->getChildren()) {
            foreach ($category->getChildren() as $child) {
                $pds = $em->getRepository('AppBundle:CatalogProduct')->findBy(['enabled'=>true, 'category'=>$child], ['name'=>'ASC']);
                if ($pds) {
                    $subProducts[] = [
                        'category' => $child,
                        'products' => $pds
                    ];
                }
            }
        }
        $products = $em->getRepository('AppBundle:CatalogProduct')->findBy(['enabled'=>true, 'category'=>$category], ['name'=>'ASC']);

        if (!$subProducts && !$products) {
            $products = $em->getRepository('AppBundle:CatalogProduct')->findBy(['enabled'=>true, 'category'=>$baseCategory], ['name'=>'ASC']);
        }

        return $this->render('AppBundle:Module/Catalog:category.html.twig', [
            'type' => 'category',
            'productExtraFields' => $category->productExtraFields(),
            'category' => $category,
            'products' => $products,
            'subProducts' => $subProducts,
            'highTags' => $em->getRepository('AppBundle:CatalogCategoryTag')->findBy(['enabled'=>true, 'category'=>$category, 'priority'=>CatalogCategoryTag::PRIORITY_HIGH], ['name'=>'ASC']),
            'lowTags' => $em->getRepository('AppBundle:CatalogCategoryTag')->findBy(['enabled'=>true, 'category'=>$category, 'priority'=>CatalogCategoryTag::PRIORITY_LOW], ['name'=>'ASC'])
        ]);
    }

    /**
     * Category tag
     *
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tagAction(Request $request, $slug)
    {
        $this->get('app.left_menu_catalog')->setUp();

        $pageHelper = $this->get('cms.page_helper');
        $em = $this->getDoctrine()->getManager();
        $categoryTag = $em->getRepository('AppBundle:CatalogCategoryTag')->findOneBy(['slug'=>$slug]);

        if (!$categoryTag) {
            throw $this->createNotFoundException();
        }

        $pageHelper->replaceActivePageSeoVars($categoryTag, 'catalog_tags');
        $this->get('cms.page')->setHead($categoryTag->getName());
        $this->get('cms.page')->setPageHead($categoryTag->getPageHead());

        $this->get('cms.breadcrumbs')->add('Главная', '/');
        $this->get('cms.breadcrumbs')->add($categoryTag->getCategory()->getName(), $this->get('router')->generate('page_catalog', ['slug'=>$categoryTag->getCategory()->getSlug()]));
        $this->get('cms.breadcrumbs')->add($categoryTag->getName());

        $products = $categoryTag->getProducts();

        return $this->render('AppBundle:Module/Catalog:category.html.twig', [
            'type' => 'tag',
            'productExtraFields' => $categoryTag->getCategory()->productExtraFields(),
            'category' => $categoryTag,
            'products' => $products,
        ]);
    }

    /**
     * Product
     *
     * @param Request $request
     * @param $slug
     * @param null $tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productAction(Request $request, $slug, $tag=null)
    {
        $this->get('app.left_menu_catalog')->setUp();

        $pageHelper = $this->get('cms.page_helper');
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository('AppBundle:CatalogProduct');

        $product = $productRepo->findOneBy(['slug'=>$slug, 'enabled'=>true]);
        $categoryTag = $tag ? $em->getRepository('AppBundle:CatalogCategoryTag')->findOneBy(['slug'=>$tag]) : null;

        if (!$product) {
            throw $this->createNotFoundException();
        }

        $pageHelper->replaceActivePageSeoVars($product, 'product');
        $this->get('cms.page')->setHead($product->getName());
        $this->get('cms.page')->setPageHead($product->getPageHead());

        $category = $product->getCategory();
        $attachments = $em->getRepository('AppBundle:CatalogProductAttachment')->findBy(['object' => $product]);

        $this->get('cms.breadcrumbs')->add('Главная', '/');
        if ($categoryTag) {
            if ($categoryTag->getCategory()) {
                $this->get('cms.breadcrumbs')->add($categoryTag->getCategory()->getName(), $this->get('router')->generate('page_catalog', ['slug'=>$categoryTag->getCategory()->getSlug()]));
            }
            $this->get('cms.breadcrumbs')->add($categoryTag->getName(), $this->get('router')->generate('page_catalog_tags', ['slug'=>$tag]));
        } elseif ($product->getCategory()) {
            $this->get('cms.breadcrumbs')->add($product->getCategory()->getName(), $this->get('router')->generate('page_catalog', ['slug'=>$product->getCategory()->getSlug()]));
        }
        $this->get('cms.breadcrumbs')->add($product->getName());

        /** @var \AppBundle\Entity\CatalogCategoryTag[] $tags */
        $tags = [];
        $productTagsCount = count($product->getCategoryTags());
        if ($productTagsCount > 0) {
            $tags = Arr::arrayRand($product->getCategoryTags()->toArray(), 5, Rand::getThreeMonthSeed());
        }

        $jointlyPurchasedProducts = Arr::arrayRand($productRepo->findJointlyPurchasedAll($product), 6, Rand::getThreeMonthSeed());

        return $this->render('AppBundle:Module/Catalog:product.html.twig', [
            'product' => $product,
            'productExtraFields' => $product->getCategory()->productExtraFields(),
            'jointlyPurchasedProducts' => $jointlyPurchasedProducts,
            'tags' => $tags,
            'attachments' => $attachments,
            'fields' => [
                'description' => 'Описание',
                'purpose' => 'Назначение',
                'advantage' => 'Преимущество',
                'form' => 'Препаративная форма',
                'actionMechanism' => 'Механизм действия',
                'activity' => 'Спектр активности',
                'actionDuration' => 'Период защитного действия',
                'speed' => 'Скорость воздействия',
                'phytotoxicity' => 'Фитотоксичность',
                'resistance' => 'Возможность возникновения резистентности',
                'selectivity' => 'Избирательность (селективность)',
                'compatibility' => 'Совместимость',
                'applicationMethod' => 'Способ применения',
                'shelfLife' => 'Срок годности',
                //'subtitle' => 'Подзаголовок',
                'cleaningEquipment' => 'Очистка инструментов (техники)',
                'packaging' => 'Упаковка',
                'recommendations' => 'Рекомендации',
                'preparationMethod' => 'Регламент применения',
                'useInPrivateFarms' => 'Использование в приусадебном хозяйстве',
                //'target' => 'Целевые объекты'
            ]
        ]);
    }
}