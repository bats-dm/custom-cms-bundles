<?php

namespace AppBundle\Controller\Module;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
    public function listAction(Request $request)
    {
        $this->get('app.left_menu_page')->setUp();

        $page = $request->query->getInt('page');
        if ($page == 1) {
            return $this->redirectToRoute('page_news', [], 301);
        }
        if (!$page) {
            $page = 1;
        }
        $limit = 10;
        $query = $this->getDoctrine()->getRepository('AppBundle:News')
            ->createQueryBuilder('f')
            ->where('f.enabled=true')
            ->orderBy('f.publicDate', 'DESC')
        ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $limit
        );

        $this->get('cms.breadcrumbs')->add('Главная', '/');
        $this->get('cms.breadcrumbs')->add($this->get('cms.page')->getObject()->getName());

        return $this->render('AppBundle:Module/News:list.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function oneAction(Request $request, $slug)
    {
        $this->get('app.left_menu_page')->setUp();

        $pageHelper = $this->get('cms.page_helper');
        $page = $this->get('cms.page');

        $item = $this->getDoctrine()->getRepository('AppBundle:News')->find($slug);

        if (!$item) {
            throw $this->createNotFoundException();
        }

        $page->setHead('');
        $page->setPageHead($item->getPageHead());
        $pageHelper->replaceActivePageSeoVars($item, 'news_one');

        $this->get('cms.breadcrumbs')->add('Главная', '/');
        $this->get('cms.breadcrumbs')->add($this->get('cms.page')->getObject()->getName(), $this->get('router')->generate('page_news'));
        $this->get('cms.breadcrumbs')->add($item->getName());

        return $this->render('AppBundle:Module/News:one.html.twig', [
            'item' => $item
        ]);
    }
}