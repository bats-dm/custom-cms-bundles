<?php

namespace AppBundle\Controller\Module;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class InfoboardController extends Controller
{
    const AGRONEWS_BASE_URL = "http://cwr.agritech.farmsat.com/Handlers/";
    const AGRONEWS_NEWS_DATA_URL = "https://www.agroxxi.ru/rss/content/2/feed.rss";

    protected $Token;

    public static function getAgronewsAuthUrl()
    {
        return self::AGRONEWS_BASE_URL."Authentication.ashx?login=CWR_Russia&password=snmDcjC3";
    }

    public static function getAgronewsWeatherDataUrl()
    {
        return self::AGRONEWS_BASE_URL."CustomWeatherDataService.ashx";
    }

    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:Module/Infoboard:index.html.twig', [
        ]);
    }

    /**
     * @Route("/agro-weather/", name="agro-weather")
     */
    public function GetWeather(Request $request)
    {
        $post = json_decode($request->request->get('prq'), true);
        $idgrid = $post['d']['idgrid'];

        $Data = $this->LoadWeather($idgrid);

        return new JsonResponse($Data);
    }

    /**
     * @Route("/agro-news/", name="agro-news")
     */
    public function GetNews()
    {
        $xml = $this->UrlGetContents(self::AGRONEWS_NEWS_DATA_URL);


        $data = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        if(!$data) {
            throw new \Exception("Data error");
        }
        $R = array();
        foreach($data->channel->item as $value) {
            $R[] = array('title'=>(string)$value->title,
                'annotation'=>strip_tags((string)$value->description),
                'link'=>(string)$value->link);
        }
        return new JsonResponse($R);
    }

    protected function LoadWeather($idgrid)
    {
        libxml_use_internal_errors(true);
        $data = '';

        $this->getToken();

        $xml = $this->UrlGetContents(self::getAgronewsWeatherDataUrl()."?key=".$this->Token."&idgrid=".$idgrid."&lang=ru-RU");

        switch ($xml) {
            case 'CWRSERV-009':
            case 'CWRSERV-011':
                $data['error']['msg'] = "Key doesn't exist";
                $data['error']['code'] = 1;
                break;
            case 'CWRSERV-012':
                $data['error']['msg'] = "No key";
                $data['error']['code'] = 2;
                break;
            case 'CWRSERV-013':
                $data['error']['msg'] = "No idgrid";
                $data['error']['code'] = 3;
                break;
            case 'CWRSERV-014':
                $data['error']['msg'] = "Unexpected error occurs";
                $data['error']['code'] = 4;
                break;

            default:
                $data = simplexml_load_string($xml);
                if (!$data) {
                    $data['error']['msg'] = (empty($xml)) ? "No data available" : ("Not documented error ".$xml);
                    $data['error']['code'] = 5;
                }
                else {
                    //file_put_contents($this->FileName, json_encode($data));
                }
                break;
        }

        return $data;
    }

    public function getToken()
    {
        if(!$this->Token) {
            if(isset($_SESSION["Services"]["AgroNews"]["TokenTime"]) && $_SESSION["Services"]["AgroNews"]["TokenTime"] < time() + 1500 && isset($_SESSION["Services"]["AgroNews"]["Token"])) {
                $this->Token = $_SESSION["Services"]["AgroNews"]["Token"];
            }
            else {
                $this->Token = $this->UrlGetContents(self::getAgronewsAuthUrl());
            }
            //$this->Token = 'E2C542CC-8E0C-4028-B460-1DA8E225C564';
        }
        return $this->Token;
    }

    protected function UrlGetContents ($url)
    {
        if (!function_exists('curl_init')){
            throw new \Exception('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(curl_exec($ch) === false) {
            $output = curl_error($ch);
        }
        else {
            $output = curl_exec($ch);
        }

        curl_close($ch);
        return $output;
    }
}