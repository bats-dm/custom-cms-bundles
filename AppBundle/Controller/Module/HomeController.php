<?php

namespace AppBundle\Controller\Module;

use AppBundle\Entity\CatalogCategory;
use AppBundle\Entity\CatalogCategoryType;
use AppBundle\Entity\CatalogProduct;
use AppBundle\Entity\CatalogProductParam;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * Home
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categoryRepo = $em->getRepository('AppBundle:CatalogCategory');
        $paramValueRepo = $em->getRepository('AppBundle:CatalogProductParamValue');

        $partners = $em->getRepository('AppBundle:Partner')->findBy(['enabled' => true], ['name'=>'ASC']);

        $categoryTypes = $em->getRepository('AppBundle:CatalogCategoryType')->findAll();

        return $this->render('AppBundle:Module/Home:index.html.twig', [
            'partners' => $partners,
            'banners' => $em->getRepository('AppBundle:Banner')->findAll(),
            'categoryTypes' => $categoryTypes
        ]);
    }

    /**
     * @Route("/filter-category-type-data", name="filter_category_type_data")
     */
    public function filterCategoryTypeDataAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $productRepo = $em->getRepository('AppBundle:CatalogProduct');
        $categoryType = $em->getRepository('AppBundle:CatalogCategoryType')->find($request->request->get('category_type_id'));
        $categories = $em->getRepository('AppBundle:CatalogCategory')->categoriesByType($categoryType);
        $category = $em->getRepository('AppBundle:CatalogCategory')->find($categories[0]);
        if ($categoryType->getParamsRelation() == 'category_type') {
            $products = $productRepo->findBy(['enabled'=>true]);
        } else {
            $products = $productRepo->findBy(['category'=>$category, 'enabled'=>true]);
        }

        $response = [
            'status' => 'ok',
            'categories_html' => $this->getFilterCategoriesHtml($categoryType),
            'filters_html' => $this->getFiltersHtml($category, $products)
        ];

        return new JsonResponse($response);
    }

    protected function getFilterCategoriesHtml(CatalogCategoryType $categoryType)
    {
        $em = $this->getDoctrine()->getManager();
        $categoryRepo = $em->getRepository('AppBundle:CatalogCategory');

        return $this->renderView('AppBundle:Module/Home:filter_categories.html.twig', [
            'categories' => $categoryRepo->categoriesByType($categoryType)
        ]);
    }

    protected function getFiltersHtml(CatalogCategory $category, $products = null, $baseParam = null)
    {
        $em = $this->getDoctrine()->getManager();
        $categoryRepo = $em->getRepository('AppBundle:CatalogCategory');

        $html = '';
        foreach($categoryRepo->productParams($category) as $param) {
            if ($param != $baseParam) {
                $html .= $this->getFilterHtml($param, $products);
            }
            if ($param->getRelation()) {
                $html .= $this->getFilterHtml($param->getRelation(), $products);
            }
        }
        return $html;
    }

    protected function getFilterHtml(CatalogProductParam $param, $products = null)
    {
        $em = $this->getDoctrine()->getManager();
        if ($products) {
            $paramValueRepo = $em->getRepository('AppBundle:CatalogProductParamValue');
            $filterValues = $paramValueRepo->findByProducts($param, $products);
        } else {
            $filterValues = $param->getValues();
        }

        return $this->renderView('AppBundle:Module/Home:filter.html.twig', [
            'filter' => $param,
            'filterValues' => $filterValues
        ]);
    }

    /**
     * @Route("/filter-get-params", name="filter_get_params")
     */
    public function filterGetParamsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $request->request->get('categories');
        $baseParamValueId = $request->request->get('base_param_value_id');
        $baseParamId = $request->request->get('base_param_id');
        $baseParam = $em->getRepository('AppBundle:CatalogProductParam')->find($baseParamId);
        $categoryTypeId = $request->request->get('category_type_id');
        $categoryType = $em->getRepository('AppBundle:CatalogCategoryType')->find($categoryTypeId);
        $productRepo = $em->getRepository('AppBundle:CatalogProduct');
        $category = $categories ? $em->getRepository('AppBundle:CatalogCategory')->find($categories[0]) : null;
        if (!$category) {
            $cats = $em->getRepository('AppBundle:CatalogCategory')->categoriesByType($categoryType);
            $category = $em->getRepository('AppBundle:CatalogCategory')->find($cats[0]);
        }

        $products = [];
        if ($categories) {
            $products = $productRepo->findBy(['category' => $categories, 'enabled' => true]);
        }
        if ($baseParamValueId) {
            $baseParamValue = $em->getRepository('AppBundle:CatalogProductParamValue')->find($baseParamValueId);
            $bpvProducts = $productRepo->findByParamValue($baseParamValue);
            if ($products) {
                $res = [];
                foreach ($products as $product) {
                    foreach ($bpvProducts as $bpvProduct) {
                        if ($product == $bpvProduct) {
                            $res[] = $product;
                            break;
                        }
                    }
                }
                $products = $res;
            } else {
                $products = $bpvProducts;
            }
        }

        $response = [
            'status' => 'ok',
            'filters_html' => $this->getFiltersHtml($category, $products, $baseParam)
        ];

        return new JsonResponse($response);
    }

    /**
     * @Route("/filter-categories-by-param-value", name="filter_categories_by_param_value")
     */
    public function filterCategoriesByParamValueAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:CatalogCategory')->findByParamValue(
            $this->getDoctrine()->getRepository('AppBundle:CatalogProductParamValue')->find($request->request->get('param_value_id'))
        );

        $response = [
            'status' => 'ok',
            'categories' => array_map(function ($category) {
                return $category->getId();
            }, $categories)
        ];

        return new JsonResponse($response);
    }

    /**
     * @Route("/filter-products", name="filter_products")
     */
    public function productsAction(Request $request)
    {
        $onPage = 8;
        $page = (int) $request->request->get('page');
        if (!$page) $page = 1;
        $offset = $onPage*($page-1);
        $name = $request->request->get('name');
        $categories = $request->request->get('categories');
        $categoryTypeId = $request->request->get('category_type_id');
        $filters = $request->request->get('filters');
        $showAll = $filters ? true : false;

        $em = $this->getDoctrine()->getManager();

        if (!$categories) {
            $categories = $em->getRepository('AppBundle:CatalogCategory')->findBy(['type'=>$categoryTypeId]);
        }

        /** @var CatalogProduct[] $products */
        $qb = $em->getRepository('AppBundle:CatalogProduct')->createQueryBuilder('p')
            ->where('p.enabled = true')
            ->orderBy('p.name')
        ;
        if (!$showAll) {
            $qb
                ->setFirstResult($offset)
                ->setMaxResults($onPage)
            ;
        }

        if ($name) {
            $qb->andWhere('p.name LIKE :name')->setParameter('name', '%'.$name.'%');
        } else {
            if ($categories) {
                $qb
                    ->andWhere('p.category IN (:categories)')
                    ->setParameter('categories', $categories)
                ;
            }

            if ($filters) {
                $productIds = null;
                foreach ($filters as $filter) {
                    $ids = [];
                    foreach ($filter['values'] as $fv) {
                        $ids[] = intval($fv);
                    }
                    if ($ids) {
                        $pvQb = $em->getRepository('AppBundle:CatalogProductsParamValues')->createQueryBuilder('pv')
                            ->select('pv.productId as col')
                            ->where('pv.paramValueId IN (:ids)')
                            ->setParameter('ids', $ids)
                        ;
                        $pIds = array_map(function ($v) {
                            return $v['col'];
                        }, $pvQb->getQuery()->getScalarResult());
                        if ($productIds === null) {
                            $productIds = $pIds;
                        } else {
                            $productIds = array_intersect($productIds, $pIds);
                        }
                    }
                }
                $qb->andWhere('p.id IN (:ids)');
                $qb->setParameter('ids', $productIds);
            }
        }

        $products = $qb->getQuery()->getResult();
        $qb->setFirstResult(0);
        $count = $qb->select('count(p.id)')->getQuery()->getSingleScalarResult();
        $pages = ceil($count/$onPage);

        $response = [
            'status' => 'ok',
            'html' => $this->renderView('AppBundle:Module/Home:products.html.twig', [
                'products' => $products,
                'hasMore' => !$showAll && ($page < $pages)
            ])
        ];

        return new JsonResponse($response);
    }
}