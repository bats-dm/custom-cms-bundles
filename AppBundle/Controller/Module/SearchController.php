<?php

namespace AppBundle\Controller\Module;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function indexAction(Request $request)
    {
        $this->get('app.left_menu_page')->setUp();

        return $this->render('AppBundle:Module/Search:index.html.twig', [
        ]);
    }
}