<?php

namespace AppBundle\Controller\Module;

use AppBundle\Entity\PartnerAbstract;
use AppBundle\Repository\PartnerAbstractRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PartnerController extends Controller
{
    public function partnersAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Partner');
        return $this->renderPartners($request, $repo);
    }

    public function contactsAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:PartnerContact');
        return $this->renderPartners($request, $repo);
    }

    public function storehousesAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:PartnerStorehouse');
        return $this->renderPartners($request, $repo);
    }

    public function renderPartners(Request $request, PartnerAbstractRepository $repo)
    {
        $this->get('app.left_menu_page')->setUp();

        $em = $this->getDoctrine()->getManager();
        $filterRegion = $request->get('region') ? $em->getRepository('AppBundle:GeoRegion')->find($request->get('region')) : null;
        $filter = ['enabled' => true];

        if ($filterRegion) {
            $filter['region'] = $filterRegion;
        }

        /** @var PartnerAbstract[] $partners */
        $partners = $repo->findBy($filter, ['name'=>'ASC']);
        $regions = $repo->partnersRegions($partners);

        return $this->render('AppBundle:Module/Partner:partners.html.twig', [
            'partners' => $partners,
            'regions' => $regions,
            'filterRegion' => $filterRegion
        ]);
    }
}