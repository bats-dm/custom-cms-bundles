<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ExceptionController extends Controller
{
    /**
     * @param Request $request
     * @param FlattenException $exception
     * @param DebugLoggerInterface|null $logger
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handleAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $code = $exception->getStatusCode();
        $page = $this->get('cms.page');

        if ($code == 404) {
            if (!($redirectUrl = $this->redirectUrl($request))) {
                $redirectUrl = $this->convertOldUrl($request);
            }

            if ($redirectUrl) {
                return $this->redirect($redirectUrl, 301);
            }
        }

        $page->setTitle($code);

        return $this->render($this->getTemplate($code));
    }

    protected function getTemplate($code)
    {
        $suffix = '';
        if ($code == 404) {
            $suffix = 404;
        }

        return (string) 'AppBundle:Exception:error'.$suffix.'.html.twig';
    }

    protected function convertOldUrl(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sourceUrl = trim(strtolower($request->getPathInfo()));
        $queryStr = $request->getQueryString();
        $redirectUrl = $sourceUrl;

        $redirectUrl = str_replace(['/ru', '.html', '.htm'], '', $redirectUrl);
        $redirectUrl = str_replace('_', '-', $redirectUrl);
        $redirectUrl = preg_replace('/\/$/', '', $redirectUrl);

        $productSlug = substr($redirectUrl, 1);
        if (strpos($productSlug, '/') === false) {
            $product = $em->getRepository('AppBundle:CatalogProduct')->findOneBy(['slug' => $productSlug]);
            if ($product) {
                $redirectUrl = '/product/' . $productSlug;
            }
        }

        if (strpos($redirectUrl, '/products') !== false) {
            $cats = explode('/', str_replace('/products', '', $redirectUrl));
            if (!empty($cats[1])) {
                $redirectUrl = '/products/' . $cats[1];
            }
        }

        if ($redirectUrl != $sourceUrl) {
            if ($queryStr) {
                $redirectUrl .= '?' . $queryStr;
            }
            return $redirectUrl;
        }

        return false;
    }

    protected function redirectUrl(Request $request)
    {
        if ($redirect = $this->getDoctrine()->getRepository('AppBundle:Redirect')->findOneBy(['sourceUrl' => $request->getPathInfo()])) {
            return $redirect->getRedirectUrl();
        }

        return false;
    }
}