<?php

namespace AppBundle\Controller;


use Intpill\CmsBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Thepixeldeveloper;

class SeoController extends Controller
{
    /**
     * @Route("/sitemap.xml", defaults={"_format"="xml"}, name="sitemap")
     */
    public function sitemapAction()
    {
        $em = $this->getDoctrine()->getManager();
        $urlSet = new Thepixeldeveloper\Sitemap\Urlset();

        $items = $em->getRepository('AppBundle:CatalogCategory')->findBy(['enabled' => true]);
        foreach ($items as $item) {
            $urlSet->addUrl($this->sitemapUrl('page_catalog', $item->getSlug(), $item->getSitemapPriority(), $this->convertSitemapDate($item->getUpdatedAt())));
        }
        $items = $em->getRepository('AppBundle:CatalogProduct')->findBy(['enabled' => true]);
        foreach ($items as $item) {
            $urlSet->addUrl($this->sitemapUrl('page_product', $item->getSlug(), $item->getSitemapPriority(), $this->convertSitemapDate($item->getUpdatedAt())));
        }
        $items = $em->getRepository('CmsBundle:Page')->findBy(['enabled' => true]);
        foreach ($items as $item) {
            if ($item->getType() == Page::TYPE_PAGE) {
                $urlSet->addUrl($this->sitemapUrl('page_' . $item->getId(), '', $item->getSitemapPriority(), $this->convertSitemapDate($item->getUpdatedAt())));
            }
        }
        $items = $em->getRepository('AppBundle:News')->findBy(['enabled' => true]);
        foreach ($items as $item) {
            $urlSet->addUrl($this->sitemapUrl('page_news_one',  $item->getId(), $item->getSitemapPriority(), $this->convertSitemapDate($item->getUpdatedAt())));
        }

        return new Response((new Thepixeldeveloper\Sitemap\Output())->getOutput($urlSet));
    }

    protected function convertSitemapDate(\DateTime $dateTime)
    {
        if ($dateTime) {
            return $dateTime->format('Y-m-d');
        }
        return null;
    }

    protected function sitemapUrl($route, $slug = '', $priority = null, $lastMod = null)
    {
        $loc = $this->generateUrl($route, $slug ? ['slug' => $slug] : [], UrlGeneratorInterface::ABSOLUTE_URL);
        $url = (new Thepixeldeveloper\Sitemap\Url($loc));
        if ($priority) {
            if ($priority < 0) {
                $priority = 0;
            }
            if ($priority > 1) {
                $priority = 1;
            }
            $url->setPriority($priority);
        }
        //->setChangeFreq($changeFreq)
        if ($lastMod) {
            $url->setLastMod($lastMod);
        }
        return $url;
    }
}