<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FormBajarena;
use AppBundle\Entity\FormFusarium;
use AppBundle\Entity\FormJournalSubscription;
use AppBundle\Entity\FormNewsletterSubscription;
use AppBundle\Entity\FormQuestion;
use AppBundle\Entity\PageAttachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FormController extends Controller
{
    /**
     * Submit form
     *
     * @Route("/ajax/form", name="form")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $formName = $request->request->get('formName');

        $fields = [
            'fieldActivity', 'companyName', 'companyType', 'postcode', 'postAddress', 'phoneCode', 'fax', 'phone', 'fax', 'email', 'site', 'sownArea', 'sugarBeet', 'winterWheat', 'springBarley', 'potatoes', 'corn', 'rape', 'sunflower', 'otherCultures', 'treatmentEquipment', 'message'
        ];
        $fieldAliases = [
            'bayarenaType' => 'region',
            'firstName' => 'lastName',
            'lastName' => 'middleName',
            'position' => 'post',
            'advEmail' => 'additionalEmail',
            'magazins' => 'magazines'
        ];

        $form = null;
        switch ($formName) {
            case 'registerBayarena':
                $fieldAliases['middleName'] = 'name';
                $form = new FormBajarena();
                break;
            case 'magazine':
                $fields[] = 'name';
                $form = new FormJournalSubscription();
                break;
            case 'contacts':
                $fields[] = 'name';
                $form = new FormQuestion();
                break;
            case 'subscribe':
                $form = new FormNewsletterSubscription();
                break;
            case 'fusarium':
                $form = new FormFusarium();
                break;
        }

        if (!$form) {
            throw new \Exception('Invalid formName');
        }

        foreach ($fields as $field) {
            if (method_exists($form, 'set'.ucfirst($field))) {
                $form->{'set'.ucfirst($field)}($request->request->get($field));
            }
        }
        foreach ($fieldAliases as $formField=>$entityField) {
            if (method_exists($form, 'set'.ucfirst($entityField))) {
                $form->{'set'.ucfirst($entityField)}($request->request->get($formField));
            }
        }

        $validator = $this->get('validator');

        $response = ['errors' => []];

        foreach($validator->validate($form) as $error) {
            $field = $error->getPropertyPath();
            foreach ($fieldAliases as $formField=>$entityField) {
                if ($field == $entityField) {
                    $field = $formField;
                    break;
                }
            }
            $response['errors'][] = [
                'field' => $field,
                'message' => $error->getMessage()
            ];
        }

        if (!$response['errors']) {
            $em->persist($form);
            $em->flush();
            $this->get('app.form.mailer')->sendNotification($form);
        }

        return new JsonResponse($response);
    }
}