<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PageAttachment;
use AppBundle\Helper\Arr;
use Behat\Transliterator\Transliterator;
use Doctrine\ORM\Query\ResultSetMapping;
use Intpill\CmsBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\CatalogProductParam;
use AppBundle\Entity\CatalogProductParamValue;

class TestController extends Controller
{
    /**
     * @Route("/taste", name="taste")
     */
    public function indexAction(Request $request)
    {
//        $em = $this->getDoctrine()->getManager();
//        foreach ($em->getRepository('AppBundle:CatalogProductParam')->findAll() as $param) {
//            $pos = 0;
//            foreach ($em->getRepository('AppBundle:CatalogProductParamValue')->findBy(['param' => $param], ['id' => 'ASC']) as $paramValue) {
//                //$paramValue->setPosition($pos);
//                $em->getConnection()->prepare('UPDATE app_catalog_product_param_value SET `position`="'.$pos.'" WHERE id="'.$paramValue->getId().'"')->execute();
//                $pos++;
//            }
//        }
//        $em->flush();
        exit('good');

        return $this->render('@App/test.html.twig');
    }

    /**
     * @Route("/clear-cache", name="clear-cache")
     */
    public function clearCacheAction(Request $request)
    {
        $fs = new Filesystem();
        $fs->remove($this->getParameter('kernel.cache_dir'));
        exit('good');
    }

    /**
     * @Route("/clear-logs", name="clear-logs")
     */
    public function clearLogsAction(Request $request)
    {
        $fs = new Filesystem();
        $fs->remove($this->getParameter('kernel.logs_dir'));
        exit('good');
    }
}