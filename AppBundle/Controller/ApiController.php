<?php
namespace AppBundle\Controller;

use AppBundle\Entity\CatalogProduct;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ApiController extends Controller
{
    /**
     * @Route("/api/products", name="api_products")
     */
    public function productsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><products></products>');
        $productImagePath = $request->getSchemeAndHttpHost() . '/' .  str_replace('../', '', $this->getParameter('catalog_product_dir'));

        foreach ($em->getRepository('AppBundle:CatalogProduct')->findAll() as $product) {
            $productXml = $xml->addChild('product');
            foreach ([
                'id', 'name', 'description', 'slug', 'purpose', 'advantage', 'form', 'actionMechanism',
                'activity', 'actionDuration', 'speed', 'phytotoxicity', 'selectivity', 'resistance',
                'compatibility', 'applicationMethod', 'shelfLife', 'slogan', 'subtitle', 'cleaningEquipment',
                'packaging', 'recommendations', 'preparationMethod', 'useInPrivateFarms', 'target',
                'title', 'type', 'itAltname', 'itOverview', 'itInfo12Title', 'enabled'
             ] as $property) {
                $method = 'get' . ucfirst($property);
                if (method_exists($product, $method)) {
                    $child = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', htmlspecialchars($product->$method()));
                    $productXml->addChild($property, $child);
                }
            }

            if ($product->getImage()) {
                $productXml->addChild('image', $productImagePath . $product->getImage());
                $productXml->addChild('imageM', $productImagePath . $product->getImageM());
                $productXml->addChild('imageS', $productImagePath . $product->getImageS());
            }
        }

        $response = new Response($xml->asXML());
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }
}