<?php
namespace AppBundle\Controller;


use AppBundle\Menu\LeftMenuItem;
use Intpill\CmsBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    /**
     * Static pages
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $this->get('app.left_menu_page')->setUp();

        $page = $this->get('cms.page')->getObject();
        if (!$page->getIsHome()) {
            $this->get('cms.breadcrumbs')->add(
                $page->getName()
            );
            $parent = $page->getParent();
            while ($parent) {
                $route = $this->get('cms.page_helper')->routeName($parent);
                $link = '#';
                if ($route) {
                    $link = $this->get('router')->generate($route);
                }
                $this->get('cms.breadcrumbs')->add(
                    $parent->getName(),
                    $link
                );
                $parent = $parent->getParent();
            }
            $this->get('cms.breadcrumbs')->reverse();
        }

        return $this->render('AppBundle:Layout/Container:page.html.twig');
    }

    /**
     * Send email with presentation
     *
     * 
     */

    public function sendPresentationAction(Request $request) {
        if (!empty($request->get('email'))) {
          $path = realpath($this->container->getParameter('kernel.root_dir').'/../www/files/ppt.pdf');
            $message = (new \Swift_Message('Crop Science'))
              ->setFrom('bayer-mailer@yandex.ru')
              ->setTo($request->get('email'))
              ->setBody('Ваша презентация об эфективном способе лечения фузариоза')
              ->attach(\Swift_Attachment::fromPath($path))
            ;

            $this->get('mailer')->send($message);

            return new Response();
        }
        
        return false;
    }
}