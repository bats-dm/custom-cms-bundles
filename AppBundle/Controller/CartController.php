<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Booking;
use AppBundle\Entity\CatalogProductParam;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CartController extends Controller
{
    /**
     * @Route("/cart-add", name="cart_add")
     */
    public function addAction(Request $request)
    {
        $this->get('app.cart')->add($request->request->get('product_id'));
        return new JsonResponse([
            'amount' => $this->get('app.cart')->amount()
        ]);
    }

    /**
     * @Route("/cart-remove", name="cart_remove")
     */
    public function removeAction(Request $request)
    {
        $this->get('app.cart')->remove($request->request->get('product_id'));
        return new JsonResponse([
            'amount' => $this->get('app.cart')->amount()
        ]);
    }

    /**
     * @Route("/cart-clear", name="cart_clear")
     */
    public function clearAction(Request $request)
    {
        $this->get('app.cart')->clear();
        return new JsonResponse([]);
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function cartAction()
    {
        $em = $this->getDoctrine()->getManager();

        $crops = $em->getRepository('AppBundle:CatalogProductParamValue')
            ->createQueryBuilder('c')
            ->select('c.id, c.value')
            ->where('c.param=:param')
            ->setParameter('param', $em->getRepository('AppBundle:CatalogProductParam')->find(CatalogProductParam::CROP_ID))
            ->orderBy('c.value')
            ->getQuery()
            ->getArrayResult()
        ;

        return new JsonResponse([
            'html' => $this->renderView('AppBundle::cart.html.twig', [
                'organizations' => $em->getRepository('AppBundle:BookingOrganization')->findAll(),
                'arableAreas' => $em->getRepository('AppBundle:BookingArableArea')->findAll(),
                'regions' => $em->getRepository('AppBundle:GeoRegion')->findBy([
                    'country' => $em->getRepository('AppBundle:GeoCountry')->findOneBy(['alfa2'=>'RU'])
                ], ['name'=>'ASC']),
                'products' => $this->get('app.cart')->getItems()
            ]),
            'crops' => $crops
        ]);
    }

    /**
     * @Route("/booking", name="booking")
     */
    public function bookingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cart = $this->get('app.cart');

        $response = ['errors' => []];

        $validator = $this->get('validator');

        $booking = (new Booking())
            ->setFarmName($request->request->get('farm_name'))
            ->setPhone($request->request->get('phone'))
            ->setEmail($request->request->get('email'))
            ->setComment($request->request->get('comment'))
            ->setName($request->request->get('name'))
            ->setRules($request->request->get('rules'))
        ;
        if ($id = $request->request->get('region')) {
            $booking->setRegion($em->getRepository('AppBundle:GeoRegion')->find($id));
        }
        if ($id = $request->request->get('organization')) {
            $booking->setOrganization($em->getRepository('AppBundle:BookingOrganization')->find($id));
        }
        if ($id = $request->request->get('arable_area')) {
            $booking->setArableArea($em->getRepository('AppBundle:BookingArableArea')->find($id));
        }
        foreach ($cart->getItems() as $product) {
            $booking->addProduct($product);
        }
        if ($request->request->get('crops')) {
            foreach ($request->request->get('crops') as $cropId) {
                if ($cropId) {
                    $exists = false;
                    foreach ($booking->getCrops() as $currentCrop) {
                        if ($currentCrop->getId() == $cropId) {
                            $exists = true;
                        }
                    }
                    if (!$exists) {
                        $booking->addCrop($em->getRepository('AppBundle:CatalogProductParamValue')->find($cropId));
                    }
                }
            }
        }
        if (!count($booking->getCrops())) {
            $response['errors'][] = [
                'field' => 'crops',
                'message' => 'Нужно добавить культуры'
            ];
        }

        foreach($validator->validate($booking) as $error) {
            $response['errors'][] = [
                'field' => strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $error->getPropertyPath())),
                'message' => $error->getMessage()
            ];
        }

        if (!$response['errors']) {
            $em->persist($booking);
            $em->flush();
            $this->get('app.order.mailer')->sendNotifications($booking);
        }

        return new JsonResponse($response);
    }
}