<?php
namespace AppBundle\Menu;


use Intpill\CmsBundle\Site\ActivePage;
use Intpill\CmsBundle\Site\Menu\MenuBuilder;

/**
 * Create left menu for static pages
 */
class LeftMenuPage
{
    protected $menuBuilder;
    protected $leftMenu;
    protected $page;
    protected $mainMenuId;

    /**
     * LeftMenuPage constructor.
     * @param MenuBuilder $menuBuilder
     * @param LeftMenu $leftMenu
     * @param ActivePage $page
     * @param $mainMenuId
     */
    public function __construct(MenuBuilder $menuBuilder, LeftMenu $leftMenu, ActivePage $page, $mainMenuId)
    {
        $this->menuBuilder = $menuBuilder;
        $this->leftMenu = $leftMenu;
        $this->page = $page;
        $this->mainMenuId = $mainMenuId;
    }

    /**
     * Set up left menu
     */
    public function setUp()
    {
        if ($currentPage = $this->page->getObject()) {
            $currentMenu = $currentPage->getMenu();
            $menuPage = $currentPage;

            while ($menuPage && !count($currentMenu)) {
                if ($menuPage = $menuPage->getParent()){
                    $currentMenu = $menuPage->getMenu();
                }
            };

            if (count($currentMenu)) {
                foreach ($currentMenu as $currentMenuItem) {
                    $parentMenu = $currentMenuItem;
                    $branchRootMenu = null;
                    $isMainMenu = false;

                    while ($parentMenu) {
                        if ($parentMenu->getId() == $this->mainMenuId) {
                            $isMainMenu = true;
                            break;
                        }
                        $branchRootMenu = $parentMenu;
                        $parentMenu = $parentMenu->getParent();
                    }

                    if ($isMainMenu) {
                        $this->leftMenu->set($this->menuBuilder->create($branchRootMenu));
                    }
                }
            }
        }
    }
}