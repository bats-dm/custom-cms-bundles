<?php
namespace AppBundle\Menu;


use Intpill\CmsBundle\Site\Menu\MenuItem;

/**
 * Site left menu items
 */
class LeftMenu
{
    protected $items = [];

    /**
     * @param MenuItem $item
     * @return $this
     */
    public function add(MenuItem $item)
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     * @param $items
     * @return $this
     */
    public function set($items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return MenuItem[]
     */
    public function all()
    {
        return $this->items;
    }
}