<?php
namespace AppBundle\Menu;

use AppBundle\Entity\CatalogCategory;
use Intpill\CmsBundle\Site\Menu\MenuAbstract;
use Intpill\CmsBundle\Site\Menu\MenuBuilderAbstract;
use Intpill\CmsBundle\Site\Menu\MenuItem;
use Intpill\CmsBundle\Site\Menu\MenuChildBuilderInterface;

/**
 * Create catalog left menu
 */
class MenuCatalogBuilder extends MenuBuilderAbstract implements MenuChildBuilderInterface
{
    /**
     * @return MenuItem[]
     */
    public function create()
    {
        return $this->createByCategory();
    }

    /**
     * @param CatalogCategory|null $categoryEntity
     * @return MenuItem[]
     */
    public function createByCategory(CatalogCategory $categoryEntity = null)
    {
        $menuItems = [];

        $repo = $this->getEm()->getRepository('AppBundle:CatalogCategory');
        /** @var CatalogCategory $child */
        foreach ($repo->children($categoryEntity, true) as $child) {
            if ($child->getEnabled()) {
                 $menuItem = (new MenuItem())
                    ->setName($child->getName())
                    ->setUrl($this->getUrl($child))
                    ->setActive($this->isActive($child))
                ;
                if ($children = $this->createByCategory($child)) {
                    $menuItem->setChildren($children);
                }
                $menuItems[] = $menuItem;
            }
        }

        return $menuItems;
    }

    public function getUrl(CatalogCategory $category)
    {
        return $this->getRouter()->generate('page_catalog', ['slug'=>$category->getSlug()]);
    }

    public function isActive(CatalogCategory $category)
    {
        return ($this->getRequestStack()->getMasterRequest()->get('_route') == 'page_catalog') && ($this->getRequestStack()->getMasterRequest()->get('slug') == $category->getSlug());
    }
}