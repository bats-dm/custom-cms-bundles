<?php
namespace AppBundle\Menu;

/**
 * Create left menu for catalog
 */
class LeftMenuCatalog
{
    protected $menuCatalogBuilder;
    protected $leftMenu;

    /**
     * LeftMenuCatalog constructor.
     * @param MenuCatalogBuilder $menuCatalogBuilder
     * @param LeftMenu $leftMenu
     */
    public function __construct(MenuCatalogBuilder $menuCatalogBuilder, LeftMenu $leftMenu)
    {
        $this->menuCatalogBuilder = $menuCatalogBuilder;
        $this->leftMenu = $leftMenu;
    }

    /**
     * Set up left menu
     */
    public function setUp()
    {
        $this->leftMenu->set($this->menuCatalogBuilder->create());
    }
}