<?php
namespace AppBundle\Order;

use AppBundle\Email\BaseMailer;
use AppBundle\Entity\Booking;
use AppBundle\Entity\CatalogCategory;
use AppBundle\Entity\CatalogProduct;
use Intpill\CmsBundle\Content\Settings;
use Doctrine\ORM\EntityManager;

/**
 * Order Mailer
 */
class Mailer
{
    protected $mailer;
    protected $em;
    protected $settings;

    /**
     * Mailer constructor.
     * @param BaseMailer $mailer
     * @param EntityManager $em
     * @param Settings $settings
     */
    public function __construct(BaseMailer $mailer, EntityManager $em, Settings $settings)
    {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->settings = $settings;
    }

    /**
     * Send created order to admin and partner emails
     *
     * @param Booking $booking
     */
    public function sendNotifications(Booking $booking)
    {
        $emails = [
            $this->settings->value('email_order_notification')
        ];

        //add email if order has category 'EQUIPMENT FOR SEEDS'
        if ($booking->getProducts()) {
            foreach ($booking->getProducts() as $product) {
                if ($product->getCategory() && $product->getCategory()->getId() == CatalogCategory::EQUIPMENT_FOR_SEEDS_ID) {
                        $emails[] = $this->settings->value('email_order_notification_with_equipment_for_seeds');
                    break;
                }
            }
        }

        if ($booking->getRegion()) {
            if ($region = $this->em->getRepository('AppBundle:GeoRegion')->find($booking->getRegion())) {
                if ($region->getPartnerEmail()) {
                    $emails[] = $region->getPartnerEmail();
                }
            }
        }

        foreach ($emails as $email) {
            $this->sendNotification($email, $booking);
        }
    }

    /**
     * @param $email
     * @param Booking $booking
     * @return int
     */
    public function sendNotification($email, Booking $booking)
    {
        $body = '';
        $createRow = function ($name, $value) use(&$body) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }
            $body .= '<p><b>'.$name.':</b> '.$value.'</p>';
        };

        $createRow('Заявка №', $booking->getId());
        $createRow('Название хозяйства', $booking->getFarmName());
        if ($booking->getRegion()) {
            $createRow('Регион', $booking->getRegion()->getName());
        }
        if ($booking->getArableArea()) {
            $createRow('Площадь пахотных земель', $booking->getArableArea()->getValue());
        }
        $createRow('Имя', $booking->getName());
        $createRow('Email', $booking->getEmail());
        $createRow('Телефон', $booking->getPhone());
        if ($booking->getComment()) {
            $createRow('Комментарии', htmlspecialchars($booking->getComment()));
        }
        if ($booking->getProducts()) {
            $values = [];
            foreach ($booking->getProducts() as $product) {
                $values[] = $product->getName();
            }
            $createRow('Продукция', $values);
        }
        if ($booking->getCrops()) {
            $values = [];
            foreach ($booking->getCrops() as $crop) {
                $values[] = $crop->getValue();
            }
            $createRow('Культуры', $values);
        }

        return $this->mailer->send('Заявка на консультацию ' . $booking->getId(), $email, $body);
    }
}