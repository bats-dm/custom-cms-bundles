<?php
namespace AppBundle\Order;

use AppBundle\Entity\CatalogProduct;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Serializer;

/**
 * Cart
 */
class Cart
{
    const SESSION_NAME = 'cart';

    protected $session;
    protected $em;
    protected $items = [];

    /**
     * Cart constructor.
     * @param Session $session
     * @param EntityManager $em
     */
    public function __construct(Session $session, EntityManager $em)
    {
        $this->session = $session;
        $this->em = $em;
        $this->loadSession();
    }

    protected function saveSession()
    {
        $this->session->set(self::SESSION_NAME, $this->items);
    }

    protected function loadSession()
    {
        $this->items = $this->session->get(self::SESSION_NAME);
        if (!$this->items) $this->items = [];
    }

    /**
     * @param $productId
     */
    public function add($productId)
    {
        if (!in_array($productId, $this->items)) {
            $this->items[] = $productId;
            $this->saveSession();
        }
    }

    /**
     * @param $productId
     */
    public function remove($productId)
    {
        if (($key = array_search($productId, $this->items)) !== false) {
            array_splice($this->items, $key, 1);
            $this->saveSession();
        }
    }

    public function clear()
    {
        $this->items = [];
        $this->saveSession();
    }

    /**
     * @return int
     */
    public function amount()
    {
        return count($this->items);
    }

    /**
     * @return \AppBundle\Entity\CatalogProduct[]
     */
    public function getItems()
    {
        return $this->em->getRepository('AppBundle:CatalogProduct')->findBy(['id'=>$this->items]);
    }
}