<?php
namespace AppBundle\Form;

use AppBundle\Email\BaseMailer;
use AppBundle\Entity\Form;
use AppBundle\Entity\FormBajarena;
use AppBundle\Entity\FormFusarium;
use AppBundle\Entity\FormJournalSubscription;
use AppBundle\Entity\FormNewsletterSubscription;
use AppBundle\Entity\FormQuestion;
use Intpill\CmsBundle\Content\Settings;
use Doctrine\ORM\EntityManager;

/**
 * Form Mailer
 */
class Mailer
{
    protected $mailer;
    protected $em;
    protected $settings;

    /**
     * Mailer constructor.
     * @param BaseMailer $mailer
     * @param EntityManager $em
     * @param Settings $settings
     */
    public function __construct(BaseMailer $mailer, EntityManager $em, Settings $settings)
    {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->settings = $settings;
    }

    /**
     * Send submitted form to admin email
     *
     * @param Form $form
     * @return bool|int
     */
    public function sendNotification(Form $form)
    {
        $fields = [
            'id' => 'Номер',
            'lastName' => 'Фамилия',
            'name' => 'Имя',
            'middleName' => 'Отчество',
            'post' => 'Должность',
            'postcode' => 'Почтовый индекс',
            'postAddress' => 'Почтовый адрес',
            'phoneCode' => 'Телефонный код',
            'phone' => 'Телефон',
            'fax' => 'Факс',
            'email' => 'Email',
            'additionalEmail' => 'Дополнительный Email',
            'site' => 'Веб-сайт',
            'region' => 'Регион',
            'fieldActivity' => 'Сфера деятельности компании',
            'companyName' => 'Название организации',
            'companyType' => 'Организационно-правовая форма',
            'sownArea' => 'Посевная площадь в хозяйстве',
            'sugarBeet' => 'Сахарная свёкла',
            'winterWheat' => 'Озимая пшеница',
            'springBarley' => 'Яровой ячмень',
            'potatoes' => 'Картофель',
            'corn' => 'Кукуруза на зерно',
            'rape' => 'Рапс',
            'sunflower' => 'Подсолнечник',
            'otherCultures' => 'Другие культуры',
            'treatmentEquipment' => 'Производитель и модель протравочного оборудования',
            'magazines' => 'Сельскохозяйственные издания',
            'message' => 'Вопрос'
        ];
        $formTypes = [
            FormBajarena::class => 'Заявка на Байарену',
            FormQuestion::class => 'Вопроса',
            FormJournalSubscription::class => 'Подписка на журнал',
            FormNewsletterSubscription::class => 'Подписка на рассылку',
            FormFusarium::class => 'Заявка на Фузариоз'
        ];

        $subject = $formTypes[get_class($form)];
        if (!$subject) {
            return false;
        }

        $body = '';
        foreach ($fields as $field=>$title) {
            $getter = 'get'.ucfirst($field);
            if (method_exists($form, $getter)) {
                if (strlen($form->$getter()) > 0) {
                    $body .= '<p><b>'.$title.':</b> '.$form->$getter().'</p>';
                }
            }
        }

        return $this->mailer->send($subject . ' ' . $form->getId(), $this->settings->value('email_order_notification'), $body);
    }
}