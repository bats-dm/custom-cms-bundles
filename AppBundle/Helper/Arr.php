<?php
namespace AppBundle\Helper;


class Arr
{
    /**
     * Choose random items from array.
     *
     * @param $array
     * @param int $count
     * @param null $seed
     * @return array
     */
    public static function arrayRand($array, $count = 1, $seed = null)
    {
        if (count($array) <= $count) {
            return $array;
        }

        $arrayResult = [];
        while (count($arrayResult) < $count) {
            if ($seed) {
                mt_srand(intval($seed.count($arrayResult)));
            }
            $key = mt_rand(0, count($array)-1);
            $arrayResult[] = $array[$key];
            array_splice($array, $key, 1);
        }

        if ($seed) {
            mt_srand();
        }

        return $arrayResult;
    }
}