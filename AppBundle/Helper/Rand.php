<?php
namespace AppBundle\Helper;


class Rand
{
    /**
     * Return seed regenerated on in 3 months
     *
     * @return int
     */
    public static function getThreeMonthSeed()
    {
        return date('Y')*100+ceil(date('n')/3);
    }
}