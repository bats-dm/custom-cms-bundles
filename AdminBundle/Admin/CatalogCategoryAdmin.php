<?php

namespace AdminBundle\Admin;

use AppBundle\Entity\CatalogCategory;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class CatalogCategoryAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    public function configure()
    {
        $this->listModes['tree'] = [
            'class' => 'fa fa-tree fa-fw',
        ];
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild(
            'Категория',
            array('uri' => $admin->generateUrl('edit', array('id' => $id)))
        );
        $menu->addChild(
            'Теги',
            array('uri' => $admin->generateUrl('admin.admin.catalog_category_tag.list', array('id' => $id)))
        );
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('tree-move');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('slug')
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('slug')
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('Doctrine');
        $subject = $this->getSubject();

        $imageOptions = ['required' => false, 'label' => false];
        if ($this->getSubject()->getImage()) {
            $src = '/' . $this->getConfigurationPool()->getContainer()->getParameter('catalog_category_dir') . $this->getSubject()->getImage();
            $imageOptions['help'] = '<p><img src="' . $src . '" class="admin-preview"></p>';
        }

        $parentOptions = [
            'query_builder' => function (EntityRepository $er) use($subject) {
                $qb = $er->createQueryBuilder('p');
                $qb->orderBy('p.name', 'ASC');
                if ($subject->getId()) {
                    $qb->where('p.id<>:id')->setParameter('id', $subject->getId());
                }
                return $qb;
            }
        ];

        if (!$subject->getId() && ($pid = $this->getRequest()->get('parent_id'))) {
            $parentOptions['data'] = $doctrine->getRepository('AppBundle:CatalogCategory')->find($pid);
        }

        $formMapper
            ->with('Наполнение', ['class' => 'col-md-8'])
                ->add('description', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('descriptionAdvanced', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
            ->end()
            ->with('Основные', ['class' => 'col-md-4'])
                ->add('name')
                ->add('parent', null, $parentOptions)
                ->add('type')
                ->add('hideProducts')
                ->add('enabled')
            ->end()
            ->with('Изображение', ['class' => 'col-md-4'])
                ->add('uploadedImage', 'file', $imageOptions)
                ->add('imageLink')
            ->end()
            ->with('SEO', ['class' => 'col-md-4'])
                ->add('slug', null, ['required'=>false])
                ->add('title')
                ->add('metaKeywords')
                ->add('metaDescription')
                ->add('sitemapPriority')
            ->end()
            ->with('Страница', ['class' => 'col-md-4'])
                ->add('pageHead')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('slug')
            ->add('name')
        ;
    }

    public function postUpdate($object)
    {
        $this->moveImages($object);
    }

    public function postPersist($object)
    {
        $this->moveImages($object);
    }

    public function preRemove($object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->remove(
            $this->getItemName($object->getId())
        );
    }

    public function getItemName($objectId)
    {
        return 'catalog_category_' . $objectId;
    }

    public function moveImages(CatalogCategory $object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->move(
            $object->getDescription().$object->getDescriptionAdvanced(),
            $this->getItemName($object->getId())
        );
    }
}
