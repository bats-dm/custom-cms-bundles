<?php

namespace AdminBundle\Admin;

use AdminBundle\Form\Type\ProductRelationParamType;
use AppBundle\Entity\CatalogProduct;
use AppBundle\Entity\CatalogProductParam;
use AppBundle\Entity\CatalogProductsParamValues;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class CatalogProductAdmin extends AbstractAdmin
{
    protected function getEm()
    {
        return $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild(
            'Товар',
            array('uri' => $admin->generateUrl('edit', array('id' => $id)))
        );
        $menu->addChild(
            'Вложения',
            array('uri' => $admin->generateUrl('admin.admin.catalog_product_attachment.list', array('id' => $id)))
        );
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('slug')
            ->add('category')
            ->add('categoryTags', null, [], null, [
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p')
                        ->leftJoin('p.category', 'c')
                        ->orderBy('c.lft, p.name', 'ASC');
                    return $qb;
                },
                'group_by' => function ($val, $key, $index) {
                    return $val->getCategory()->getName();
                }
            ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('slug')
            ->add('category.name')
            ->add('categoryTags')
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getEm();
        $repo = $em->getRepository('AppBundle:CatalogProduct');
        /** @var CatalogProduct $product */
        $product = $this->getSubject();

        $imageOptions = ['required' => false];
        if ($this->getSubject()->getImage()) {
            $srcM = '/' . $this->getConfigurationPool()->getContainer()->getParameter('catalog_product_dir') . $this->getSubject()->getImageM();
            $src = '/' . $this->getConfigurationPool()->getContainer()->getParameter('catalog_product_dir') . $this->getSubject()->getImage();
            $imageOptions['help'] = '<p><a href="' . $src . '" target="_blank"><img src="' . $srcM . '" class="admin-preview"></a></p>';
        }

        $formMapper
            ->with('Наполнение', ['class' => 'col-md-8'])
                ->add('description', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('purpose', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('advantage', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('form', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('actionMechanism', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('activity', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('actionDuration', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('speed', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('phytotoxicity', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('selectivity', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('resistance', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('compatibility', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('applicationMethod', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('shelfLife', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('slogan', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('subtitle', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('cleaningEquipment', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('packaging', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('recommendations', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('preparationMethod', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('useInPrivateFarms', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('target', 'textarea', [
//                    'attr' => [
//                        'class' => 'tinymce-full'
//                    ],
                    'required' => false
                ])
                ->add('type', 'textarea', [
                    'required' => false
                ])
                ->add('itAltname', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('itOverview', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('itInfo12Title', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->end()
            ->with('Основные', ['class' => 'col-md-4'])
            ->add('name')
            ->add('category', null, [
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p');
                    //$qb->andWhere('p.parent IS NULL');
                    $qb->orderBy('p.lft', 'ASC');
                    return $qb;
                }
            ])
            ->add('categoryTags', null, [
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p')
                        ->leftJoin('p.category', 'c')
                        ->orderBy('c.lft, p.name', 'ASC');
                    return $qb;
                },
                'group_by' => function ($val, $key, $index) {
                    return $val->getCategory()->getName();
                }
            ])
            ->add('uploadedImage', 'file', $imageOptions)
            ->add('enabled')
            ->end();

        if ($product) {
            if ($params = $repo->params($product)) {
                $formMapper->with('Параметры', ['class' => 'col-md-4']);
                /** @var CatalogProductParam $param */
                foreach ($params as $param) {
                    switch ($param->getType()) {
                        case CatalogProductParam::TYPE_CHOICE:
                            break;
                        case CatalogProductParam::TYPE_MULTICHOICE:
                            if ($param->getRelation()) {
                                $formMapper->add('param-' . $param->getId(), ProductRelationParamType::class, [
                                    'label' => false,
                                    'product' => $product,
                                    'paramId' => $param->getId(),
                                    'mapped' => false,
                                    'required' => false
                                ]);
                            } else {
                                $values = [];
                                foreach ($param->getValues() as $value) {
                                    $values[$value->getValue()] = $value->getId();
                                }
                                $data = [];
                                foreach ($product->getParamValues() as $productParamValue) {
                                    if ($productParamValue->getParamValue() && $productParamValue->getParamValue()->getParam() == $param) {
                                        $data[] = $productParamValue->getParamValue()->getId();
                                    }
                                }

                                $formMapper->add('param-' . $param->getId(), 'choice', [
                                    'label' => $param->getName(),
                                    'data' => $data,
                                    'mapped' => false,
                                    'multiple' => true,
                                    'choices' => $values,
                                    'required' => false
                                ]);
                            }
                            break;
                    }
                }
                $formMapper->end();
            }
        }

        $formMapper
            ->with('SEO', ['class' => 'col-md-4'])
                ->add('slug', null, ['required' => false])
                ->add('title')
                ->add('metaKeywords')
                ->add('metaDescription')
                ->add('sitemapPriority')
            ->end()
            ->with('Страница', ['class' => 'col-md-4'])
                ->add('pageHead')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('slug')
            ->add('name');
    }

    public function postUpdate($object)
    {
        $this->moveImages($object);
        $this->saveParams($object);
    }

    public function postPersist($object)
    {
        $this->moveImages($object);
    }

    public function preRemove($object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->remove(
            $this->getItemName($object->getId())
        );
    }

    public function saveParams(CatalogProduct $product)
    {
        $paramName = 'param-';
        $em = $this->getEm();
        $repo = $em->getRepository('AppBundle:CatalogProduct');

        foreach ($product->getParamValues() as $paramValue) {
            $em->remove($paramValue);
        }
        $em->flush();

        foreach ($this->getForm()->all() as $field=>$formData) {
            if (strpos($field, $paramName) === 0) {
                $paramId = (int) str_replace($paramName, '', $field);
                /** @var CatalogProductParam $param */
                $param = $em->getRepository('AppBundle:CatalogProductParam')->find($paramId);
                if ($param->getRelation()) {
                    foreach ($formData->getData() as $paramValueId=>$relationParamValues) {
                        if ($paramValueId) {
                            $ppv = (new CatalogProductsParamValues())
                                ->setParamValue($em->getRepository('AppBundle:CatalogProductParamValue')->find($paramValueId))
                                ->setProduct($product)
                            ;
                            $em->persist($ppv);
                            if ($relationParamValues) {
                                foreach ($relationParamValues as $relationParamValueId) {
                                    $rppv = (new CatalogProductsParamValues())
                                        ->setParent($ppv)
                                        ->setParamValue($em->getRepository('AppBundle:CatalogProductParamValue')->find($relationParamValueId))
                                        ->setProduct($product)
                                    ;
                                    $em->persist($rppv);
                                }
                            }
                        }
                    }
                } else {
                    foreach ($formData->getData() as $paramValueId) {
                        $ppv = (new CatalogProductsParamValues())
                            ->setParamValue($em->getRepository('AppBundle:CatalogProductParamValue')->find($paramValueId))
                            ->setProduct($product)
                        ;
                        $em->persist($ppv);
                    }
                }
            }
        }

        $em->flush();
    }

    public function getItemName($objectId)
    {
        return 'catalog_product_' . $objectId;
    }

    public function moveImages(CatalogProduct $object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->move(
            $object->getDescription().$object->getPurpose().$object->getAdvantage().$object->getForm().$object->getActionMechanism().$object->getActivity().$object->getActionDuration().$object->getSpeed().$object->getPhytotoxicity().$object->getSelectivity().$object->getResistance().$object->getCompatibility().$object->getApplicationMethod().$object->getShelfLife().$object->getSlogan().$object->getSubtitle().$object->getCleaningEquipment().$object->getPackaging().$object->getRecommendations().$object->getPreparationMethod().$object->getUseInPrivateFarms().$object->getTarget(),
            $this->getItemName($object->getId())
        );
    }
}
