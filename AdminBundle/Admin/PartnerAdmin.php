<?php

namespace AdminBundle\Admin;

use AppBundle\Entity\PartnerAbstract;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityRepository;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PartnerAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('fax')
            ->add('email')
            ->add('site')
            ->add('name')
            ->add('address')
            ->add('postcode')
            ->add('enabled')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('region.name')
            ->add('city.name')
            ->add('address')
            ->add('postcode')
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $russiaCountry = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager')->getRepository('AppBundle:GeoCountry')->findOneBy(['alfa2'=>'RU']);

        $formMapper
            ->add('name')
            ->add('region', null, [
                'query_builder' => function (EntityRepository $er) use ($russiaCountry) {
                    return $er->createQueryBuilder('p')
                        ->where('p.country=:country')
                        ->setParameter('country', $russiaCountry)
                        ->orderBy('p.name', 'ASC');
                },
                'attr' => [
                    'class' => 'form-region'
                ]
            ])
            ->add('city', null, [
                'attr' => [
                    'class' => 'form-city'
                ]
            ])
            ->add('address')
            ->add('postcode')
            ->add('email_list', 'collection', [
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('phone_list', 'collection', [
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('fax')
            ->add('site')
            ->add('enabled')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('region')
            ->add('city')
            ->add('address')
            ->add('email_list', 'array')
            ->add('phone_list', 'array')
            ->add('fax')
            ->add('site')
            ->add('latitude')
            ->add('longitude')
            ->add('enabled')
        ;
    }

    public function prePersist($object)
    {
        $this->setLatLong($object);
    }

    public function preUpdate($object)
    {
        $this->setLatLong($object);
    }

    public function setLatLong(PartnerAbstract $object)
    {
        try {
            $url = "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=".urlencode($object->getFullAddress());
            $data = file_get_contents($url);
            $data = json_decode(file_get_contents($url), true);
            if($data && is_array($data) && isset($data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"])) {
                $data = explode(" ", $data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"]);
                $object->setLongitude($data[0]);
                $object->setLatitude($data[1]);
            }
        } catch (\Exception $e) {}
    }
}
