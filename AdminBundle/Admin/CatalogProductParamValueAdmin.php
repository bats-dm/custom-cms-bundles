<?php

namespace AdminBundle\Admin;

use AppBundle\Entity\CatalogProductParam;
use AppBundle\Entity\CatalogProductParamValue;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class CatalogProductParamValueAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'param';

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('value')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('value')
            ->add('position')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var CatalogProductParamValue $subject */
        $subject = $this->getSubject();
        $imageOptions = ['required' => false];
        if ($this->getSubject()->getImage()) {
            $src = '/' . $this->getConfigurationPool()->getContainer()->getParameter('catalog_product_param_value_dir') . $this->getSubject()->getImage();
            $imageOptions['help'] = '<p><a href="' . $src . '" target="_blank"><img src="' . $src . '" class="admin-preview"></a></p>';
        }
        $formMapper->add('value');
        $formMapper->add('position');
        $formMapper->add('uploadedImage', 'file', $imageOptions);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('value')
            ->add('position')
        ;
    }
}
