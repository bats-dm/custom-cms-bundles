<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FormAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('email')
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
//        if ($this->getClass() == 'AppBundle\Entity\FormBajarena') {}
        $listMapper
            ->add('id')
            ->add('createdAt', null, ['format' => 'd-m-Y H:i'])
            ->add('email')
            ->add('name')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $class = get_class($this->getSubject());

//        $showMapper
//            ->add('id')
//            ->add('email')
//            ->add('name')
//            ->add('createdAt', null, ['format' => 'd-m-Y H:i'])
//        ;
        switch ($class) {
            case 'AppBundle\Entity\FormBajarena':
                $showMapper
                    ->with('#1', ['class' => 'col-md-6'])
                        ->add('createdAt', null, ['format' => 'd-m-Y H:i'])
                        ->add('region', null, ['label'=>null])
                        ->add('fieldActivity')
                        ->add('companyName')
                        ->add('companyType')

                        ->add('sownArea')
                        ->add('sugarBeet')
                        ->add('winterWheat')
                        ->add('springBarley')
                        ->add('potatoes')
                        ->add('corn')
                        ->add('rape')
                        ->add('sunflower')
                        ->add('otherCultures')
                        ->add('treatmentEquipment')

                        ->add('magazines')
                    ->end()
                    ->with('#2', ['class' => 'col-md-6'])
                        ->add('lastName')
                        ->add('name')
                        ->add('middleName')
                        ->add('post')

                        ->add('postcode')
                        ->add('postAddress')
                        ->add('phoneCode')
                        ->add('phone')
                        ->add('fax')
                        ->add('email')
                        ->add('additionalEmail')
                        ->add('site')
                    ->end()
                ;
                break;
            case 'AppBundle\Entity\FormQuestion':
                $showMapper
                    ->add('createdAt', null, ['format' => 'd-m-Y H:i'])
                    ->add('name')
                    ->add('companyName')
                    ->add('post')
                    ->add('email')
                    ->add('phone')
                    ->add('message')
                ;
                break;
            case 'AppBundle\Entity\FormJournalSubscription':
                $showMapper
                    ->with('#1', ['class' => 'col-md-6'])
                        ->add('createdAt', null, ['format' => 'd-m-Y H:i'])
                        ->add('postAddress')
                        ->add('postcode')
                        ->add('phone')
                        ->add('email')
                    ->end()
                    ->with('#2', ['class' => 'col-md-6'])
                        ->add('name')
                        ->add('companyName')
                        ->add('post')
                    ->end()
                ;
                break;
            case 'AppBundle\Entity\FormNewsletterSubscription':
            case 'AppBundle\Entity\FormFusarium':
                $showMapper
                    ->add('createdAt', null, ['format' => 'd-m-Y H:i'])
                    ->add('email')
                    ->add('name')
                ;
                break;
        }
    }
}
