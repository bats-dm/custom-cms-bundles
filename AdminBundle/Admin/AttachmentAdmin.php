<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class  AttachmentAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'object';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('file')
            ->add('fileExtension')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('file')
            ->add('fileSize')
            ->add('fileExtension')
            ->add('createdAt', null, [
                'format' => 'd-m-Y'
            ])
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $fileOptions = ['required' => false, 'label' => false];
        if ($this->getSubject()->getFile()) {
            $url = '/' . $this->getConfigurationPool()->getContainer()->getParameter('attachment_dir') . $this->getSubject()->getFile();
            $fileOptions['help'] = '<p><a href="' . $url . '" target="_blank">'.$this->getSubject()->getFile().'</a></p>';
        }
        $formMapper
            ->add('name')
            ->add('uploadedFile', 'file', $fileOptions)
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
        ;
    }
}
