<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class BannerAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('text1')
            ->add('text2')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('text1')
            ->add('text2')
            ->add('buttonText')
            ->add('link')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $imageOptions = ['required' => false];
        if ($this->getSubject()->getImage()) {
            $src = '/' . $this->getConfigurationPool()->getContainer()->getParameter('banner_dir') . $this->getSubject()->getImage();
            $imageOptions['help'] = '<p><img src="' . $src . '" class="admin-preview"></p>';
        }

        $formMapper
            ->add('uploadedImage', 'file', $imageOptions)
            ->add('text1')
            ->add('text2')
            ->add('buttonText')
            ->add('link')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('text1')
            ->add('text2')
            ->add('buttonText')
            ->add('link')
        ;
    }
}
