<?php

namespace AdminBundle\Admin;

use AppBundle\Entity\CatalogCategoryTag;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityRepository;

class CatalogCategoryTagAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'category';

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('slug')
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('slug')
            ->add('priority')
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Наполнение', ['class' => 'col-md-8'])
                ->add('description', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
                ->add('descriptionAdvanced', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
            ->end()
            ->with('Основные', ['class' => 'col-md-4'])
                ->add('name')
                ->add('priority', 'choice', [
                    'choices' => CatalogCategoryTag::getPriorities()
                ])
                ->add('enabled')
            ->end()
            ->with('SEO', ['class' => 'col-md-4'])
                ->add('slug', null, ['required'=>false])
                ->add('title')
                ->add('metaKeywords')
                ->add('metaDescription')
            ->end()
            ->with('Страница', ['class' => 'col-md-4'])
                ->add('pageHead')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('slug')
            ->add('name')
        ;
    }

    public function postUpdate($object)
    {
        $this->moveImages($object);
    }

    public function postPersist($object)
    {
        $this->moveImages($object);
    }

    public function preRemove($object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->remove(
            $this->getItemName($object->getId())
        );
    }

    public function getItemName($objectId)
    {
        return 'catalog_category_tag_' . $objectId;
    }

    public function moveImages(CatalogCategoryTag $object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->move(
            $object->getDescription().$object->getDescriptionAdvanced(),
            $this->getItemName($object->getId())
        );
    }
}
