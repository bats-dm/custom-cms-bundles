<?php

namespace AdminBundle\Admin;

use AppBundle\Entity\News;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class NewsAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'publicDate',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('publicDate', null, ['format' => 'd-m-Y'])
            ->add('name')
            ->add('enabled')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $imageOptions = ['required' => false];
        if ($this->getSubject()->getImageM()) {
            $src = '/' . $this->getConfigurationPool()->getContainer()->getParameter('news_dir') . $this->getSubject()->getImageM();
            $imageOptions['help'] = '<p><a href="' . $src . '" target="_blank"><img style="width: 200px;" src="' . $src . '" class="admin-preview"></a></p>';
        }
        $formMapper
            ->with('Наполнение', ['class' => 'col-md-8'])
                ->add('name')
                ->add('preview')
                ->add('text', 'textarea', [
                    'attr' => [
                        'class' => 'tinymce-full'
                    ],
                    'required' => false
                ])
            ->end()
            ->with('Основные', ['class' => 'col-md-4'])
                ->add('publicDate', null, [
                    'choice_translation_domain' => 'SonataAdminBundle'
                ])
                ->add('uploadedImageM', 'file', $imageOptions)
                ->add('enabled')
            ->end()
            ->with('SEO', ['class' => 'col-md-4'])
                ->add('title')
                ->add('metaKeywords')
                ->add('metaDescription')
                ->add('sitemapPriority')
            ->end()
            ->with('Страница', ['class' => 'col-md-4'])
                ->add('pageHead')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('preview')
            ->add('text')
            ->add('name')
            ->add('title')
            ->add('metaKeywords')
            ->add('metaDescription')
            ->add('enabled')
            ->add('publicDate')
            ->add('imageM')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    public function postUpdate($object)
    {
        $this->moveImages($object);
    }

    public function postPersist($object)
    {
        $this->moveImages($object);
    }

    public function preRemove($object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->remove(
            $this->getItemName($object->getId())
        );
    }

    public function getItemName($objectId)
    {
        return 'news_' . $objectId;
    }

    public function moveImages(News $object)
    {
        $this->getConfigurationPool()->getContainer()->get('cms.tinymce.imageuploader')->move(
            $object->getText(),
            $this->getItemName($object->getId())
        );
    }
}
