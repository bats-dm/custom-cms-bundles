<?php
namespace AdminBundle\Form\Type;

use AppBundle\Entity\CatalogProduct;
use AppBundle\Entity\CatalogProductsParamValues;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductRelationParamType extends AbstractType implements DataTransformerInterface
{
    protected $paramId;
    /** @var  CatalogProduct */
    protected $product;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->paramId = $options['paramId'];
        $this->product = $options['product'];

        $builder->addViewTransformer($this);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $param = $this->em->getRepository('AppBundle:CatalogProductParam')->find($this->paramId);
        $paramValues = [];
        foreach ($param->getValues() as $value) {
            $paramValues[$value->getId()] = $value->getValue();
        }
        $relationParamValues = [];
        foreach ($param->getRelation()->getValues() as $value) {
            $relationParamValues[$value->getId()] = $value->getValue();
        }

        $productParamValues = [];
        /** @var CatalogProductsParamValues $ppv */
        foreach ($this->product->getParamValues() as $ppv) {
            if ($ppv->getParamValue() && $ppv->getParamValue()->getParam() && $ppv->getParamValue()->getParam()->getId()==$this->paramId) {
                $productParamValues[$ppv->getParamValue()->getId()] = [];
                /** @var CatalogProductsParamValues $child */
                foreach ($ppv->getChildren() as $child) {
                    if ($child->getParamValue()) {
                        $productParamValues[$ppv->getParamValue()->getId()][] = $child->getParamValue()->getId();
                    }
                }
            }
        }

        $view->vars['param'] = $param;
        $view->vars['paramValues'] = $paramValues;
        $view->vars['relationParamValues'] = $relationParamValues;
        $view->vars['productParamValues'] = $productParamValues;
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function transform($data)
    {
        return $data;
    }

    public function reverseTransform($data)
    {
        return json_decode($data);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'paramId' => null,
            'product' => null
        ]);
    }
}