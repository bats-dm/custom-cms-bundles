<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\CatalogCategory;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CatalogCategoryAdminController extends CRUDController
{
    public function listAction()
    {
        $request = $this->getRequest();
        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        } else {
            if (!$this->admin->getRequest()->getSession()->get(sprintf('%s.list_mode', $this->admin->getCode()))) {
                $this->admin->setListMode('tree');
            }
        }
        $listMode = $this->admin->getListMode();
        if ($listMode === 'tree') {
            $request = $this->getRequest();
            $preResponse = $this->preList($request);
            if ($preResponse !== null) {
                return $preResponse;
            }
            return $this->render('AdminBundle::admin_catalog_category_tree.html.twig', array(
                'action' => 'list',
                'tree' => $this->getTree(),
                'csrf_token' => $this->getCsrfToken('sonata.batch'),
            ), null);
        }

        return parent::listAction();
    }

    public function treeMoveAction(Request $request)
    {
        $id = (int)$request->request->get('id');
        $positon = (int)$request->request->get('position');
        $oldPositon = (int)$request->request->get('old_position');
        $parentId = (int)str_replace('root_1', '', $request->request->get('parent_id'));

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:CatalogCategory');

        $category = $repo->find($id);
        $parent = $repo->find($parentId);

        if ($category->getParent() == $parent) {
            if ($positon > $oldPositon) {
                $repo->moveDown($category, $positon-$oldPositon);
            } else {
                $repo->moveUp($category, $oldPositon-$positon);
            }
        } else {
            $category->setParent($parent);
            $em->flush();

            if ($positon > 0) {
                $repo->moveDown($category, $positon);
            }
        }

        return new JsonResponse([]);
    }

    public function getTree($parentNode = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:CatalogCategory');

        $result = [];
        /** @var CatalogCategory $node */
        foreach ($repo->children($parentNode, true) as $node) {
            $resultItem = [
                'key' => $node->getId(),
                'title' => $node->getName(),
                'link' => [
                    'edit' => $this->admin->generateUrl('edit', ['id' => $node->getId()]),
                    'remove' => $this->admin->generateUrl('delete', ['id' => $node->getId()]),
                    'add' => $this->admin->generateUrl('create', ['parent_id' => $node->getId()])
                ],
                'columns' => [
                    $node->getEnabled() ? '<span class="label label-success"><i class="fa fa-unlock" aria-hidden="true"></i></span>' : '<span class="label label-danger"><i class="fa fa-lock" aria-hidden="true"></i></span>'
                ]
            ];
            if ($children = $this->getTree($node)) {
                $resultItem['children'] = $children;
            }
            $result[] = $resultItem;
        }

        return $result;
    }
}
