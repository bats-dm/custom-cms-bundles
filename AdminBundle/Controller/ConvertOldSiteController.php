<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\BookingArableArea;
use AppBundle\Entity\BookingOrganization;
use AppBundle\Entity\CatalogCategory;
use AppBundle\Entity\CatalogCategoryType;
use AppBundle\Entity\CatalogProduct;
use AppBundle\Entity\CatalogProductAttachment;
use AppBundle\Entity\CatalogProductParam;
use AppBundle\Entity\CatalogProductParamGroup;
use AppBundle\Entity\CatalogProductParamValue;
use AppBundle\Entity\FormBajarena;
use AppBundle\Entity\FormJournalSubscription;
use AppBundle\Entity\FormNewsletterSubscription;
use AppBundle\Entity\FormQuestion;
use AppBundle\Entity\GeoCity;
use AppBundle\Entity\GeoCountry;
use AppBundle\Entity\GeoFederalDistrict;
use AppBundle\Entity\GeoRegion;
use AppBundle\Entity\News;
use AppBundle\Entity\PageAttachment;
use AppBundle\Entity\Partner;
use AppBundle\Entity\PartnerContact;
use AppBundle\Entity\PartnerStorehouse;
use AppBundle\Menu\MenuCatalog;
use Behat\Transliterator\Transliterator;
use Intpill\CmsBundle\Entity\AdminUser;
use Intpill\CmsBundle\Entity\Menu;
use Intpill\CmsBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;

class ConvertOldSiteController extends Controller
{
    const MENU_MAIN_ID = 1;

    protected $oldEm;

    public function convertAllAction()
    {
        exit();

        $em = $this->getDoctrine()->getEntityManager();

        $path = $this->getParameter('kernel.root_dir').'/../www/';
        $convert = function ($entity, $field, $template, $folder) use ($path, $em) {
            $getter = 'get'.ucfirst($field);
            $imageName = $entity->$getter();
            $ext = pathinfo($imageName, PATHINFO_EXTENSION);

            $fileName = $template;
            preg_match_all('/{([^{}]+)}/', $template, $matches);
            foreach ($matches[1] as $index => $entityFields) {
                $search = $matches[0][$index];
                $replace = $entity;
                foreach (explode('.', $entityFields) as $entityField) {
                    $getter = 'get'.ucfirst($entityField);
                    $replace = $replace->$getter();
                }
                $replace = Transliterator::transliterate($replace);
                $replace = substr($replace, 0, 100);
                $fileName = str_replace($search, $replace, $fileName);
            }

            if ($fileName && $imageName) {
                $fileName = strtolower($fileName).'.'.$ext;
                if (@copy($path.$folder.$imageName, $path.$folder.$fileName)) {
                    unlink($path.$folder.$imageName);
                    $setter = 'set'.ucfirst($field);
                    $entity->$setter($fileName);
                    $em->flush();
                }
            }
        };

        $news = $em->getRepository('AppBundle:News')->findAll();
        foreach ($news as $newsOne) {
            $convert($newsOne, 'imageM', '{id}-{name}', $this->getParameter('news_dir'));
        }
        $params = $em->getRepository('AppBundle:CatalogProductParamValue')->findAll();
        foreach ($params as $param) {
            $convert($param, 'image', '{id}-{value}', $this->getParameter('catalog_product_param_value_dir'));
        }
        $categories = $em->getRepository('AppBundle:CatalogCategory')->findAll();
        foreach ($categories as $category) {
            $convert($category, 'image', '{id}-{name}', $this->getParameter('catalog_category_dir'));
        }
        $products = $em->getRepository('AppBundle:CatalogProduct')->findAll();
        foreach ($products as $product) {
            $convert($product, 'image', '{id}-{category.name}-{name}', $this->getParameter('catalog_product_dir'));
            $convert($product, 'imageM', '{id}-{category.name}-{name}-m', $this->getParameter('catalog_product_dir'));
            $convert($product, 'imageS', '{id}-{category.name}-{name}-s', $this->getParameter('catalog_product_dir'));
        }

        exit();

        /*
        $oldProducts = $this->fetchAll('catalogues_products');
        foreach ($oldProducts as $oldProduct) {
            $oldProductName = $this->fetchAll('catalogues_products_names', 'product_id=' . $oldProduct['product_id']);
            $oldProductName = $oldProductName[0];
            if ($oldProduct['alias']) {
                $product = $em->getRepository('AppBundle:CatalogProduct')->findOneBy(['slug'=>$oldProduct['alias']]);
                if ($product) {
                    $product->setItAltname($oldProductName['it_altname']);
                    $product->setItOverview($oldProductName['it_overview']);
                    $product->setItInfo12Title($oldProductName['it_info12_title']);
                }
            }
        }
        $em->flush();

        exit();

        set_time_limit(3600);

        $this->clearTables();
        if (!isset($_GET['do'])) {
            return $this->redirectToRoute('convert_old_site_all', ['do' => 1]);
        }

        foreach (['Вы представитель агробизнеса', 'Личное подсобное хозяйство (дача, огород, приусадебный участок)'] as $v) {
            $em->persist((new BookingOrganization())->setName($v));
        }
        $em->flush();
        foreach (['до 1000 га', '1000-5000 га', '5000-10000 га', '10000+ га'] as $v) {
            $em->persist((new BookingArableArea())->setValue($v));
        }
        $em->flush();

        //product params
        $categoryTypeSZR = (new CatalogCategoryType())->setName('СЗР');
        $categoryTypeSeeds = (new CatalogCategoryType())->setName('Семена');
        $em->persist($categoryTypeSZR);
        $em->persist($categoryTypeSeeds);

        $productParamGroupSZR = (new CatalogProductParamGroup())->setName('СЗР');
        $productParamGroupSeedsCorn = (new CatalogProductParamGroup())->setName('Семена кукурузы');
        $productParamGroupSeedsSunflower = (new CatalogProductParamGroup())->setName('Семена подсолнечника');
        $productParamGroupSeedsRapeseed = (new CatalogProductParamGroup())->setName('Семена рапса');
        $productParamGroupSeedsSorghum = (new CatalogProductParamGroup())->setName('Семена сорго');
        $em->persist($productParamGroupSZR);
        $em->persist($productParamGroupSeedsCorn);
        $em->persist($productParamGroupSeedsSunflower);
        $em->persist($productParamGroupSeedsRapeseed);
        $em->persist($productParamGroupSeedsSorghum);

        $productParamCulture = (new CatalogProductParam())->setName('Культура')->setGroup($productParamGroupSZR)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_CHOICE_ICONS);
        $productParamHarmfulObject = (new CatalogProductParam())->setName('Вредный объект')->setGroup($productParamGroupSZR)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_CHOICE);
        $productParamCulture->setRelation($productParamHarmfulObject);
        $productParamFao = (new CatalogProductParam())->setName('ФАО')->setGroup($productParamGroupSeedsCorn)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamPurpose = (new CatalogProductParam())->setName('Назначение')->setGroup($productParamGroupSeedsCorn)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamManufacturability = (new CatalogProductParam())->setName('Технологичность')->setGroup($productParamGroupSeedsCorn)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamBroomrapeResistance = (new CatalogProductParam())->setName('Устойчивость к заразихе')->setGroup($productParamGroupSeedsSunflower)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamTechnology = (new CatalogProductParam())->setName('Технология')->setGroup($productParamGroupSeedsSunflower)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamSeedsSunflowerRipenessGroup = (new CatalogProductParam())->setName('Группа спелости')->setGroup($productParamGroupSeedsSunflower)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamSeedsRapeseedRipenessGroup = (new CatalogProductParam())->setName('Группа спелости')->setGroup($productParamGroupSeedsRapeseed)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamSeedsSorghumRipenessGroup = (new CatalogProductParam())->setName('Группа спелости')->setGroup($productParamGroupSeedsSorghum)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamOilQuality = (new CatalogProductParam())->setName('Качество масла')->setGroup($productParamGroupSeedsSunflower)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamSuitabilityForMiniTillAndNoTill = (new CatalogProductParam())->setName('Пригодность для mini-till и no-till')->setGroup($productParamGroupSeedsSunflower)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $productParamType = (new CatalogProductParam())->setName('Тип')->setGroup($productParamGroupSeedsRapeseed)->setType(CatalogProductParam::TYPE_MULTICHOICE)->setFilterType(CatalogProductParam::FILTER_TYPE_MULTICHOICE_LIST);
        $em->persist($productParamCulture);
        $em->persist($productParamHarmfulObject);
        $em->persist($productParamFao);
        $em->persist($productParamPurpose);
        $em->persist($productParamManufacturability);
        $em->persist($productParamBroomrapeResistance);
        $em->persist($productParamTechnology);
        $em->persist($productParamSeedsSunflowerRipenessGroup);
        $em->persist($productParamSeedsRapeseedRipenessGroup);
        $em->persist($productParamSeedsSorghumRipenessGroup);
        $em->persist($productParamOilQuality);
        $em->persist($productParamSuitabilityForMiniTillAndNoTill);
        $em->persist($productParamType);

        $addParamValue = function ($param, $values) use ($em) {
            foreach ($values as $image=>$value) {
                $obj = (new CatalogProductParamValue())->setParam($param)->setValue($value);
                if (!is_numeric($image)) {
                    @copy(
                        $this->getParameter('kernel.root_dir') . '/../web/assets/img/' . $image,
                        $this->getParameter('kernel.root_dir') . '/../web/' . $this->getParameter('catalog_product_param_value_dir') . $image
                    );
                    $obj->setImage($image);
                }
                $em->persist($obj);
            }
        };
        $addParamValue($productParamCulture, ['choise3.png'=>'Яровая пшеница', 'Яровой ячмень', 'Озимый ячмень', 'choise2.png'=>'Озимая пшеница', 'Озимая рожь', 'choise5.png'=>'Кукуруза', 'Овес', 'choise6.png'=>'Сахарная свекла', 'Рапс', 'Пшеница', 'choise4.png'=>'Ячмень', 'Подсолнечник', 'Сахарная свекла', 'choise7.png'=>'Картофель', 'Просо', 'Лен-долгунец', 'Лен масличный', 'Горох']);
        $addParamValue($productParamHarmfulObject, ['Твердая головня', 'Пыльная головня', 'Фузариозная корневая гниль', 'Гельминтоспориозная корневая гниль', 'Снежная плесень', 'Плесневение семян', 'Септориоз', 'Каменная головня', 'Ложная пыльная головня', 'Сетчатая пятнистость', 'Стеблевая головня', 'Тифулез', 'Крестоцветные блошки', 'Корневые гнили', 'Питиоз', 'Фузариоз', 'Альтернариоз', 'Хлебная жужелица', 'Полосатая хлебная блошка', 'злаковые мухи тли', 'Проволочники', 'Долгоносики', 'Комплекс вредителей всходов', 'Колорадский жук', 'Тли', 'Ризоктониоз', 'Парша обыкновенная', 'Фузариозная снежная плесень', 'Тифулезная снежная плесень', 'Покрытая головня', 'Красно-бурая пятнистость', 'Крапчатость', 'Антракноз', 'Бурая ржавчина', 'Головня метелок', 'Пшеничная муха', 'Обыкновенная шведская муха', 'Гельминтоспориозная корневаягниль', 'Ризоктониозная прикорневая гниль', 'Ячменная шведская муха', 'Лаковые тли', 'злаковые тли', 'Cетчатая пятнисгость', 'Парша серебристая']);
        $addParamValue($productParamFao, ['160-199', '200-299', '300-399']);
        $addParamValue($productParamPurpose, ['Крупа', 'Зерно', 'Силос', 'Пищевая']);
        $addParamValue($productParamManufacturability, ['Интенсивные', 'Экстенсивные', 'Пригодность для no till и mini till']);
        $addParamValue($productParamBroomrapeResistance, ['A-E', 'A-F', 'A-G', 'G+']);
        $addParamValue($productParamTechnology, ['Классическая', 'Clearfield', 'Clearfield Plus', 'Сульфо']);
        $addParamValue($productParamSeedsSunflowerRipenessGroup, ['Раннеспелые', 'Среднеранние', 'Среднеспелые']);
        $addParamValue($productParamSeedsRapeseedRipenessGroup, ['Очень ранний', 'Ранний', 'Средне-ранний', 'Средний', 'Средне-поздний']);
        $addParamValue($productParamSeedsSorghumRipenessGroup, ['Ранний', 'Средне-ранний', 'Средне-поздний']);
        $addParamValue($productParamOilQuality, ['Линолевые', 'Высокоолеиновые']);
        $addParamValue($productParamSuitabilityForMiniTillAndNoTill, ['Да', 'Нет']);
        $addParamValue($productParamType, ['Гибрид F1', 'Линейный сорт']);

        $categoryTypeSZR->addProductParam($productParamCulture);

        $em->flush();

        //base cms data
        $admin = (new AdminUser())
            ->setUsername('admin_dev')
            ->setRole('ROLE_ADMIN_DEV')
            ->setEnabled(true)
        ;
        $admin->setPassword($this->get('security.password_encoder')->encodePassword($admin, '147852'));
        $em->persist($admin);

        $admin = (new AdminUser())
            ->setUsername('admin')
            ->setRole('ROLE_ADMIN_FULL')
            ->setEnabled(true)
        ;
        $admin->setPassword($this->get('security.password_encoder')->encodePassword($admin, 'admin'));
        $em->persist($admin);

        $homePage = (new Page())
            ->setName('Главная')
            ->setType(Page::TYPE_MODULE)
            ->setModule('home')
            ->setSlug('main')
            ->setTitle('Bayer CropScience: Bayer CropScience Россия')
            ->setMetaDescription('Bayer Crop Science — ваш партнер на пути к успеху')
            ->setMetaKeywords('Bayer, CropScience, Байер, КропСайенс, защита растений, средства защиты растений, СЗР, гербициды, фунгициды, инсектициды, семена полевых культур, семена овощных культур, техника для протравливания')
            ->setIsHome(true)
            ->setEnabled(true)
        ;
        $em->persist($homePage);
        $em->flush();

        $mainMenu = (new Menu())
            ->setName('Главное меню')
            ->setType(Menu::TYPE_ROOT)
            ->setEnabled(true)
        ;
        $em->persist($mainMenu);
        $em->flush();

        $page = (new Page())
            ->setName('Каталог продукции')
            ->setType(Page::TYPE_MODULE)
            ->setParent($homePage)
            ->setSlug('products')
            ->setModule('catalog')
            ->setMenuClass(MenuCatalog::class)
            ->setEnabled(true)
        ;
        $em->persist($page);
        $em->flush();

        $menu = (new Menu())
            ->setName('Каталог продукции')
            ->setPage($page)
            ->setParent($mainMenu)
            ->setEnabled(true)
        ;
        $em->persist($menu);
        $em->flush();

        $page = (new Page())
            ->setName('Каталог - Теги')
            ->setType(Page::TYPE_MODULE)
            ->setParent($homePage)
            ->setSlug('tags')
            ->setModule('catalog_tags')
            ->setEnabled(true)
        ;
        $em->persist($page);
        $em->flush();

        $page = (new Page())
            ->setName('Товары')
            ->setType(Page::TYPE_MODULE)
            ->setParent($homePage)
            ->setSlug('product')
            ->setModule('product')
            ->setEnabled(true)
        ;
        $em->persist($page);
        $em->flush();

        //geo
        $oldFederalDistricts = $this->fetchAll('geo_federal_districts');
        foreach ($oldFederalDistricts as $oldFederalDistrict) {
            $oldFederalDistrictsNames = $this->fetchAll('geo_federal_districts_names', 'federal_district_id='.$oldFederalDistrict['federal_district_id']);
            if (!empty($oldFederalDistrictsNames[0])) {
                $fd = (new GeoFederalDistrict())
                    ->setName($oldFederalDistrictsNames[0]['name'])
                ;
                $em->persist($fd);
            }
        }
        $countryDependency = [];
        $oldCountries = $this->fetchAll('geo_countries');
        foreach ($oldCountries as $oldCountry) {
            $oldCountriesNames = $this->fetchAll('geo_countries_names', 'country_id='.$oldCountry['country_id']);
            if (!empty($oldCountriesNames[0])) {
                $country = (new GeoCountry())
                    ->setAlfa2($oldCountry['alfa2'])
                    ->setAlfa3($oldCountry['alfa3'])
                    ->setName($oldCountriesNames[0]['name'])
                    ->setShortName($oldCountriesNames[0]['short_name'])
                ;
                $em->persist($country);
                $countryDependency[$oldCountry['country_id']] = $country;
            }
        }
        $regionDependency = [];
        $oldRegions = $this->fetchAll('geo_regions');
        foreach ($oldRegions as $oldRegion) {
            $oldRegionsNames = $this->fetchAll('geo_regions_names', 'region_id='.$oldRegion['region_id']);
            if (!empty($oldRegionsNames[0])) {
                $region = (new GeoRegion())
                    ->setName($oldRegionsNames[0]['name'])
                ;
                if (!empty($countryDependency[$oldRegion['country_id']])) {
                    $region->setCountry(
                        $countryDependency[$oldRegion['country_id']]
                    );
                }
                $em->persist($region);
                $regionDependency[$oldRegion['region_id']] = $region;
            }
        }
        $cityDependency = [];
        $oldCities = $this->fetchAll('geo_cities');
        foreach ($oldCities as $oldCity) {
            $oldCitiesNames = $this->fetchAll('geo_cities_names', 'city_id='.$oldCity['city_id']);
            if (!empty($oldCitiesNames[0])) {
                $city = (new GeoCity())
                    ->setName($oldCitiesNames[0]['name'])
                    ->setLatitude($oldCity['latitude'])
                    ->setLongitude($oldCity['longitude'])
                ;
                if (!empty($countryDependency[$oldCity['country_id']])) {
                    $city->setCountry(
                        $countryDependency[$oldCity['country_id']]
                    );
                }
                if (!empty($regionDependency[$oldCity['region_id']])) {
                    $city->setRegion(
                        $regionDependency[$oldCity['region_id']]
                    );
                }
                $em->persist($city);
                $cityDependency[$oldCity['city_id']] = $city;
            }
        }

        //partners
        $oldPartners = $this->fetchAll('partners');
        foreach ($oldPartners as $oldPartner) {
            $oldPartnersNames = $this->fetchAll('partners_names', 'partner_id='.$oldPartner['partner_id']);
            if (!empty($oldPartnersNames[0])) {
                $partner = null;
                switch ($oldPartner['partner_type_id']) {
                    case 1:
                        $partner = new Partner();
                        break;
                    case 2:
                        $partner = new PartnerContact();
                        break;
                    case 3:
                        $partner = new PartnerStorehouse();
                        break;
                }

                $partner
                    ->setDescription($oldPartnersNames[0]['description'])
                    ->setAddress($oldPartnersNames[0]['address'])
                    ->setName($oldPartnersNames[0]['name'])
                    ->setCreatedAt(new \DateTime($oldPartner['created']))
                    ->setUpdatedAt(new \DateTime($oldPartner['updated']))
                    ->setEnabled($oldPartner['status_id']==1)
                    ->setSite($oldPartner['site'])
                    ->setEmail($oldPartner['email'])
                    ->setFax($oldPartner['fax'])
                    ->setPhones($oldPartner['phones'])
                    ->setLongitude($oldPartner['longitude'])
                    ->setLatitude($oldPartner['latitude'])
                ;
                if (!empty($regionDependency[$oldPartner['region_id']])) {
                    $partner->setRegion(
                        $regionDependency[$oldPartner['region_id']]
                    );
                }
                if (!empty($cityDependency[$oldPartner['city_id']])) {
                    $partner->setCity(
                        $cityDependency[$oldPartner['city_id']]
                    );
                }
                $em->persist($partner);
            }
        }
        $em->flush();

        $ids = [380, 378, 381, 382, 379];
        $sectionDependency = [];
        foreach ($ids as $id) {
            $oldSitesSectionName = $this->fetchAll('sites_sections_names', 'section_id=' . $id);
            $page = (new Page())
                ->setName($oldSitesSectionName[0]['name'])
                ->setType(Page::TYPE_PAGE_LINK)
                ->setParent($homePage)
                ->setEnabled(true)
            ;
            $em->persist($page);
            $em->flush();
            $sectionDependency[$id] = $page;
        }

        //pages
        $pageDependency = [];
        $oldSitesPages = $this->fetchAll('sites_pages');
        foreach ($oldSitesPages as $key => $oldSitesPage) {
            $oldSitesPageName = $this->fetchAll('sites_pages_names', 'page_id=' . $oldSitesPage['page_id'], 'slug DESC');
            $slug = $oldSitesPageName[0]['slug'];
            if (!$slug) {
                $oldPath = $this->fetchAll('sites_pathes', 'essence_id=6 and object_id=' . $oldSitesPage['page_id']);
                if (isset($oldPath[0]['path'])) {
                    $slug = basename($oldPath[0]['path'], '.html');
                    foreach ($pageDependency as $pd) {
                        if ($pd->getSlug() && $pd->getSlug() == $slug) {
                            $slug = null;
                        }
                    }
                }
            }
            if ($oldSitesPageName[0]['name'] && $slug && ($slug != 'admin')) {
                if ($slug == 'gsearch') $slug = 'search';
                $page = (new Page())
                    ->setName($this->fixText($oldSitesPageName[0]['name']))
                    ->setSlug($slug)
                    ->setTitle($oldSitesPageName[0]['meta-title'])
                    ->setMetaKeywords($oldSitesPageName[0]['meta-keywords'])
                    ->setMetaDescription($oldSitesPageName[0]['meta-description'])
                    ->setContent($oldSitesPageName[0]['content'])
                    ->setEnabled($oldSitesPage['status_id'] == 1 ? true : false)
                    ;
                switch ($slug) {
                    case 'search':
                        $page
                            ->setContent('')
                            ->setType(Page::TYPE_MODULE)
                            ->setModule('search')
                        ;
                        break;
                    case 'partners':
                        $page
                            ->setType(Page::TYPE_MODULE)
                            ->setModule('partner')
                        ;
                        break;
                    case 'contacts':
                        $page
                            ->setType(Page::TYPE_MODULE)
                            ->setModule('partner_contact')
                        ;
                        break;
                    case 'storehouses':
                        $page
                            ->setType(Page::TYPE_MODULE)
                            ->setModule('partner_storehouse')
                        ;
                        break;
                    case 'news':
                        $page
                            ->setType(Page::TYPE_MODULE)
                            ->setModule('news')
                        ;
                        break;
                }
                if ($oldSitesPage['section_id']) {
                    if (isset($sectionDependency[$oldSitesPage['section_id']])) {
                        $page->setParent($sectionDependency[$oldSitesPage['section_id']]);
                    } else if($oldSitesPage['section_id'] != 1) {
                        $oldSitesSection = $this->fetchAll('sites_sections', 'section_id=' . $oldSitesPage['section_id']);
                        if (isset($sectionDependency[$oldSitesSection[0]['parent_section_id']])) {
                            $page->setParent($sectionDependency[$oldSitesSection[0]['parent_section_id']]);
                        }
                    }
                }
                if (!$page->getParent()) {
                    $page->setParent($homePage);
                }
                $em->persist($page);

                $pageDependency[$oldSitesPage['page_id']] = $page;
            }
        }
        $em->flush();

        foreach ($this->getOldPagesRelations() as $oldPageParentId=>$oldPageIds) {
            if (isset($pageDependency[$oldPageParentId]) && ($pageParentId = $pageDependency[$oldPageParentId]->getId())) {
                foreach ($oldPageIds as $oldPageId) {
                    if (isset($pageDependency[$oldPageId]) && ($pageId = $pageDependency[$oldPageId]->getId())) {
                        $page = $em->getRepository('CmsBundle:Page')->find($pageId);
                        if ($page) {
                            $page->setParent($em->getRepository('CmsBundle:Page')->find($pageParentId));
                            $em->flush();
                        }
                    }
                }
            }
        }

        //menu
        $menuItems = $this->getMenuItems();
        foreach ($menuItems as $menuItem) {
            $menu = (new Menu())
                ->setName($menuItem['name'])
                ->setType(Menu::TYPE_PAGE)
                ->setEnabled(true)
                ;
            $menu->setParent($em->getRepository('CmsBundle:Menu')->find(self::MENU_MAIN_ID));
            $em->persist($menu);
            foreach ($menuItem['items'] as $menuSubItemOldId) {
                if (isset($pageDependency[$menuSubItemOldId])) {
                    $subMenu = (new Menu())
                        ->setName($pageDependency[$menuSubItemOldId]->getName())
                        ->setType(Menu::TYPE_PAGE)
                        ->setPage($pageDependency[$menuSubItemOldId])
                        ->setEnabled(true)
                    ;
                    $subMenu->setParent($menu);
                    $em->persist($subMenu);
                }
            }
        }

        //catalog
        $oldCategories = $this->fetchAll('catalogues_categories');
        $categoryDependency = [];
        foreach ($oldCategories as $oldCategory) {
            $oldCategoryName = $this->fetchAll('catalogues_categories_names', 'category_id='.$oldCategory['category_id']);
            $oldCategoryName = $oldCategoryName[0];

            $oldPath = $this->fetchAll('sites_pathes', 'add_params="categoryId=' . $oldCategory['category_id'] . '"');
            $slug = null;
            if (isset($oldPath[0]['path'])) {
                $slug = basename($oldPath[0]['path'], '.html');
            }
            if (!$slug) {
                $slug = $oldCategory['alias'];
            }

            $catalogCategory = (new CatalogCategory())
                ->setName($this->fixText($oldCategoryName['name']))
                ->setSlug($slug)
                ->setDescription($oldCategoryName['description'])
                ->setDescriptionAdvanced($oldCategoryName['text_advanced'])
                ->setTitle($oldCategoryName['meta-title'])
                ->setMetaKeywords($oldCategoryName['meta-keywords'])
                ->setMetaDescription($oldCategoryName['meta-description'])
                ->setEnabled($oldCategory['status_id'] == 1 ? true : false)
                ->setCreatedAt(new \DateTime($oldCategory['created']))
                ->setUpdatedAt(new \DateTime($oldCategory['updated']))
            ;
            if ($oldCategory['parent_category_id'] && isset($categoryDependency[$oldCategory['parent_category_id']])) {
                $catalogCategory->setParent($categoryDependency[$oldCategory['parent_category_id']]);
            }
            switch ($oldCategory['category_id']) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 8:
                    $catalogCategory->setType($categoryTypeSZR);
                    break;
                case 7:
                    $catalogCategory->setType($categoryTypeSeeds);
                    break;
                case 37:
                    $catalogCategory->addProductParam($productParamFao);
                    $catalogCategory->addProductParam($productParamPurpose);
                    $catalogCategory->addProductParam($productParamManufacturability);
                    break;
                case 38:
                    $catalogCategory->addProductParam($productParamBroomrapeResistance);
                    $catalogCategory->addProductParam($productParamTechnology);
                    $catalogCategory->addProductParam($productParamSeedsSunflowerRipenessGroup);
                    $catalogCategory->addProductParam($productParamOilQuality);
                    $catalogCategory->addProductParam($productParamSuitabilityForMiniTillAndNoTill);
                    break;
                case 35:
                case 36:
                    $catalogCategory->addProductParam($productParamType);
                    $catalogCategory->addProductParam($productParamSeedsRapeseedRipenessGroup);
                    break;
                case 39:
                    $catalogCategory->addProductParam($productParamSeedsSorghumRipenessGroup);
                    break;
            }
            $em->persist($catalogCategory);
            $categoryDependency[$oldCategory['category_id']] = $catalogCategory;
        }

        $oldProducts = $this->fetchAll('catalogues_products');
        $productDependency = [];
        foreach ($oldProducts as $oldProduct) {
            $oldProductName = $this->fetchAll('catalogues_products_names', 'product_id='.$oldProduct['product_id']);
            $oldProductName = $oldProductName[0];
            $catalogProduct = (new CatalogProduct())
                ->setName($this->fixText($oldProductName['name']))
                ->setSlug($oldProduct['alias'])
                ->setDescription($oldProductName['description'])
                ->setPurpose($oldProductName['purpose'])
                ->setAdvantage($oldProductName['advantage'])
                ->setForm($oldProductName['form'])
                ->setActionMechanism($oldProductName['action_mechanism'])
                ->setActivity($oldProductName['activity'])
                ->setActionDuration($oldProductName['action_duration'])
                ->setSpeed($oldProductName['speed'])
                ->setPhytotoxicity($oldProductName['phytotoxicity'])
                ->setSelectivity($oldProductName['selectivity'])
                ->setCompatibility($oldProductName['compatibility'])
                ->setApplicationMethod($oldProductName['application_method'])
                ->setShelfLife($oldProductName['shelf_life'])
                ->setSlogan($oldProductName['slogan'])
                ->setSubtitle($oldProductName['subtitle'])
                ->setCleaningEquipment($oldProductName['cleaning_equipment'])
                ->setRecommendations($oldProductName['recommendations'])
                ->setPreparationMethod($oldProductName['preparation_method'])
                ->setUseInPrivateFarms($oldProductName['use_in_private_farms'])
                ->setTarget($oldProductName['target'])
                ->setTitle($oldProductName['meta_title'])
                ->setMetaKeywords($oldProductName['meta_keywords'])
                ->setMetaDescription($oldProductName['meta_description'])
                ->setEnabled($oldProduct['status_id'] == 1 ? true : false)
                ->setCreatedAt(new \DateTime($oldProduct['created']))
                ->setUpdatedAt(new \DateTime($oldProduct['updated']))
            ;
            $catalogProduct->setImage($this->copyImage($oldProductName['icons'], 'catalog_product_dir'));
            $catalogProduct->setImageM($this->copyImage($oldProductName['m_icon'], 'catalog_product_dir'));
            $catalogProduct->setImageS($this->copyImage($oldProductName['s_icon'], 'catalog_product_dir'));

            $cpcc = $this->fetchAll('catalogues_products_catalogues_categories', 'product_id='.$oldProduct['product_id']);
            if ($cpcc = @$cpcc[0]) {
                $ccc = $this->fetchAll('catalogues_categories_catalogues', 'record_id='.$cpcc['record_id']);
                if (($oldCategoryId = intval($ccc[0]['category_id'])) && isset($categoryDependency[$oldCategoryId])) {
                    $catalogProduct->setCategory($categoryDependency[$oldCategoryId]);
                }
            }

            $em->persist($catalogProduct);
            $productDependency[$oldProduct['product_id']] = $catalogProduct;
        }

        //attachments
        $oldSitesAttachments = $this->fetchAll('sites_attachements');
        foreach ($oldSitesAttachments as $oldSitesAttachment) {
            $oldSitesAttachmentsNames = $this->fetchAll('sites_attachements_names', 'attachement_id='.$oldSitesAttachment['attachement_id']);
            if (isset($oldSitesAttachmentsNames[0])) {
                $attachment = null;
                if ($oldSitesAttachment['essence_id'] == 6) {
                    if (isset($pageDependency[$oldSitesAttachment['object_id']])) {
                        $attachment = (new PageAttachment())
                            ->setObject($pageDependency[$oldSitesAttachment['object_id']]);
                    }
                } else {
                    if (isset($productDependency[$oldSitesAttachment['object_id']])) {
                        $attachment = (new CatalogProductAttachment())
                            ->setObject($productDependency[$oldSitesAttachment['object_id']]);
                    }
                }
                if ($attachment) {
                    $file = __DIR__.'/../../../../ibayer.loc/'.$oldSitesAttachmentsNames[0]['path'];
                    if (file_exists($file)) {
                        $fileExt = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                        $fileSize = filesize($file);
                        $fileName = uniqid().'.'.$fileExt;

                        copy($file, __DIR__.'/../../../web/'.$this->getParameter('attachment_dir').$fileName);

                        $attachment->setName($oldSitesAttachmentsNames[0]['name'])
                            ->setFile($fileName)
                            ->setFileExtension($fileExt)
                            ->setFileSize($fileSize)
                            ->setCreatedAt(new \DateTime($oldSitesAttachment['created']))
                            ->setUpdatedAt(new \DateTime($oldSitesAttachment['updated']))
                        ;
                        $em->persist($attachment);
                    }
                }
            }
        }

        //forms
        $oldForms = $this->fetchAll('sites_forms_persons_data');
        foreach ($oldForms as $oldForm) {
            $form = null;
            if ($oldForm['form_id'] == 2) {
                $form = (new FormBajarena())
                    ->setPhone($oldForm['phone'])
                    ->setRegion($oldForm['bayarena_type'])
                    ->setFieldActivity($oldForm['field_activity'])
                    ->setCompanyName($oldForm['company_name'])
                    ->setCompanyType($oldForm['company_type'])
                    ->setLastName($oldForm['first_name'])
                    ->setMiddleName($oldForm['last_name'])
                    ->setPost($oldForm['position'])
                    ->setPostcode($oldForm['postcode'])
                    ->setPostAddress($oldForm['post_address'])
                    ->setPhoneCode($oldForm['phone_code'])
                    ->setFax($oldForm['fax'])
                    ->setAdditionalEmail($oldForm['adv_email'])
                    ->setSite($oldForm['site'])
                    ->setSownArea($oldForm['sown_area'])
                    ->setSugarBeet($oldForm['sugar_beet'])
                    ->setWinterWheat($oldForm['winter_wheat'])
                    ->setSpringBarley($oldForm['spring_barley'])
                    ->setPotatoes($oldForm['potatoes'])
                    ->setCorn($oldForm['corn'])
                    ->setRape($oldForm['rape'])
                    ->setSunflower($oldForm['sunflower'])
                    ->setOtherCultures($oldForm['other_cultures'])
                    ->setTreatmentEquipment($oldForm['treatment_equipment'])
                    ->setMagazines($oldForm['magazins'])
                    ->setName($oldForm['middle_name'])
                ;
            } elseif ($oldForm['form_id'] == 3) {
                $form = (new FormQuestion())
                    ->setCompanyName($oldForm['company_name'])
                    ->setPost($oldForm['position'])
                    ->setPhone($oldForm['phone'])
                    ->setMessage($oldForm['message'])
                    ->setName($oldForm['name'])
                ;
            } elseif ($oldForm['form_id'] == 5) {
                $form = (new FormJournalSubscription())
                    ->setPostAddress($oldForm['post_address'])
                    ->setPostcode($oldForm['postcode'])
                    ->setPhone($oldForm['phone'])
                    ->setCompanyName($oldForm['company_name'])
                    ->setPost($oldForm['position'])
                    ->setName($oldForm['name'])
                ;
            }
            if ($form) {
                $form
                    ->setEmail($oldForm['email'])
                    ->setCreatedAt(new \DateTime($oldForm['created']))
                ;
                $em->persist($form);
            }
        }

        $oldForms = $this->fetchAll('persons_subscribes');
        foreach ($oldForms as $oldForm) {
            $form = (new FormNewsletterSubscription())
                ->setName($oldForm['name'])
                ->setEmail($oldForm['email'])
                ->setCreatedAt(new \DateTime($oldForm['created']))
            ;
            $em->persist($form);
        }
        $em->flush();

        //news
        $oldBlogs = $this->fetchAll('blogs');
        foreach ($oldBlogs as $oldBlog) {
            $oldBlogsNames = $this->fetchAll('blogs_names', 'blog_id='.$oldBlog['blog_id']);
            if (isset($oldBlogsNames[0])) {
                if ($oldBlog['blog_type_id'] == 1) {
                    $news = (new News())
                        ->setName($oldBlogsNames[0]['title'])
                        ->setPreview($oldBlogsNames[0]['announce'])
                        ->setText($oldBlogsNames[0]['text'])
                        ->setTitle($oldBlogsNames[0]['meta-title'])
                        ->setMetaKeywords($oldBlogsNames[0]['meta-keywords'])
                        ->setMetaDescription($oldBlogsNames[0]['meta-description'])
                        ->setPublicDate(new \DateTime($oldBlog['public_date']))
                        ->setEnabled($oldBlog['status_id']==1)
                        ->setCreatedAt(new \DateTime($oldBlog['created']))
                        ->setUpdatedAt(new \DateTime($oldBlog['updated']))
                    ;
                    if ($oldBlog['image_min']) {
                        $file = __DIR__.'/../../../../ibayer.loc'.$oldBlog['image_min'];
                        if (file_exists($file)) {
                            $fileExt = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                            $fileName = uniqid().'.'.$fileExt;

                            copy($file, __DIR__.'/../../../web/'.$this->getParameter('news_dir').$fileName);

                            $news->setImageM($fileName);
                        }
                    }
                    $em->persist($news);
                }
            }
        }
        $em->flush();

        */

        foreach ($em->getRepository('CmsBundle:Page')->findAll() as $object) {
            $this->convertLinks($object);
        }
        $em->flush();
        foreach ($em->getRepository('AppBundle:CatalogCategory')->findAll() as $object) {
            $this->convertLinks($object);
        }
        $em->flush();
        foreach ($em->getRepository('AppBundle:CatalogProduct')->findAll() as $object) {
            $this->convertLinks($object);
        }
        $em->flush();
        foreach ($em->getRepository('AppBundle:News')->findAll() as $object) {
            $this->convertLinks($object);
        }
        $em->flush();

        $this->checkLinks();

        exit('');
    }

    public function fixText($text)
    {
        return str_replace(['', ''], '', $text);
    }

    public $convertedLinks = [];

    public function convertLinks($object)
    {
        $fields = ['content', 'preview', 'text', 'description', 'descriptionAdvanced', 'purpose', 'advantage', 'form', 'actionMechanism', 'activity', 'actionDuration', 'speed', 'phytotoxicity', 'selectivity', 'resistance', 'compatibility', 'applicationMethod', 'shelfLife', 'slogan', 'subtitle', 'cleaningEquipment', 'packaging', 'recommendations', 'preparationMethod', 'useInPrivateFarms', 'target', 'itAltname', 'itOverview', 'itInfo12Title'];

        foreach ($fields as $field) {
            $getter = 'get'.ucfirst($field);
            $setter = 'set'.ucfirst($field);
            if (method_exists($object, $getter)) {
                $crawler = new Crawler(
                    $object->$getter()
                );
                $search = [];
                $replace = [];
                $crawler->filter('a')->each(function (Crawler $node, $i) use (&$search, &$replace) {
                    if (($href = $node->attr('href')) && (strpos($href, 'http') === false) && (strpos($href, 'href') === false)) {
                        if (strpos($href, '/ru/') !== false || strpos($href, 'ru') === 0 || strpos($href, '.html') !== false) {
                            if (strpos($href, '#') !== 0) {
                                $result = $href;
                                if (strpos($result, 'ru/') === false) {
                                    $result = '/ru/' . $result;
                                }
                                $result = $this->getOldRedirect($href);
                                $result = strtolower($result);
                                $result = str_replace(['.html', '.htm', '../..'], ['', '', ''], $result);
                                $result = str_replace('_', '-', $result);
                                $bn = $this->baseName($result);

                                $slugAliases = [
                                    'bayarena-report-krasnodar-2011' => 'bayarena-report-krasnodar-2011-overview',
                                    'bayarena-krasnodar-partner' => 'bayarena-partners',
                                    'bayarena-kursk-partner' => 'bayarena-partners',
                                ];
                                if (isset($slugAliases[$bn])) {
                                    $bn = $slugAliases[$bn];
                                }

                                if (strpos($result, 'products') !== false) {
                                    if ($bn == 'products') {
                                        $result = '/products-list';
                                    } else {
                                        $result = '/products/' . $bn;
                                    }
                                } else {
                                    $result = '/' . $bn;
                                    if (!$this->checkLink($result)) {
                                        if ($this->checkProduct($result)) {
                                            $result = '/product/' . $bn;
                                        } else {
                                            $result = '#';
                                        }
                                    }
                                }
                                $this->convertedLinks[] = [$href, $result];
                                $search[] = $href;
                                $replace[] = $result;
                            }
                        }
                    }
                });
                if ($search && $replace) {
                    $object->$setter(str_replace($search, $replace, $object->$getter()));
                }
            }
        }
    }

    public $oldRedirects = null;

    public function getOldRedirect($path)
    {
        if (!$this->oldRedirects) {
            foreach ($this->fetchAll('sites_pathes', 'status_code=301') as $redirect) {
                $this->oldRedirects[$redirect['path']] = $redirect['result_url'];
            }
        }

        if (isset($this->oldRedirects[$path]) && !in_array($path, ['/ru/partners.html'])) {
            //dump($path . ' => ' . $this->oldRedirects[$path]);
            return $this->oldRedirects[$path];
        } else {
            return $path;
        }
    }

    public $cProducts = [];
    public function checkProduct($link)
    {
        $bn = $this->baseName($link);
        if (!$this->cProducts) {
            $em = $this->getDoctrine()->getManager();
            foreach($em->getRepository('AppBundle:CatalogProduct')->findAll() as $product) {
                $this->cProducts[$product->getSlug()] = 1;
            }
        }
        return isset($this->cProducts[$bn]);
    }

    public $clPages = [];
    public $clCategories = [];
    public function checkLink($link)
    {
        if (!$this->clPages) {
            $em = $this->getDoctrine()->getManager();
            foreach($em->getRepository('CmsBundle:Page')->findAll() as $page) {
                $this->clPages[$page->getSlug()] = 1;
            }
            foreach($em->getRepository('AppBundle:CatalogCategory')->findAll() as $category) {
                $this->clCategories[$category->getSlug()] = 1;
            }
        }

        $basename = $this->baseName($link);
        $isset = false;
        if (strpos($link, 'products') !== false) {
            $isset = isset($this->clCategories[$basename]);
        } elseif (strpos($link, 'product') !== false) {
            $isset = isset($this->cProducts[$basename]);
        } else {
            $isset = isset($this->clPages[$basename]);
        }

        return $isset;
    }

    public function baseName($str)
    {
        if ($pos = strpos($str, '#')) {
            $str = substr($str, 0, $pos);
        }
        return basename($str);
    }

    public function checkLinks()
    {
        foreach ($this->convertedLinks as $linkFull) {
            $link = $linkFull[1];
            if (!$this->checkLink($link)) {
                dump($linkFull[0].' :: '.$linkFull[1]);
            }
        }
    }

    public function copyImage($imageFrom, $dirParam)
    {
        if (!$imageFrom) return '';
        $ext = pathinfo($imageFrom, PATHINFO_EXTENSION);
        $newImageName = uniqid().'.'.$ext;
        @copy(
            $this->getParameter('kernel.root_dir') . '/../web' . $imageFrom,
            $this->getParameter('kernel.root_dir') . '/../web/' . $this->getParameter($dirParam) . $newImageName
        );
        return $newImageName;
    }

    protected function getOldPagesRelations()
    {
        return [
            168 => [167, 169, 165, 170, 171, 172, 173, 174, 183, 175, 176, 177, 178, 179, 180, 181, 182, 184, 185, 186, 187, 188, 189, 190, 191, 277, 246, 276, 245, 247, 273, 275, 274],
            198 => [158, 157, 156, 46]
        ];
    }

    protected function getMenuItems () {
        return [
            [
                'name' => 'Культуры',
                'items' => [209, 134, 135, 136, 141, 143, 132, 139, 140, 137, 138, 145, 222, 15]
            ],
            [
                'name' => 'Купить',
                'items' => [34, 35, 27, 36, 13]
            ],
            [
                'name' => 'Полевая академия',
                'items' => [42, 14, 168, 166, 76, 78, 86, 116]
            ],
            [
                'name' => 'БайАрена',
                'items' => [164, 160, 163, 64, 162, 63, 62, 161, 61, 16, 198]
            ],
            [
                'name' => 'О компании',
                'items' => [28, 50, 201, 33, 45, 85]
            ]
        ];
    }

    protected function clearTables()
    {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();
        $sm = $connection->getSchemaManager();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        foreach ($sm->listTables() as $table) {
            $q = $dbPlatform->getTruncateTableSql($table->getName());
            $connection->executeUpdate($q);
        }
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }

    protected function fetchAll($table, $where = '', $order = '', $group = '', $limit = '')
    {
        $query = $this->getOldEm()->getConnection()->prepare(
            "SELECT * FROM " . $table .
            ($where ? " WHERE " . $where : "") .
            ($group ? " GROUP BY " . $group : "") .
            ($order ? " ORDER BY " . $order : "") .
            ($limit ? " LIMIT " . $limit : "")
        );
        $query->execute();
        return $query->fetchAll();
    }

    protected function getOldEm()
    {
        if (!$this->oldEm) {
            $em = $this->getDoctrine()->getEntityManager();
            $conn = array(
                'driver'   => 'pdo_mysql',
                'user'     => 'root',
                'password' => '',
                'dbname'   => 'ibayer_dev'
            );
            $this->oldEm = \Doctrine\ORM\EntityManager::create(
                $conn,
                $em->getConfiguration(),
                $em->getEventManager()
            );
        }
        return $this->oldEm;
    }
}
