<?php
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GeoController extends Controller
{
    public function citiesByRegionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $region = $em->getRepository('AppBundle:GeoRegion')->find($request->get('region'));
        $result = $em->getRepository('AppBundle:GeoCity')->createQueryBuilder('c')
            ->select('c.id, c.name')
            ->orderBy('c.name')
            ->where('c.region=:region')
            ->setParameter('region', $region)
            ->getQuery()
            ->getArrayResult()
        ;
        return new JsonResponse($result);
    }
}